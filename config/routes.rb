Rails.application.routes.draw do
  get 'generate_schedule', to: 'schedule#phenotype'
  mount_devise_token_auth_for 'User', at: 'auth'

  mount_devise_token_auth_for 'Lecturer', at: 'lecturer_auth'

  mount_devise_token_auth_for 'Student', at: 'student_auth'
  as :student do
    # Define routes for Student within this block.
  end
  as :lecturer do
    # Define routes for Lecturer within this block.
  end
  scope '/api' do
  end
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root 'welcome#index'
  mount GraphiQL::Rails::Engine, at: '/graphiql', graphql_path: '/graphql' if Rails.env.development?
  post '/graphql', to: 'graphql#execute'

  get '*path', to: 'application#fallback_index_html', constraints: ->(request) do
    !request.xhr? && request.format.html?
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
