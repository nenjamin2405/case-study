ActiveAdmin.register RegisteredClass do
  menu label: 'Schedule Generator'
  permit_params :day, :start_section, :class_room_id, :groupable_id, :group

  action_item(:edit) do
    link_to 'Generate Schedule', generate_schedule_admin_registered_classes_path, method: :post
  end

  collection_action :generate_schedule, method: :post do
    @schedule = ScheduleGenerator.new(20, 0.01).generate_schedule_for_current_semester
    @fitness_score = @schedule.calculate_score
    redirect_to admin_registered_classes_path, notice: 'Schedule successfully generated'
  end

  form title: 'Edit Registered Class' do |_f|
    inputs 'Details' do
      input :day, as: :select, collection: RegisteredClass.days.map { |key, value| [value, key] }
      input :start_section
      input :class_room, as: :select, collection: ClassRoom.all.map { |cr| [cr.code, cr.id] }
    end
    actions
  end
end
