ActiveAdmin.register Lecturer do
  permit_params :first_name, :last_name, :email, :gender, :address, :date_of_birth, :phone_number, :salary, department_ids: []
  index do
    column :first_name
    column :last_name
    column :email
    column :gender do |obj|
      case obj.gender
      when 1
        'Male'
      else
        'Female'
      end
    end
    column :date_of_birth do |obj|
      obj.date_of_birth.localtime.strftime('%B %d, %Y')
    end
    column :salary
    column :phone_number
    column 'Departments' do |lecturer|
      safe_join(lecturer.departments.map { |dep| link_to dep.name, admin_department_path(dep) }, ', ')
    end
    actions
  end

  show do
    attributes_table do
      row :first_name
      row :last_name
      row :email
      row :gender do |obj|
        case obj.gender
        when 1
          'Male'
        else
          'Female'
        end
      end
      row :address
      row :date_of_birth do |obj|
        obj.date_of_birth.localtime.strftime('%B %d, %Y')
      end
      row :salary
      row :phone_number
      row 'Departments' do |lecturer|
        safe_join(lecturer.departments.map { |dep| link_to dep.name, admin_department_path(dep) }, ', ')
      end
    end
  end

  form title: 'Edit Lecturer' do |_f|
    inputs 'Details' do
      input :first_name
      input :last_name
      input :email
      input :gender, as: :select, collection: [['Male', 1], ['Female', 0]]
      input :address
      input :date_of_birth, as: :datepicker, datepicker_options: {
        change_year: true,
        change_month: true,
        year_range: '1900:+200'
      }
      input :salary
      input :phone_number
      input :departments, as: :select, input_html: { multiple: true }
    end
    actions
  end
end
