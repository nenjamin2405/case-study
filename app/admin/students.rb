ActiveAdmin.register Student do
  permit_params :major_id, :password, :supervisor_id, :first_name, :last_name, :email, :gender, :address, :date_of_birth, :phone_number, :entering_year, :supervisor
  index do
    column :first_name
    column :last_name
    column :entering_year
    column :email
    column :gender do |obj|
      case obj.gender
      when 1
        'Male'
      else
        'Female'
      end
    end
    column :address
    column :date_of_birth do |obj|
      obj.date_of_birth ? obj.date_of_birth.localtime.strftime('%B %d, %Y') : ''
    end
    column :phone_number
    column :major
    column :supervisor
    actions
  end

  show do
    attributes_table do
      row :first_name
      row :last_name
      row :entering_year
      row :email
      row :gender do |obj|
        case obj.gender
        when 1
          'Male'
        else
          'Female'
        end
      end
      row :address
      row :date_of_birth do |obj|
        obj.date_of_birth ? obj.date_of_birth.localtime.strftime('%B %d, %Y') : ''
      end
      row :phone_number
      row :major
      row :supervisor
    end
  end

  form do |_f|
    inputs 'Details' do
      input :first_name
      input :last_name
      input :email
      input :password, as: :password
      input :entering_year
      input :gender, as: :select, collection: [['Male', 1], ['Female', 0]]
      input :address
      input :date_of_birth, as: :datepicker, datepicker_options: {
        change_year: true,
        change_month: true,
        year_range: '1900:+200'
      }
      input :phone_number
      input :major
      input :supervisor
    end
    actions
  end
end
