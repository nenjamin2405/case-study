ActiveAdmin.register Department do
  permit_params :code, :name, :description,
                majors_attributes: [:id, :code, :name, :description, :_destroy]

  sidebar 'Department Details', only: [:show, :edit] do
    ul do
      li link_to 'Majors', admin_department_majors_path(resource)
    end
  end

  show do |department|
    attributes_table do
      row :code
      row :name
      row :description
    end
    div do
      panel('Majors') do
        table_for(department.majors) do
          column :code
          column :name
          column :description
          column :fee_per_credit
        end
      end
    end
  end
end
