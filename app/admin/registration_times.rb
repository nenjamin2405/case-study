ActiveAdmin.register RegistrationTime do
  belongs_to :semester
  permit_params :semester_id, :from_year, :to_year, :start_time, :end_time, :date

  index do
    (RegistrationTime.column_names - %w[start_time end_time semester_id]).each do |c|
      column c.to_sym
    end
    column 'Start Time', &:real_start_time
    column 'End Time', &:real_end_time
    actions
  end

  show do
    attributes_table do
      (RegistrationTime.column_names - %w[start_time end_time semester_id]).each do |r|
        row r.to_sym
      end
      row 'Start Time', &:real_start_time
      row 'End Time', &:real_end_time
    end
  end

  form title: 'Edit Registration Time' do |_f|
    inputs 'Details' do
      input :from_year
      input :to_year
      input :date, as: :datepicker, datepicker_options: {
        change_year: true,
        change_month: true,
        year_range: '2018:+200'
      }
      input :start_time, as: :time_picker
      input :end_time, as: :time_picker
    end
    actions
  end
end
