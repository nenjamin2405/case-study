ActiveAdmin.register Semester do
  permit_params :start_year, :end_year, :category, :usd_to_vnd_rate, theory_groups_attributes: [:id, :slot, :course_id, :lecturer_id], student_school_fees_attributes: [:id, :student_id, :fee, :paid], enrolments_attributes: [:id, :student_id, :status]
  form do |_f|
    inputs do
      input :start_year
      input :end_year
      input :category, as: :select, collection: Semester.categories.map { |key, value| [value, key] }
      input :usd_to_vnd_rate
    end
    actions
  end

  sidebar 'Semester Details', only: [:show, :edit] do
    ul do
      li link_to 'Theory Groups', admin_semester_theory_groups_path(resource)
      li link_to 'Student School Fees', admin_semester_student_school_fees_path(resource)
      li link_to 'Enrolments', admin_semester_enrolments_path(resource)
      li link_to 'Registration Times', admin_semester_registration_times_path(resource)
    end
  end
end
