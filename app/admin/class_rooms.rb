ActiveAdmin.register ClassRoom do
  permit_params :code, :seats, :building, :category, course_class_rooms_attributes: [:id, :coursable_identifier, :_destroy]

  form do |_f|
    inputs do
      input :code
      input :seats
      input :building
      input :category, as: :select, collection: ClassRoom.categories.map { |key, value| [value, key] }
      inputs 'For exclusive courses/labs', class: 'course-relationship-container' do
        has_many :course_class_rooms, allow_destroy: true, class: 'course-relationship', new_record: 'Add New Exclusive Course/Lab' do |s|
          s.input :coursable_identifier, label: 'Course/Lab', collection: (Course.all + Laboratory.all).map { |i| [i.name, "#{i.class}-#{i.id}"] }
        end
      end
    end
    actions
  end
end
