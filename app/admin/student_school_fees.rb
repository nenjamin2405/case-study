ActiveAdmin.register StudentSchoolFee do
  belongs_to :semester
  permit_params :student_id, :fee, :paid, :remit

  form title: 'Edit Student School Fee' do |_f|
    inputs 'Details' do
      input :student
      input :fee, input_html: { readonly: true }
      input :remit
      input :paid
    end
    actions
  end
end
