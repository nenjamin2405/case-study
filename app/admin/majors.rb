ActiveAdmin.register Major do
  belongs_to :department
  permit_params :code, :name, :description, :fee_per_credit
end
