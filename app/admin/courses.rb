ActiveAdmin.register Course do
  permit_params :code, :name, :can_be_enroll_from_year, :credits, :class_sections, :category, :gpa_calculated, :description, major_ids: [], laboratory_attributes: [:id, :code, :name, :credits, :class_sections, :description], prerequisite_courses_attributes: [:id, :prereq_id, :_destroy]
  index do
    (Course.column_names - %w[category description]).each do |c|
      column c.to_sym
    end
    column 'Majors' do |course|
      course.majors.map(&:name).join(', ')
    end
    column 'Category' do |course|
      course.category.humanize.titleize
    end
    actions
  end

  show do
    attributes_table do
      (Course.column_names - %w[category]).each do |c|
        row c.to_sym
      end
      row 'Majors' do |course|
        course.majors.map(&:name).join(', ')
      end
      row 'Category' do |course|
        course.category.humanize.titleize
      end
    end
  end

  form do |_f|
    inputs do
      input :code
      input :name
      input :credits
      input :class_sections
      input :gpa_calculated
      input :can_be_enroll_from_year
      input :description
      input :majors, as: :select, input_html: { multiple: true }
      input :category, as: :select, collection: Course.categories.map { |key, value| [value, key] }, input_html: { data: { function: 'course_category', action: 'slide', target: '.course-lab-creation' } }
      inputs 'Prerequisite Courses', class: 'course-relationship-container' do
        has_many :prerequisite_courses, allow_destroy: true, class: 'course-relationship' do |t|
          t.input :prereq, label: 'Name'
        end
      end
      inputs 'Laboratory', for: [:laboratory, course.laboratory || Laboratory.new], class: 'course-lab-creation' do |lab|
        lab.input :code
        lab.input :name
        lab.input :description
        lab.input :credits
        lab.input :class_sections
      end
    end
    actions
  end
end
