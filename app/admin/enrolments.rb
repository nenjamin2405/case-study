ActiveAdmin.register Enrolment do
  belongs_to :semester
  permit_params :student_id, :status, :semester_id, :registered_class_id

  form title: 'Edit Enrolment' do |_f|
    inputs 'Details' do
      input :student
      input :status, as: :select, collection: Enrolment.statuses.map { |key, value| [value, key] }
      input :registered_class, as: :select, collection: RegisteredClass.all.map { |r| [r.info, r.id] }
    end
    actions
  end
end
