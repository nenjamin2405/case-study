ActiveAdmin.register TheoryGroup do
  belongs_to :semester
  permit_params :start_time, :end_time, :course_id, :lecturer_id, :slot, lab_groups_attributes: [:id, :slot, :lecturer_id, :start_time, :end_time, :_destroy]

  form title: 'Edit Theory Group' do |_f|
    inputs 'Details' do
      input :course, collection: Course.all.map { |course| [course.having_lab?, course.id] }, input_html: { data: { function: 'should_hide_lab', action: 'slide', target: '.course-relationship-container' } }
      input :leader
      input :slot
      input :start_time, as: :datepicker, datepicker_options: {
        change_year: true,
        change_month: true,
        year_range: '2018:+200'
      }
      input :end_time, as: :datepicker, datepicker_options: {
        change_year: true,
        change_month: true,
        year_range: '2018:+200'
      }
      inputs 'Lab Groups', class: 'course-relationship-container' do
        has_many :lab_groups, allow_destroy: true, class: 'course-relationship' do |t|
          t.input :lecturer, label: 'Lecturer In Charge'
          t.input :slot
          t.input :start_time, as: :datepicker, datepicker_options: {
            change_year: true,
            change_month: true,
            year_range: '2018:+200'
          }
          t.input :end_time, as: :datepicker, datepicker_options: {
            change_year: true,
            change_month: true,
            year_range: '2018:+200'
          }
        end
      end
    end
    actions
  end
end
