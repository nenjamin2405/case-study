class Department < ApplicationRecord
  has_many :majors, dependent: :destroy
  accepts_nested_attributes_for :majors, allow_destroy: true
  has_and_belongs_to_many :lecturers
end
