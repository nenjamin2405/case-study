class Comment < ApplicationRecord
  belongs_to :author, polymorphic: true
  belongs_to :discussion
  has_many :notifications, as: :notifiable, dependent: :destroy, inverse_of: :notifiable

  after_create :create_notification

  def create_notification
    discussion_students = discussion.students.uniq
    if author.is_a?(Student) && discussion_students.present?
      (discussion_students - [author]).each do |student|
        Notification.create(actor: author, receiver: student, action: Notification::COMMENT_ACTION, notifiable: self)
      end
      Notification.create(actor: author, receiver: discussion.author, action: Notification::COMMENT_ACTION, notifiable: self)
    else
      discussion_students.each do |student|
        Notification.create(actor: author, receiver: student, action: Notification::COMMENT_ACTION, notifiable: self)
      end
    end
  end
end
