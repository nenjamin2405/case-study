class LabGroup < ApplicationRecord
  belongs_to :laboratory, optional: true
  belongs_to :theory_group
  belongs_to :lecturer
  has_one :registered_class, as: :groupable, dependent: :destroy, inverse_of: :groupable
  has_many :enrolments, through: :registered_class
  delegate :semester, to: :theory_group
  before_save :set_theory_group
  after_create :register_class

  def set_theory_group
    self.laboratory_id = theory_group.course.laboratory.id
  end

  def register_class
    RegisteredClass.create!(
      groupable: self,
      gpa_calculated: theory_group.course.gpa_calculated
    )
    puts '#Register class for this lab group'
  end

  delegate :name, to: :laboratory

  delegate :code, to: :laboratory
end
