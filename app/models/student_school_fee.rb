class StudentSchoolFee < ApplicationRecord
  default_scope { order(created_at: :desc) }
  belongs_to :semester
  belongs_to :student, inverse_of: :school_fees

  def display_name
    student.full_name
  end
end
