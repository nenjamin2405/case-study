class RegisteredClass < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  MON = 'monday'
  TUES = 'tuesday'
  WEDS = 'wednesday'
  THURS = 'thursday'
  FRI = 'friday'
  SATUR = 'saturday'
  THEORY_GROUP = 'TheoryGroup'
  LAB_GROUP = 'LabGroup'
  DAY_SECTIONS = 10
  DATE_FORMAT = '%d/%m/%Y'

  scope :gpa_calculated, -> { where gpa_calculated: true }
  enum day: { monday: MON, tuesday: TUES, wednesday: WEDS, thursday: THURS, friday: FRI, saturday: SATUR }
  enum groupable_type: { TheoryGroup: THEORY_GROUP, LabGroup: LAB_GROUP }
  belongs_to :groupable, polymorphic: true
  belongs_to :class_room, inverse_of: :registered_classes, optional: true
  has_many :enrolments, dependent: :destroy
  has_many :students, through: :enrolments
  has_many :grade_columns, dependent: :destroy
  has_many :discussions, dependent: :destroy
  before_create :add_addtional_info
  after_destroy :destroy_relative_classes

  def name
    semester = groupable.semester
    semester_info = "#{semester.category} #{semester.start_year} #{semester.end_year}"
    if TheoryGroup?
      "theory #{groupable.course.name} #{groupable.id} #{semester_info}"
    else
      "#{groupable.laboratory.name} #{groupable.id} #{semester_info}"
    end
  end

  def add_addtional_info
    case groupable_type
    when THEORY_GROUP
      self.class_sections = groupable.course.class_sections
      self.slot = groupable.slot
    when LAB_GROUP
      self.class_sections = groupable.laboratory.class_sections
      self.slot = groupable.slot
    end
  end

  def lecturer_overlap_with?(other_class)
    groupable.lecturer_id == other_class.groupable.lecturer_id
  end

  def group_overlap_with?(other_class)
    if TheoryGroup?
      return false if other_class.TheoryGroup? || !theory_includes_lab(self, other_class)
    elsif other_class.LabGroup? || !theory_includes_lab(other_class, self)
      return false
    end
    true
  end

  def exclusive_rooms
    if TheoryGroup?
      groupable.course.class_rooms
    else
      groupable.laboratory.class_rooms
    end
  end

  def course_type
    if TheoryGroup?
      if groupable.course.lab_isolated?
        'Lab Isolated'
      else
        'Theory'
      end
    else
      'Laboratory'
    end
  end

  def info
    lecturer_name = groupable.lecturer.full_name
    if TheoryGroup?
      course_name = groupable.course.name
      if groupable.course.lab_isolated?
        "[Lab Isolated] #{course_name} - #{lecturer_name}"
      else
        "[Theory] #{course_name} - #{lecturer_name}"
      end
    else
      "[Laboratory] of Course #{groupable.laboratory.course.name} - #{lecturer_name}"
    end
  end

  def student_grades
    grade_data = students.map do |student|
      row_data = {
        student: student.attributes
      }
      grade_columns.map do |grade_column|
        score = grade_column.grades.find_by(student: student)&.value || 0
        row_data["grade-column-#{grade_column.id}"] = score
      end
      row_data
    end
    grade_data
  end

  def grades_of(student)
    row_data = {}
    grade_columns.map do |grade_column|
      score = grade_column.is_published ? (grade_column.grades.find_by(student: student)&.value) || 0 : ''
      row_data["grade-column-#{grade_column.id}"] = score
    end
    row_data
  end

  def average_grade_of(student)
    average_grade = 0
    grade_columns.map do |grade_column|
      score = grade_column.is_published ? (grade_column.grades.find_by(student: student)&.value) || 0 : 0
      percentage = grade_column.percentage
      average_grade += score * (percentage / 100).to_f
    end
    average_grade.round
  end

  def credits
    enrolments.first.credits
  end

  def status_with(student)
    if TheoryGroup?
      groupable.course.status_with_current_student(student)
    else
      groupable.laboratory.status_with_current_student(student)
    end
  end

  def notify_grade_columns_publishment(grade_columns)
    grade_columns.each do |grade_column|
      students.each do |student|
        Notification.create(actor: groupable.lecturer, receiver: student, action: Notification::GRADE_PUBLISH_ACTION, notifiable: grade_column)
      end
    end
  end

  private

  def theory_includes_lab(theory, lab)
    return true if theory.groupable.lab_groups.pluck(:id).include?(lab.groupable.id)

    false
  end

  def destroy_relative_classes
    if TheoryGroup?
      groupable.lab_groups.map do |lab_group|
        lab_group.registered_classs.destroy
      end
    else
      groupable.theory_group.registered_class.destroy
    end
  end
end
