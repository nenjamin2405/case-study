# frozen_string_literal: true

class Student < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
  include DeviseTokenAuth::Concerns::User

  belongs_to :supervisor, class_name: 'Lecturer', foreign_key: 'supervisor_id', inverse_of: :supervisees
  belongs_to :major
  has_many :school_fees, class_name: 'StudentSchoolFee', foreign_key: 'student_id', dependent: :destroy, inverse_of: :student
  has_many :enrolments, dependent: :destroy do
    def by_semester(semester)
      where(semester: semester)
    end
  end
  has_many :notifications_as_actor, class_name: 'Notification', foreign_key: 'actor_id', as: :actor, dependent: :destroy, inverse_of: :actor
  has_many :notifications, as: :receiver, dependent: :destroy, inverse_of: :receiver
  has_many :comments, as: :author, dependent: :destroy, inverse_of: :author
  has_many :attending_semesters, -> { distinct }, through: :enrolments, source: :semester
  has_many :registered_classes_relations, through: :enrolments, source: :registered_class
  has_many :theory_groups, through: :registered_classes_relations, source_type: 'TheoryGroup', source: :groupable
  has_many :lab_groups, through: :registered_classes_relations, source_type: 'LabGroup', source: :groupable
  has_many :grades, dependent: :destroy
  has_many :this_semester_enrolments, -> { where(semester: Semester.last) }, class_name: 'Enrolment', inverse_of: :student
  has_many :this_semester_registered_classes, through: :this_semester_enrolments, source: :registered_class
  after_create :send_confirmation_email, unless: proc { confirmed_at.present? }

  def full_name
    first_name + ' ' + last_name
  end

  def display_name
    full_name.to_s
  end

  def enroll_classes(class_ids)
    current_semester = Semester.last
    ActiveRecord::Base.transaction do
      enrolments.where(semester: current_semester).destroy_all
      class_ids.each do |class_id|
        (fee, credits) = fee_and_credits_by_class_id(class_id)
        enrolments.create!(registered_class_id: class_id, fee: fee, credits: credits)
        registered_class = RegisteredClass.find(class_id)
        next unless registered_class.LabGroup?

        theory_group_class = registered_class.groupable.theory_group.registered_class
        (fee, credits) = fee_and_credits_by_class_id(theory_group_class.id)
        enrolments.find_or_create_by(registered_class: theory_group_class, fee: fee, credits: credits) if registered_class.LabGroup?
      end
      school_fees.find_or_create_by(semester: current_semester).update(fee: school_fee_by_enrolments)
    end
  end

  def registered_classes
    registered_data = []
    Semester.all.find_each do |semester|
      theory_groups_registered = theory_groups.where(semester: semester)
      lab_groups_registered = semester.lab_groups.where(id: lab_groups.ids)
      next if theory_groups_registered.empty? && lab_groups_registered.empty?

      registered_data << {
        semester: semester,
        theory_groups: theory_groups_registered,
        lab_groups: lab_groups_registered
      }
    end
    registered_data
  end

  def semesters_grades
    grades_data = []
    Semester.all.find_each do |semester|
      grades_data << {
        semester: semester,
        enrolments: enrolments.by_semester(semester)
      }
    end
    grades_data
  end

  def all_semesters_grades_analysis
    data_analysis = {}
    average_grade = 0
    credits_achieved = 0
    attending_semesters.map do |semester|
      semester_data = semester.classes_analysis_of(self)
      average_grade += semester_data[:average_grade]
      credits_achieved += semester_data[:credits_achieved]
    end
    data_analysis[:average_grade] = average_grade
    data_analysis[:credits_achieved] = credits_achieved
    data_analysis[:classification] = GradingConvention.classification_of(average_grade)
    data_analysis[:system_four] = GradingConvention.system_four_of(average_grade)
    data_analysis
  end

  private

  def send_confirmation_email
    send_confirmation_instructions
  end

  def school_fee_by_enrolments
    enrolments.where(semester: Semester.last).pluck(:fee).sum
  end

  def fee_and_credits_by_class_id(class_id)
    registered_class = RegisteredClass.find(class_id)
    major_fee_per_credit = major.fee_per_credit
    if registered_class.TheoryGroup?
      course_credits = registered_class.groupable.course.credits
      [major_fee_per_credit * course_credits, course_credits]
    else
      lab_credits = registered_class.groupable.laboratory.credits
      [lab_credits * major_fee_per_credit, lab_credits]
    end
  end
end
