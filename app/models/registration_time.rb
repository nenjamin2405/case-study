class RegistrationTime < ApplicationRecord
  belongs_to :semester

  def real_start_time
    start_time.to_s(:time)
  end

  def real_end_time
    end_time.to_s(:time)
  end
end
