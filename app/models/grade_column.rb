class GradeColumn < ApplicationRecord
  belongs_to :registered_class
  has_many :grades, dependent: :destroy
  has_many :notifications, as: :notifiable, dependent: :destroy, inverse_of: :notifiable
  validate :total_percentage, on: [:create, :update]
  validates :name, uniqueness: { case_sensitive: false }

  def total_percentage
    prev_total_percentage = registered_class.grade_columns.where.not(id: id).reduce(0) { |total, grade_column| total + grade_column.percentage }
    errors.add(:total_grade_column_percentage, 'must be less than 100%') if prev_total_percentage + percentage > 100
  end
end
