class Laboratory < ApplicationRecord
  belongs_to :course
  has_many :course_class_rooms, as: :coursable, dependent: :destroy, inverse_of: :coursable
  has_many :class_rooms, through: :course_class_rooms
  accepts_nested_attributes_for :class_rooms

  def status_with_current_student(current_student)
    enrolments = current_student.enrolments.sorted_by_created_at
    related_enrolment = enrolments.find do |enrolment|
      registered_class = enrolment.registered_class
      next(false) if registered_class.TheoryGroup? || registered_class.groupable.laboratory != self

      next(true)
    end

    return Enrolment::STATUS_NOT_LEARNED if related_enrolment.blank?

    related_enrolment.status
  end
end
