class Discussion < ApplicationRecord
  default_scope { order(created_at: :desc) }

  CATEGORY_DISCUSSION = 'discussion'
  CATEGORY_ANNOUNCEMENT = 'announcement'

  enum category: { discussion: CATEGORY_DISCUSSION, announcement: CATEGORY_ANNOUNCEMENT }
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]
  belongs_to :author, polymorphic: true
  belongs_to :registered_class
  has_many :comments, dependent: :destroy
  has_many :students, through: :comments, dependent: false, source: :author, source_type: 'Student'
  has_many :notifications, as: :notifiable, dependent: :destroy, inverse_of: :notifiable
  after_create :create_notification

  def create_notification
    registered_class.students.uniq.each do |student|
      Notification.create(actor: author, receiver: student, action: Notification::POST_ACTION, notifiable: self)
    end
  end
end
