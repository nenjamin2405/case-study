class Course < ApplicationRecord
  CATEGORY_THEORY = 'Theory'
  CATEGORY_LAB_ISOLATED = 'Lab Isolated'

  scope :sorted_by_year_can_enroll, -> { order(can_be_enroll_from_year: :asc) }

  enum category: { lab_isolated: CATEGORY_LAB_ISOLATED, theory: CATEGORY_THEORY }

  has_and_belongs_to_many :majors
  accepts_nested_attributes_for :majors

  has_one :laboratory, dependent: :destroy
  accepts_nested_attributes_for :laboratory, reject_if: proc { |attrs| attrs[:code].blank? }

  has_many :prerequisite_courses,
           dependent: :destroy
  accepts_nested_attributes_for :prerequisite_courses, allow_destroy: true

  has_many :course_class_rooms, as: :coursable, dependent: :destroy, inverse_of: :coursable
  has_many :class_rooms, through: :course_class_rooms
  accepts_nested_attributes_for :class_rooms

  def having_lab?
    if laboratory.nil?
      name
    else
      name + ' (has lab)'
    end
  end

  def contains_lab?
    return false if laboratory.nil?

    true
  end

  def status_with_current_student(current_student)
    enrolments = current_student.enrolments.sorted_by_created_at
    related_enrolment = enrolments.find do |enrolment|
      registered_class = enrolment.registered_class
      next(false) if registered_class.LabGroup? || registered_class.groupable.course != self

      next(true)
    end
    return Enrolment::STATUS_NOT_LEARNED if related_enrolment.blank?

    related_enrolment.status
  end
end
