class CourseClassRoom < ApplicationRecord
  belongs_to :coursable, polymorphic: true, inverse_of: :course_class_rooms
  belongs_to :class_room

  def coursable_identifier
    "#{coursable.class}-#{coursable.id}" if coursable
  end

  def coursable_identifier=(coursable_data)
    return if coursable_data.blank?

    coursable_data = coursable_data.split('-')
    self.coursable_type = coursable_data[0]
    self.coursable_id = coursable_data[1]
  end
end
