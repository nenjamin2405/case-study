# frozen_string_literal: true

class Lecturer < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
  include DeviseTokenAuth::Concerns::User

  has_and_belongs_to_many :departments
  has_many :notifications_as_actor, class_name: 'Notification', foreign_key: 'actor_id', as: :actor, dependent: :destroy, inverse_of: :actor
  has_many :notifications, as: :receiver, dependent: :destroy, inverse_of: :receiver
  has_many :discussions, as: :author, dependent: :destroy, inverse_of: :author
  has_many :comments, as: :author, dependent: :destroy, inverse_of: :author
  has_many :supervisees, class_name: 'Student', foreign_key: 'supervisor_id', dependent: :destroy, inverse_of: :supervisor
  has_many :theory_groups, dependent: :destroy, inverse_of: :leader
  has_many :lab_groups, dependent: :destroy
  has_many :this_semester_theory_groups, -> { where(semester: Semester.last) }, class_name: 'TheoryGroup', inverse_of: :lecturer
  has_many :this_semester_theory_classes, through: :this_semester_theory_groups, source: :registered_class
  accepts_nested_attributes_for :departments
  after_create :send_confirmation_email, unless: proc { confirmed_at.present? }

  def full_name
    first_name + ' ' + last_name
  end

  def display_name
    full_name.to_s
  end

  def teaching_classes_this_semester
    this_semester_theory_classes.union(this_semester_lab_classes)
  end

  def this_semester_lab_classes
    RegisteredClass.where(id: Semester.last.lab_groups.where(lecturer: self).map { |lab_group| lab_group.registered_class.id })
  end

  def teaching_classes
    teaching_data = []
    Semester.all.find_each do |semester|
      theory_groups_in_charge = theory_groups.where(semester: semester)
      lab_groups_in_charge = semester.lab_groups.where(lecturer: self)
      next if theory_groups_in_charge.empty? && lab_groups_in_charge.empty?

      teaching_data << {
        semester: semester,
        theory_groups: theory_groups_in_charge,
        lab_groups: lab_groups_in_charge
      }
    end
    teaching_data
  end

  private

  def send_confirmation_email
    send_confirmation_instructions
  end
end
