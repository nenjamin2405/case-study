class TheoryGroup < ApplicationRecord
  belongs_to :course
  belongs_to :semester
  belongs_to :leader, class_name: 'Lecturer', foreign_key: 'lecturer_id', inverse_of: :theory_groups
  belongs_to :lecturer, class_name: 'Lecturer', foreign_key: 'lecturer_id', inverse_of: :theory_groups
  has_many :lab_groups, dependent: :destroy
  accepts_nested_attributes_for :lab_groups, allow_destroy: true
  has_one :registered_class, as: :groupable, dependent: :destroy, inverse_of: :groupable
  has_many :enrolments, through: :registered_class
  after_create :register_class

  def register_class
    RegisteredClass.create!(
      groupable: self,
      gpa_calculated: course.gpa_calculated
    )
    puts '#Register class for this theory group'
  end

  delegate :name, to: :course

  delegate :code, to: :course
end
