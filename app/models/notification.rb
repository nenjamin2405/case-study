class Notification < ApplicationRecord
  belongs_to :actor, polymorphic: true
  belongs_to :receiver, polymorphic: true
  belongs_to :notifiable, polymorphic: true
  scope :unseen, -> { where(seen_at: nil) }
  scope :unread, -> { where(read_at: nil) }
  POST_ACTION = 'posted'
  COMMENT_ACTION = 'commented'
  GRADE_PUBLISH_ACTION = 'published'

  default_scope { order(created_at: :desc).limit(5) }
  after_create :push_notification

  def push_notification
    noti_service = NotificationService.new
    noti_keys = receiver.noti_keys
    return if noti_keys.empty?

    options = noti_options
    noti_service.push_notification(noti_keys, options)
  end

  def content
    case action
    when POST_ACTION
      "#{actor_type} #{actor.full_name} posted a #{notifiable.category} named #{notifiable.title}"
    when COMMENT_ACTION
      "#{actor_type} #{actor.full_name} commented '#{notifiable.content.truncate(10)}' on #{notifiable.discussion.category} named #{notifiable.discussion.title}"
    when GRADE_PUBLISH_ACTION
      "#{actor_type} #{actor.full_name} published a grade column named #{notifiable.name} in #{notifiable.registered_class.name} class"
    end
  end

  def navigate_link
    case action
    when POST_ACTION
      "/class/#{notifiable.registered_class.slug}/#{notifiable.category}s/#{notifiable.slug}"
    when COMMENT_ACTION
      discussion = notifiable.discussion
      "/class/#{discussion.registered_class.slug}/#{discussion.category}s/#{discussion.slug}"
    when GRADE_PUBLISH_ACTION
      "/class/#{notifiable.registered_class.slug}/students"
    end
  end

  private

  def noti_options
    title = title_for_noti_options
    noti_options = {
      title: title
    }
    noti_options[:body] = content
    noti_options[:navigateLink] = navigate_link
    noti_options[:notification] = {
      id: id,
      readAt: read_at,
      type: title_for_noti_options,
      notifiable: notifiable_for_noti_options
    }
    {
      notification: noti_options
    }.as_json
  end

  def title_for_noti_options
    case notifiable
    when Comment
      'Comment'
    when Discussion
      notifiable.category.humanize
    when GradeColumn
      'Grade Column Publishment'
    end
  end

  def notifiable_for_noti_options
    case notifiable
    when Comment
      {
        slug: notifiable.discussion.slug
      }
    when Discussion
      {
        slug: notifiable.slug
      }
    when GradeColumn
      {
        registeredClass: {
          slug: notifiable.registered_class.slug
        }
      }
    end
  end
end
