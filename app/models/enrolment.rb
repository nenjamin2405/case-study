class Enrolment < ApplicationRecord
  STATUS_INCOMPLETED = 'Incompleted'
  STATUS_IN_PROGRESS = 'In Progress'
  STATUS_PASSED = 'Passed'
  STATUS_FAILED = 'Failed'
  STATUS_NOT_LEARNED = 'Not Learned'

  scope :sorted_by_created_at, -> { order(created_at: :desc) }
  enum status: { incompleted: STATUS_INCOMPLETED, in_progress: STATUS_IN_PROGRESS, passed: STATUS_PASSED, failed: STATUS_FAILED }
  belongs_to :student, inverse_of: :this_semester_enrolments
  belongs_to :semester
  belongs_to :registered_class
  before_validation :set_default_attributes

  def set_default_attributes
    self.status ||= STATUS_IN_PROGRESS
    self.semester ||= Semester.last
  end
end
