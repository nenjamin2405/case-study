class Semester < ApplicationRecord
  CATEGORY_SUMMER = 'Summer'
  CATEGORY_SPRING = 'Spring'
  CATEGORY_FALL = 'Fall'

  FIRST_CLASS = 'First'
  UPPER_SECOND_CLASS = 'Upper Second'
  LOWER_SECOND_CLASS = 'Lower Second Class'
  THIRD = 'Third'
  PASS = 'Pass'

  enum category: { summer: CATEGORY_SUMMER, spring: CATEGORY_SPRING, fall: CATEGORY_FALL }
  has_many :theory_groups, dependent: :destroy
  has_many :lab_groups, through: :theory_groups
  accepts_nested_attributes_for :theory_groups, allow_destroy: true
  has_many :student_school_fees, dependent: :destroy
  accepts_nested_attributes_for :student_school_fees, allow_destroy: true
  has_many :enrolments, dependent: :destroy
  accepts_nested_attributes_for :enrolments, allow_destroy: true
  has_many :registration_times, dependent: :destroy
  accepts_nested_attributes_for :registration_times, allow_destroy: true
  def registered_classes
    class_ids = []
    theory_groups.each do |theory|
      class_ids.push theory.registered_class.id
      theory.lab_groups.each do |lab|
        class_ids.push lab.registered_class.id
      end
    end
    class_ids
  end

  def theory_groups_for_current_student(current_student)
    entering_year = current_student.entering_year
    current_amount_of_year_of_student = end_year - entering_year

    current_student_course_ids = current_student.major.courses.where('can_be_enroll_from_year <= ?', current_amount_of_year_of_student).ids
    theory_groups.where(course_id: current_student_course_ids)
    # @TODO Constraints for courses preprequisites
    # @TODO GPA calculated for scoring
    # @TODO Add prereq into course program
  end

  def it_is_time_for_registration?(current_student)
    registration_time = registration_times.find_by('from_year <= ? AND to_year >= ? AND date BETWEEN ? AND ?', current_student.entering_year, current_student.entering_year, DateTime.now.utc.beginning_of_day, DateTime.now.utc.end_of_day)
    return [false, nil] unless registration_time.present? && registration_time.end_time.strftime('%H%M%S%N') >= Time.now.in_time_zone('Asia/Ho_Chi_Minh').strftime('%H%M%S%N') && registration_time.start_time.strftime('%H%M%S%N') <= Time.now.in_time_zone('Asia/Ho_Chi_Minh').strftime('%H%M%S%N')

    [true, registration_time.end_time.strftime('%H%M%S%N')]
  end

  def classes_analysis_of(student)
    data_analysis = {}
    gpa_calculated_classes = student.registered_classes_relations.gpa_calculated
    semester_average_grade = gpa_calculated_classes.empty? ? 0 : (gpa_calculated_classes.reduce(0) { |total, registered_class| total + registered_class.average_grade_of(student) } / gpa_calculated_classes.count.to_f).round
    data_analysis[:average_grade] = semester_average_grade
    data_analysis[:credits_achieved] = gpa_calculated_classes.reduce(0) do |total, registered_class|
      next(0) unless registered_class.status_with(student) == Enrolment::STATUS_PASSED.downcase

      total + registered_class.credits
    end
    data_analysis[:classification] = GradingConvention.classification_of(semester_average_grade)
    data_analysis[:system_four] = GradingConvention.system_four_of(semester_average_grade)
    data_analysis
  end
end
