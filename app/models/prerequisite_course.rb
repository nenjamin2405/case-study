class PrerequisiteCourse < ApplicationRecord
  belongs_to :prereq, class_name: 'Course'
  belongs_to :course, class_name: 'Course'
end
