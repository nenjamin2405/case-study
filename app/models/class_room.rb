class ClassRoom < ApplicationRecord
  CATEGORY_LAB = 'Lab'
  CATEGORY_NORMAL = 'Normal'
  CATEGORY_EXCLUSIVE = 'Exclusive'

  enum category: { lab: CATEGORY_LAB, normal: CATEGORY_NORMAL, exclusive: CATEGORY_EXCLUSIVE }
  has_many :course_class_rooms, dependent: :destroy
  accepts_nested_attributes_for :course_class_rooms, allow_destroy: true
  has_many :registered_classes, dependent: :destroy, inverse_of: :class_room
end
