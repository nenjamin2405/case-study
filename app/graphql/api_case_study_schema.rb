class ApiCaseStudySchema < GraphQL::Schema
  mutation(Types::MutationType)
  query(Types::QueryType)

  def self.resolve_type(_type, obj, _ctx)
    case obj
    when Student
      Types::Student
    when Lecturer
      Types::Lecturer
    when TheoryGroup
      Types::TheoryGroup
    when LabGroup
      Types::LabGroup
    when GradeColumn
      Types::GradeColumn
    when Discussion
      Types::Discussion
    when Comment
      Types::Comment
    end
  end
end

GraphQL::Errors.configure(ApiCaseStudySchema) do
  rescue_from ActiveRecord::RecordNotFound do |_exception|
    nil
  end

  rescue_from ActiveRecord::RecordInvalid do |exception|
    GraphQL::ExecutionError.new(exception.record.errors.full_messages.join(', '))
  end

  # rescue_from StandardError do |_exception|
  #   GraphQL::ExecutionError.new('Please try to execute the query for this field later')
  # end

  # rescue_from CustomError do |exception, object, arguments, context|
  #   error = GraphQL::ExecutionError.new("Error found!")
  #   firstError.path = context.path + ["myError"]
  #   context.add_error(firstError)
  # end
end
