module Types
  class ClassesForCurrentUserType < Types::BaseObject
    field :theory_groups, [Types::TheoryGroup], null: true, description: 'Classes for current student to register'
    field :registered_class_ids, [String], null: true
    field :it_is_time_for_registration, Boolean, null: false
    field :end_time, String, null: true
  end
end
