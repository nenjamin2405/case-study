module Types
  class TimetableWeeks < Types::BaseObject
    field :weeks, [String], null: true
    field :week_covers_current_time, String, null: true
  end
end
