module Types
  class ClassRoom < Types::BaseObject
    field :id, ID, null: false
    field :code, String, null: false
    field :seats, Integer, null: false
    field :building, String, null: false
    field :campus, String, null: false
    field :category, String, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
