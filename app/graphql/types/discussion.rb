module Types
  class Discussion < Types::BaseObject
    field :id, ID, null: false
    field :author_type, String, null: false
    field :author_id, Integer, null: false
    field :author, Types::User, null: true
    field :content, String, null: false
    field :title, String, null: false
    field :category, String, null: false
    field :is_comments_enabled, Boolean, null: false
    field :slug, String, null: true
    field :comments, [Types::Comment], null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :registered_class, Types::RegisteredClass, null: false
  end
end
