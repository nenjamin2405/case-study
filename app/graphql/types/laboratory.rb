module Types
  class Laboratory < Types::BaseObject
    field :id, ID, null: false
    field :code, String, null: false
    field :name, String, null: false
    field :description, String, null: false
    field :credits, Integer, null: false
    field :class_sections, Integer, null: false
    field :course_id, Integer, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :status_with_current_student, String, null: true

    def status_with_current_student
      status = object.status_with_current_student(context[:current_user])
      return nil if status.blank?

      status.humanize.titleize
    end
  end
end
