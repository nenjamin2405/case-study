module Types
  class AdminUser < Types::BaseObject
    field :id, ID, null: false
    field :email, String, null: false
    field :reset_password_sent_at, GraphQL::Types::ISO8601DateTime, null: false
    field :remember_created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
