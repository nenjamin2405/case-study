module Types
  class LabGroup < Types::BaseObject
    field :id, ID, null: false
    field :laboratory_id, Integer, null: false
    field :theory_group_id, Integer, null: false
    field :slot, Integer, null: true
    field :laboratory, Types::Laboratory, null: true
    field :remaining_slot, Integer, null: true
    field :start_time, GraphQL::Types::ISO8601DateTime, null: false
    field :end_time, GraphQL::Types::ISO8601DateTime, null: false
    field :lecturer, Types::Lecturer, null: false
    field :registered_class, Types::RegisteredClass, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :name, String, null: false
    field :code, String, null: false
  end
end
