module Types
  class ClassStudentsType < Types::BaseObject
    field :student_grades, Types::Json, null: false
    field :grade_columns, [Types::GradeColumn], null: true
  end
end
