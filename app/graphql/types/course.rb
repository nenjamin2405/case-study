module Types
  class Course < Types::BaseObject
    field :id, ID, null: false
    field :gpa_calculated, Boolean, null: false
    field :laboratory, Types::Laboratory, null: true
    field :code, String, null: false
    field :name, String, null: false
    field :can_be_enroll_from_year, Integer, null: false
    field :description, String, null: false
    field :credits, Integer, null: false
    field :class_sections, Integer, null: false
    field :category, String, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :status_with_current_student, String, null: true

    def category
      object.category.humanize.titleize
    end

    def status_with_current_student
      status = object.status_with_current_student(context[:current_user])
      return nil if status.blank?

      status.humanize.titleize
    end
  end
end
