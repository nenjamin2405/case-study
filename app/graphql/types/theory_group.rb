module Types
  class TheoryGroup < Types::BaseObject
    field :id, ID, null: false
    field :lecturer, Types::Lecturer, null: false
    field :registered_class, Types::RegisteredClass, null: false
    field :semester, Types::Semester, null: false
    field :course, Types::Course, null: false
    field :lab_groups, [Types::LabGroup], null: false
    field :slot, Integer, null: true
    field :remaining_slot, Integer, null: true
    field :start_time, GraphQL::Types::ISO8601DateTime, null: false
    field :end_time, GraphQL::Types::ISO8601DateTime, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :name, String, null: false
    field :code, String, null: false
  end
end
