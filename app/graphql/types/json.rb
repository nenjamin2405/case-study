module Types
  class Json < Types::BaseScalar
    def self.coerce_input(input_value, _context)
      JSON.parse(input_value)
    end

    def self.coerce_result(ruby_value, _context)
      JSON.dump(ruby_value)
    end
  end
end
