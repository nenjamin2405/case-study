module Types
  class SemestersGradesType < Types::BaseObject
    field :enrolments, [Types::Enrolment], null: true
    field :semester, Types::Semester, null: true
  end
end
