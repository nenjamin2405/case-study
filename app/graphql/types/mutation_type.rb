module Types
  class MutationType < Types::BaseObject
    field :unregisterNotiKey, mutation: Mutations::UnregisterNotiKey
    field :registerNotiKey, mutation: Mutations::RegisterNotiKey
    field :markAllSeen, mutation: Mutations::MarkAllSeen
    field :markAsRead, mutation: Mutations::MarkAsRead
    field :createDiscussion, mutation: Mutations::CreateDiscussion
    field :togglePublishGradeColumn, mutation: Mutations::TogglePublishGradeColumn
    field :publishClassGrades, mutation: Mutations::PublishClassGrades
    field :updateRecord, mutation: Mutations::UpdateRecord
    field :deleteGradeColumn, mutation: Mutations::DeleteGradeColumn
    field :save_student_grades, mutation: Mutations::SaveStudentGrades
    field :createRecord, mutation: Mutations::CreateRecord
    field :enroll_courses, mutation: Mutations::EnrollCourses
    field :update_avatar, mutation: Mutations::UpdateAvatar
    field :create_comment, mutation: Mutations::CreateComment
  end
end
