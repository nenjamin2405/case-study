module Types
  class Record < Types::BaseUnion
    description 'Record can be any record'
    possible_types Types::GradeColumn
  end
end
