module Types
  class Notifiable < Types::BaseUnion
    description 'Notification can be a lot of types'
    possible_types Types::Discussion, Types::Comment, Types::GradeColumn
  end
end
