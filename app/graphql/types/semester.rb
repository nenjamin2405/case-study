module Types
  class Semester < Types::BaseObject
    field :id, ID, null: false
    field :category, String, null: false
    field :usd_to_vnd_rate, Float, null: false
    field :start_year, Integer, null: false
    field :end_year, Integer, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :classes_analysis_of_current_student, Types::ClassesAnalysisType, null: true

    def category
      object.category.capitalize
    end

    def classes_analysis_of_current_student
      object.classes_analysis_of(context[:current_user])
    end
  end
end
