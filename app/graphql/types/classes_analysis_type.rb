module Types
  class ClassesAnalysisType < Types::BaseObject
    field :average_grade, Float, null: false
    field :credits_achieved, Integer, null: true
    field :classification, String, null: false
    field :system_four, Float, null: true
  end
end
