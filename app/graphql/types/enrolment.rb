module Types
  class Enrolment < Types::BaseObject
    field :id, ID, null: false
    field :student, Types::Student, null: false
    field :status, String, null: false
    field :semester_id, Integer, null: false
    field :fee, Float, null: false
    field :credits, Integer, null: false
    field :registered_class, Types::RegisteredClass, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
