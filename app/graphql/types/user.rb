module Types
  class User < Types::BaseUnion
    description 'Current user can be lecturer or student'
    possible_types Types::Student, Types::Lecturer
  end
end
