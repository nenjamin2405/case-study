module Types
  class RegisteredClass < Types::BaseObject
    field :id, ID, null: false
    field :day, String, null: true
    field :start_section, Integer, null: true
    field :class_room_id, Integer, null: true
    field :groupable, Types::Groupable, null: false
    field :class_sections, Integer, null: true
    field :slot, Integer, null: true
    field :amount_of_students, Integer, null: true
    field :class_room, Types::ClassRoom, null: true
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :registered_by_current_student, Boolean, null: true
    field :slug, String, null: true
    field :enrolments, [Types::Enrolment], null: true
    field :average_grade_of_current_student, Float, null: true
    field :credits, Integer, null: true

    def day
      object.day&.capitalize
    end

    def amount_of_students
      object.enrolments.count
    end

    delegate :credits, to: :object

    delegate :groupable, to: :object

    def registered_by_current_student
      context[:current_user].is_a?(::Student) && object.enrolments.exists?(student: context[:current_user])
    end

    def average_grade_of_current_student
      object.average_grade_of(context[:current_user])
    end
  end
end
