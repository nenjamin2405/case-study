module Types
  class Timetable < Types::BaseObject
    field :section, Integer, null: true
    field :monday, Types::RegisteredClass, null: true
    field :tuesday, Types::RegisteredClass, null: true
    field :wednesday, Types::RegisteredClass, null: true
    field :thursday, Types::RegisteredClass, null: true
    field :friday, Types::RegisteredClass, null: true
    field :saturday, Types::RegisteredClass, null: true
  end
end
