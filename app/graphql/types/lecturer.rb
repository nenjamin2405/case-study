module Types
  class Lecturer < Types::BaseObject
    field :id, ID, null: false
    field :provider, String, null: false
    field :uid, String, null: false
    field :full_name, String, null: false
    field :encrypted_password, String, null: false
    field :reset_password_token, String, null: false
    field :reset_password_sent_at, GraphQL::Types::ISO8601DateTime, null: false
    field :allow_password_change, Boolean, null: false
    field :remember_created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :sign_in_count, Integer, null: false
    field :current_sign_in_at, GraphQL::Types::ISO8601DateTime, null: false
    field :last_sign_in_at, GraphQL::Types::ISO8601DateTime, null: false
    field :current_sign_in_ip, String, null: false
    field :last_sign_in_ip, String, null: false
    field :confirmation_token, String, null: false
    field :confirmed_at, GraphQL::Types::ISO8601DateTime, null: false
    field :confirmation_sent_at, GraphQL::Types::ISO8601DateTime, null: false
    field :unconfirmed_email, String, null: false
    field :name, String, null: false
    field :nickname, String, null: false
    field :image, String, null: false
    field :email, String, null: false
    field :gender, Integer, null: false
    field :first_name, String, null: false
    field :last_name, String, null: false
    field :address, String, null: false
    field :date_of_birth, GraphQL::Types::ISO8601DateTime, null: false
    field :salary, Float, null: false
    field :phone_number, String, null: false
    field :tokens, Json, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :avatar, String, null: false
  end
end
