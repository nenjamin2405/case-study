module Types
  class Grade < Types::BaseObject
    field :id, ID, null: false
    field :grade_column_id, Integer, null: false
    field :student_id, Integer, null: false
    field :value, Float, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
