module Types
  class Groupable < Types::BaseUnion
    description 'Groupable can be theory group or lab group'
    possible_types Types::TheoryGroup, Types::LabGroup
  end
end
