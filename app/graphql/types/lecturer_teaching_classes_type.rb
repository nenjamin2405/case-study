module Types
  class LecturerTeachingClassesType < Types::BaseObject
    field :theory_groups, [Types::TheoryGroup], null: true
    field :lab_groups, [Types::LabGroup], null: true
    field :semester, Types::Semester, null: true
  end
end
