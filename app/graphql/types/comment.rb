module Types
  class Comment < Types::BaseObject
    field :id, ID, null: false
    field :author_type, String, null: false
    field :author_id, Integer, null: false
    field :author, Types::User, null: true
    field :content, String, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :discussion_slug, String, null: false

    def discussion_slug
      object.discussion.slug
    end
  end
end
