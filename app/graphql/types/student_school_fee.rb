module Types
  class StudentSchoolFee < Types::BaseObject
    field :id, ID, null: false
    field :semester, Types::Semester, null: false
    field :student_id, Integer, null: false
    field :fee, Float, null: false
    field :paid, Float, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :remit, Float, null: false
    field :enrolments, [Types::Enrolment], null: true

    def enrolments
      object.student.enrolments.where(semester: object.semester)
    end
  end
end
