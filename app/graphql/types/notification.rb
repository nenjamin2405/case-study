module Types
  class Notification < Types::BaseObject
    field :id, ID, null: false
    field :actor_type, String, null: false
    field :actor_id, Integer, null: false
    field :actor, Types::User, null: false
    field :receiver_type, String, null: false
    field :receiver_id, Integer, null: false
    field :receiver, Types::User, null: false
    field :read_at, GraphQL::Types::ISO8601DateTime, null: true
    field :seen_at, GraphQL::Types::ISO8601DateTime, null: true
    field :action, String, null: false
    field :notifiable_type, String, null: false
    field :notifiable_id, Integer, null: false
    field :notifiable, Types::Notifiable, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :content, String, null: true
    field :navigate_link, String, null: true

    delegate :content, to: :object
    delegate :navigate_link, to: :object
  end
end
