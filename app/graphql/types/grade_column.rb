module Types
  class GradeColumn < Types::BaseObject
    field :id, ID, null: false
    field :registered_class, Types::RegisteredClass, null: false
    field :name, String, null: false
    field :percentage, Float, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :is_published, Boolean, null: false
  end
end
