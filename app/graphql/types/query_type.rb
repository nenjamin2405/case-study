module Types
  class QueryType < Types::BaseObject
    field :current_user, Types::User, null: true, description: 'Current User'

    def current_user
      context[:current_user]
    end

    field :classes_for_current_student_to_register, Types::ClassesForCurrentUserType, null: true, description: 'Classes for current student to register'

    def classes_for_current_student_to_register
      return nil if ::RegisteredClass.where(id: ::Semester.last.registered_classes).exists?(day: nil)

      (is_time_for_registration, end_time) = ::Semester.last.it_is_time_for_registration?(context[:current_user])
      {
        theory_groups: ::Semester.last.theory_groups_for_current_student(context[:current_user]),
        registered_class_ids: context[:current_user].enrolments.pluck(:registered_class_id).reject { |class_id| ::RegisteredClass.find(class_id).TheoryGroup? && ::RegisteredClass.find(class_id).groupable.lab_groups.present? },
        it_is_time_for_registration: is_time_for_registration,
        end_time: end_time
      }
    end

    field :timetable_for_current_student, [Types::Timetable], null: true do
      argument :week, String, required: true
    end

    def timetable_for_current_student(week:)
      TimePeriod.timetable_for(week, context[:current_user].this_semester_registered_classes)
    end

    field :enrolments, [Types::Enrolment], null: true do
      argument :registered_class_id, String, required: false
    end

    def enrolments(registered_class_id:)
      registered_class_id.present? ? ::RegisteredClass.find(registered_class_id).enrolments : ::Enrolment.all
    end

    field :class_students, Types::ClassStudentsType, null: true do
      argument :registered_class_id, String, required: true
    end

    def class_students(registered_class_id:)
      registered_class = ::RegisteredClass.find(registered_class_id)
      {
        student_grades: registered_class.student_grades,
        grade_columns: registered_class.grade_columns
      }
    end

    field :grades_of_student, Types::ClassStudentsType, null: true do
      argument :registered_class_id, String, required: true
    end

    def grades_of_student(registered_class_id:)
      registered_class = ::RegisteredClass.find(registered_class_id)
      {
        student_grades: registered_class.grades_of(current_user),
        grade_columns: registered_class.grade_columns
      }
    end

    field :student_school_fees, [Types::StudentSchoolFee], null: false

    def student_school_fees
      context[:current_user].school_fees
    end

    field :student_course_program, [Types::Course], null: false

    def student_course_program
      context[:current_user].major.courses.sorted_by_year_can_enroll
    end

    field :timetable_weeks, Types::TimetableWeeks, null: true

    def timetable_weeks
      (weeks, week_covers_current_time) = TimePeriod.weeks_for_timetable(context[:current_user].this_semester_registered_classes)
      return nil if weeks.blank?

      {
        weeks: weeks,
        week_covers_current_time: week_covers_current_time
      }
    end

    field :lecturer_teaching_classes, [Types::LecturerTeachingClassesType], null: false

    def lecturer_teaching_classes
      context[:current_user].teaching_classes
    end

    field :student_registered_classes, [Types::LecturerTeachingClassesType], null: false

    def student_registered_classes
      context[:current_user].registered_classes
    end

    field :lecturer_timetable_weeks, Types::TimetableWeeks, null: true

    def lecturer_timetable_weeks
      (weeks, week_covers_current_time) = TimePeriod.weeks_for_timetable(context[:current_user].teaching_classes_this_semester)
      return nil if weeks.blank?

      {
        weeks: weeks,
        week_covers_current_time: week_covers_current_time
      }
    end

    field :timetable_for_current_lecturer, [Types::Timetable], null: true do
      argument :week, String, required: true
    end

    def timetable_for_current_lecturer(week:)
      TimePeriod.timetable_for(week, context[:current_user].teaching_classes_this_semester)
    end

    field :registered_class, Types::RegisteredClass, null: true do
      argument :slug, String, required: true
    end

    def registered_class(slug:)
      ::RegisteredClass.find(slug)
    end

    field :students_list, [Types::Student], null: true do
      argument :slug, String, required: true
    end

    def students_list(slug:)
      ::RegisteredClass.find(slug).students.where.not(id: current_user.id)
    end

    field :all_semesters_grades_analysis, Types::ClassesAnalysisType, null: true

    delegate :all_semesters_grades_analysis, to: :current_user

    field :class_discussions, [Types::Discussion], null: true do
      argument :slug, String, required: true
    end

    def class_discussions(slug:)
      ::RegisteredClass.find(slug).discussions.discussion
    end

    def class_announcements(slug:)
      ::RegisteredClass.find(slug).discussions.announcement
    end

    field :class_announcements, [Types::Discussion], null: true do
      argument :slug, String, required: true
    end

    field :discussion, Types::Discussion, null: true do
      argument :discussion_slug, String, required: true
    end

    def discussion(discussion_slug:)
      ::Discussion.find(discussion_slug)
    end

    field :comments, [Types::Comment], null: true do
      argument :discussion_slug, String, required: true
    end

    def comments(discussion_slug:)
      ::Discussion.find(discussion_slug).comments
    end

    field :notifications, [Types::Notification], null: true

    delegate :notifications, to: :current_user
  end
end
