module Mutations
  class CreateComment < GraphQL::Schema::RelayClassicMutation
    argument :attributes, Types::Json, required: true

    field :comment, Types::Comment, null: true

    def resolve(attributes:)
      attributes['discussion_id'] = Discussion.find(attributes['discussion_id']).id
      comment = Comment.create!(author: context[:current_user], **attributes.symbolize_keys)
      {
        comment: comment
      }
    end
  end
end
