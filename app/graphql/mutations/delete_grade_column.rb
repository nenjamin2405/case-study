module Mutations
  class DeleteGradeColumn < GraphQL::Schema::RelayClassicMutation
    argument :id, ID, required: true

    field :success, Boolean, null: false

    def resolve(id:)
      GradeColumn.find(id).destroy
      {
        success: true
      }
    end
  end
end
