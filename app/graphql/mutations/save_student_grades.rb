module Mutations
  class SaveStudentGrades < GraphQL::Schema::RelayClassicMutation
    argument :student_grades, Types::Json, required: true

    field :success, Boolean, null: false

    def resolve(student_grades:)
      student_grades.each do |student_grade_row|
        student_id = student_grade_row['student']['id']
        student_grade_row.except('student').each do |key, value|
          grade_column_id = key.split('-').last
          Grade.find_or_create_by(student_id: student_id, grade_column_id: grade_column_id).update(value: value.to_i)
        end
      end
      {
        success: true
      }
    end
  end
end
