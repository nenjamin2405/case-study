module Mutations
  class CreateRecord < GraphQL::Schema::RelayClassicMutation
    argument :attributes, Types::Json, required: true
    argument :type, String, required: true

    field :record, Types::Record, null: false

    def resolve(attributes:, type:)
      if attributes.key?('registered_class_id')
        attributes['registered_class_id'] = RegisteredClass.find(attributes['registered_class_id']).id
      end
      record_created = type.constantize.create!(attributes)
      {
        record: record_created
      }
    end
  end
end
