module Mutations
  class EnrollCourses < GraphQL::Schema::RelayClassicMutation
    argument :class_ids, [String], required: true

    field :enrolled, Boolean, null: false

    def resolve(class_ids:)
      context[:current_user].enroll_classes(class_ids)
      {
        enrolled: true
      }
    end
  end
end
