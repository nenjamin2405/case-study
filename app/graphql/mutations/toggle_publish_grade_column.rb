module Mutations
  class TogglePublishGradeColumn < GraphQL::Schema::RelayClassicMutation
    argument :id, String, required: true

    field :grade_column, Types::GradeColumn, null: false

    def resolve(id:)
      grade_column = GradeColumn.find(id)
      grade_column.toggle!(:is_published)
      if grade_column.reload.is_published
        grade_column.registered_class.notify_grade_columns_publishment([grade_column])
      end
      {
        grade_column: grade_column.reload
      }
    end
  end
end
