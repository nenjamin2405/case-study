module Mutations
  class CreateDiscussion < GraphQL::Schema::RelayClassicMutation
    argument :attributes, Types::Json, required: true

    field :discussion, Types::Discussion, null: true

    def resolve(attributes:)
      attributes['registered_class_id'] = RegisteredClass.find(attributes['registered_class_id']).id
      discussion = Discussion.create!(author: context[:current_user], **attributes.symbolize_keys)
      {
        discussion: discussion
      }
    end
  end
end
