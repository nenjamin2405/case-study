module Mutations
  class MarkAllSeen < GraphQL::Schema::RelayClassicMutation
    field :marked, Boolean, null: false

    def resolve
      context[:current_user].notifications.unseen.update_all(seen_at: Time.now.utc)
      {
        marked: true
      }
    end
  end
end
