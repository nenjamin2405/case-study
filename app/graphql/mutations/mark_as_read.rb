module Mutations
  class MarkAsRead < GraphQL::Schema::RelayClassicMutation
    argument :notification_id, String, required: true

    field :marked, Boolean, null: false

    def resolve(notification_id:)
      ::Notification.find(notification_id).update(read_at: Time.now.utc)
      {
        marked: true
      }
    end
  end
end
