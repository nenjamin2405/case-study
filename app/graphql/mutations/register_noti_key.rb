module Mutations
  class RegisterNotiKey < GraphQL::Schema::RelayClassicMutation
    argument :noti_key, String, required: true
    field :registered, Boolean, null: false

    def resolve(noti_key:)
      noti_keys = context[:current_user].noti_keys
      unless noti_keys.include?(noti_key)
        noti_keys << noti_key
        context[:current_user].update(noti_keys: noti_keys)
      end

      { registered: true }
    end
  end
end
