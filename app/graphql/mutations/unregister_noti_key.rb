module Mutations
  class UnregisterNotiKey < GraphQL::Schema::RelayClassicMutation
    argument :noti_key, String, required: true

    field :un_registered, Boolean, null: false

    def resolve(noti_key:)
      noti_keys = context[:current_user].noti_keys
      noti_keys.delete(noti_key)
      context[:current_user].update(noti_keys: noti_keys)

      { un_registered: true }
    end
  end
end
