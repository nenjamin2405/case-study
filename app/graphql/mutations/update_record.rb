module Mutations
  class UpdateRecord < GraphQL::Schema::RelayClassicMutation
    argument :attributes, Types::Json, required: true
    argument :type, String, required: true

    field :record, Types::Record, null: false

    def resolve(attributes:, type:)
      record_need_to_be_updated = type.constantize.find(attributes['id'])
      record_need_to_be_updated.update!(attributes)
      {
        record: record_need_to_be_updated.reload
      }
    end
  end
end
