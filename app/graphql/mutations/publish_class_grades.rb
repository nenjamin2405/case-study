module Mutations
  class PublishClassGrades < GraphQL::Schema::RelayClassicMutation
    argument :slug, String, required: true

    field :success, Boolean, null: false

    def resolve(slug:)
      register_class = RegisteredClass.find(slug)
      grade_columns = register_class.grade_columns
      grade_columns.update_all(is_published: true)
      register_class.notify_grade_columns_publishment(grade_columns)
      {
        success: true
      }
    end
  end
end
