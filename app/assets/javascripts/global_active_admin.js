function should_hide_lab( el ) {
  let course_name = $("span#select2-theory_group_course_id-container").attr('title')
  if (course_name && course_name.includes('(has lab)')){
    return false
  }
  return true
}

function course_category(el) {
  let course_category = $("span#select2-course_category-container").attr('title')
  if(course_category && course_category === 'Lab Isolated'){
    return true
  }
  return false
}