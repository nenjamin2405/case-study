class ScheduleController < ApplicationController
  def phenotype
    @schedule = ScheduleGenerator.new(20, 0.01).generate_schedule_for_current_semester
    @fitness_score = @schedule.calculate_score
  end
end
