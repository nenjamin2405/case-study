class Schedule
  attr_accessor :number_of_rooms, :slots, :rooms_arr, :score, :class_positions, :constraint_count, :number_of_days, :number_of_sections
  def initialize(rooms_arr, classes_arr)
    @number_of_days = 6
    @number_of_sections = 10
    # Initialize two main array as well as their lengths
    @rooms_arr = rooms_arr
    @number_of_rooms = @rooms_arr.length
    @classes_arr = classes_arr # array of classes from active record
    @number_of_classes = @classes_arr.length

    # Main attributes of Schedule
    @slots =
      Array.new(@number_of_days) do
        Array.new(@number_of_sections) do
          Array.new(number_of_rooms) do
            []
          end
        end
      end
    # 3-dimensional array to store classes
    @class_positions = {} # hash to store position of class in 3-dimensional array (the genotype)
    @score = 0
    @constraint_count = []
  end

  # place classes at random position
  def randomize_classes
    @classes_arr.each do |class_id|
      registered_class = RegisteredClass.find(class_id)
      duration = registered_class.class_sections
      # Random attributes
      room_rand = rand_room_index(rooms_arr: @rooms_arr, registered_class: registered_class)
      day_rand = rand(0...@number_of_days)
      start_section_rand = rand_start_section(duration)

      # Combine random attribute into array
      position = [day_rand, start_section_rand, room_rand]

      # Add class position to hash
      @class_positions[class_id] = position

      # According to duration, add class to number of slots
      (start_section_rand...(start_section_rand + duration)).each do |section|
        @slots[day_rand][section][room_rand].push(class_id)
      end
    end
  end

  def calculate_score
    # initialize the total score for all classes
    total_score = 0
    lecturer_overlap_count = 0
    group_overlap_count = 0
    not_enough_seats_count = 0
    room_overlap_count = 0
    theory_in_lab_room_count = 0
    class_need_exclusive_but_not_in_it_count = 0
    lab_class_without_exclusive_not_in_lab_room_count = 0
    # loop through each class to calculate score
    @class_positions.each do |class_id, position|
      # puts "Calculating score for class: #{class_id} of position #{position}"
      # Get corresponding informaton of class position (day, start_section, room)
      day = position[0]
      start_section = position[1]
      room = position[2]
      duration = RegisteredClass.find(class_id).class_sections

      # 1st Constraint: If a class uses a spare classroom, we increment its score
      room_overlap = false
      (start_section...(start_section + duration)).each do |section|
        if @slots[day][section][room].length > 1
          room_overlap = true
          break
        end
      end

      # puts "Room overlaped" if room_overlap
      total_score += 1 unless room_overlap

      # 2nd Constraint: If a class is located in a classroom with enough available seats, guess what, we increment its score.
      room_seats = ClassRoom.find(@rooms_arr[room]).seats
      class_seats = RegisteredClass.find(class_id).slot
      p room_seats
      p class_seats
      # if a class belongs to many group --> class seats = total group seats
      total_score += 1 if room_seats >= class_seats

      # puts "Not enough seats" if room_seats < class_seats

      # 3rd Constraint: If a lecturer has no other classes at the time, we increment the class's score once again.
      # 4th Constraint: if any of the student groups that attend the class has any other class at the same time, and if they don't, we increment the score of the class
      lecturer_overlap = false
      group_overlap = false
      current_class = RegisteredClass.find(class_id)
      catch :both_overlap do
        (start_section...(start_section + duration)).each do |section|
          (0...number_of_rooms).each do |room_id|
            @slots[day][section][room_id].each do |class_to_check_id|
              next unless class_to_check_id != class_id

              class_to_check = RegisteredClass.find(class_to_check_id)
              # Check lecturer overlap
              lecturer_overlap = true if !lecturer_overlap && current_class.lecturer_overlap_with?(class_to_check)

              # Check group overlap
              group_overlap = true if !group_overlap && current_class.group_overlap_with?(class_to_check)

              # If both type of overlapping --> no need to check more
              throw :both_overlap if lecturer_overlap && group_overlap
            end
          end
        end
      end

      total_score += 1 unless lecturer_overlap
      # puts "Lecturer Overlaped" if lecturer_overlap

      total_score += 1 unless group_overlap
      # puts "Group Overlaped" if group_overlap
      # puts "Score: #{total_score}"

      # 5th contraint theory not in lab room
      # 6th constraint Course Lab isolated and Laboratory without exclusive room need to be in the room for lab
      lab_class_without_exclusive_not_in_lab_room = false
      theory_in_lab_room = false
      located_room = ClassRoom.find(@rooms_arr[room])
      if current_class.TheoryGroup? && current_class.groupable.course.theory?
        theory_in_lab_room = true if located_room.lab?
      elsif current_class.exclusive_rooms.empty?
        lab_class_without_exclusive_not_in_lab_room = true unless located_room.lab?
      end

      total_score += 1 unless theory_in_lab_room

      total_score += 1 unless lab_class_without_exclusive_not_in_lab_room

      # 7th constraint Class that has group with course has exclusive rooms located need to be in the exclusive room
      class_need_exclusive_but_not_in_it = false
      unless current_class.exclusive_rooms.empty?
        class_need_exclusive_but_not_in_it = true unless current_class.exclusive_rooms.include? located_room
      end

      total_score += 1 unless class_need_exclusive_but_not_in_it

      # Calculate Counts

      lecturer_overlap_count += 1 if lecturer_overlap

      group_overlap_count += 1 if group_overlap

      room_overlap_count += 1 if room_overlap

      not_enough_seats_count += 1 if room_seats < class_seats

      lab_class_without_exclusive_not_in_lab_room_count += 1 if lab_class_without_exclusive_not_in_lab_room

      class_need_exclusive_but_not_in_it_count += 1 if class_need_exclusive_but_not_in_it

      theory_in_lab_room_count += 1 if theory_in_lab_room
    end
    @constraint_count.push(lecturer_overlap_count)
    @constraint_count.push(group_overlap_count)
    @constraint_count.push(room_overlap_count)
    @constraint_count.push(not_enough_seats_count)
    @constraint_count.push(lab_class_without_exclusive_not_in_lab_room_count)
    @constraint_count.push(class_need_exclusive_but_not_in_it_count)
    @constraint_count.push(theory_in_lab_room_count)

    # Calculate the fitness of this schedule
    @score = total_score
  end

  def crossover(partner_schedule)
    # # puts "father: #{@class_positions}"
    # # puts "mother: #{partner_schedule.class_positions}"
    # Initialize new children_schedule
    children_schedule = Schedule.new(@rooms_arr, @classes_arr)

    # Pick the midpoint
    mid_point = rand(0...@number_of_classes)
    # puts "random midpoint: #{mid_point}"
    # Get the right genotype of current schedule
    right_genotype = @class_positions.to_a.slice((mid_point + 1)..(@number_of_classes - 1))

    # Get the left genotype of partner schedule
    left_genotype = partner_schedule.class_positions.to_a.slice(0..mid_point)

    # Combine two genotype to get the genotype of new children
    children_genotype = left_genotype + right_genotype

    # Convert back from array to hash and add to class_positions
    children_schedule.class_positions = children_genotype.to_h
    # # puts "children new genotype: #{children_schedule.class_positions}"
    # Generate new phenotype according to genotype
    children_schedule.generate_phenotype

    # return children schedule
    children_schedule
  end

  def generate_phenotype
    @class_positions.each do |class_id, position|
      day = position[0]
      start_section = position[1]
      room = position[2]

      duration = RegisteredClass.find(class_id).class_sections

      # According to duration, add class to number of slots
      (start_section...(start_section + duration)).each do |section|
        @slots[day][section][room].push(class_id)
      end
    end
  end

  def mutate(mutation_rate)
    @class_positions_arr = @class_positions.to_a
    (0...@number_of_classes).each do |pos|
      next unless rand(0...1.0) < mutation_rate

      # puts "mutation at position #{pos}"
      class_picked = @class_positions_arr[pos]

      # Get information of class and its old position
      class_id = class_picked[0]
      class_position = class_picked[1]

      day = class_position[0]
      start_section = class_position[1]
      room = class_position[2]
      registered_class = RegisteredClass.find(class_id)
      duration = registered_class.class_sections

      # According to duration, remove class_id from old slots
      (start_section...(start_section + duration)).each do |section|
        @slots[day][section][room].delete(class_id)
      end

      # Random new position for this mutated class
      duration = registered_class.class_sections

      # Random attributes
      room_rand = rand_room_index(rooms_arr: @rooms_arr, registered_class: registered_class)
      day_rand = rand(0...@number_of_days)
      start_section_rand = rand_start_section(duration)

      # Combine random attribute into array
      position = [day_rand, start_section_rand, room_rand]

      # Add class position to hash
      @class_positions[class_id] = position

      # According to duration, add class to number of slots
      (start_section_rand...(start_section_rand + duration)).each do |section|
        @slots[day_rand][section][room_rand].push(class_id)
      end
      # puts "Mutated Chomosome: #{@class_positions}"
    end
  end

  private

  def rand_start_section(duration)
    start_section = 0
    if [3, 4, 5].include?(duration)
      start_section = [0, 5].sample
    elsif duration == 2
      start_section = [0, 2, 5, 7].sample
    end
    start_section
  end

  def rand_room_index(rooms_arr:, registered_class:)
    if registered_class.exclusive_rooms.present?
      find_index_by_room_id(rooms_arr: rooms_arr, room_id: registered_class.exclusive_rooms.sample.id)
    elsif registered_class.TheoryGroup? && registered_class.groupable.course.theory?
      find_index_by_room_id(rooms_arr: rooms_arr, room_id: ClassRoom.normal.sample.id)
    elsif !(registered_class.TheoryGroup? && registered_class.groupable.course.theory?)
      find_index_by_room_id(rooms_arr: rooms_arr, room_id: ClassRoom.lab.sample.id)
    else
      find_index_by_room_id(rooms_arr: rooms_arr, room_id: ClassRoom.where('seats >= ?', registered_class.slot).sample.id)
    end
  end

  def find_index_by_room_id(rooms_arr:, room_id:)
    rooms_arr.index(room_id)
  end
end
