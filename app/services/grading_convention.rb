module GradingConvention
  class << self
    def classification_of(grade)
      if (85..100).cover?(grade)
        Semester::FIRST_CLASS
      elsif (77...85).cover?(grade)
        Semester::UPPER_SECOND_CLASS
      elsif (67...77).cover?(grade)
        Semester::LOWER_SECOND_CLASS
      elsif (60...67).cover?(grade)
        Semester::THIRD
      else
        Semester::PASS
      end
    end

    def system_four_of(grade)
      if (60..100).cover?(grade)
        4
      elsif (55...60).cover?(grade)
        3.5
      elsif (50...55).cover?(grade)
        3
      elsif (43...50).cover?(grade)
        2.5
      elsif (35...43).cover?(grade)
        2
      else
        0
      end
    end
  end
end
