class ScheduleGenerator
  def initialize(population_max, mutation_rate)
    @number_of_constraints = 7
    @all_rooms = ClassRoom.ids
    @rooms = @all_rooms
    @classes = Semester.last.registered_classes
    @population_max = population_max
    @mutation_rate = mutation_rate
    @perfect_score = @classes.length * @number_of_constraints
  end

  def generate_schedule_for_current_semester
    @generation = 0
    @best_fit_schedule = -1
    @population = []

    # Initialize Population
    (0...@population_max).each do |index|
      @population[index] = Schedule.new(@rooms, @classes)
      @population[index].randomize_classes
    end

    # Start the generic algorithm
    catch :finished do
      loop do
        puts "Generation #{@generation += 1}"
        sleep 1
        score_sum = 0
        @constraint_count = []
        @number_of_constraints.times do
          @constraint_count.push(0)
        end
        # Calculate Fitness Score and
        # evaluate whether this loop finish or not
        # Calculate the score sum in population
        (0...@population_max).each do |index|
          # After each score calculation, we compare it with the perfect score
          # if equal --> finished --> not calculate anymore --> break
          score = @population[index].calculate_score
          score_sum += score
          @constraint_count = [@constraint_count, @population[index].constraint_count].transpose.map { |x| x.reduce(:+) }
          if score == @perfect_score
            @best_fit_schedule = index
            throw :finished
          end
        end

        puts "Constraint Analysis: #{@constraint_count}"
        output_contraint_count(@constraint_count)
        @avg_fitness_score = score_sum.to_f / (@population_max * @perfect_score)
        puts "Average Fitness Score: #{@avg_fitness_score}"
        sleep 5
        # Crossover N times to create new generation and mutate concurrently
        @new_population = []
        (0...@population_max).each do |_time|
          first_schedule = pick_one_randomly(@population, score_sum)
          second_schedule = pick_one_randomly(@population, score_sum)
          new_schedule = first_schedule.crossover(second_schedule)
          new_schedule.mutate(@mutation_rate)
          @new_population.push(new_schedule)
        end

        @population = @new_population
      end
    end
    best_schedule = @population[@best_fit_schedule]
    p 'Saving Schedule to Database'
    save_schedule_to_database(registered_classes: best_schedule.class_positions, rooms_arr: best_schedule.rooms_arr)
    best_schedule
  end

  def output_contraint_count(constraint_count)
    title = ['Lecturer Overlap', 'Group Overlap', 'Room Overlap', 'Not Enough Seats', 'Lab Class Not In lab room', 'Class Need Exclusive Not in it', 'Theory class in lab room']
    total_count = constraint_count.sum
    max_index = 0
    max_percentage = 0
    constraint_count.each_with_index do |value, index|
      percentage = (value.to_f / total_count) * 100
      puts "#{title[index]} Percentage: #{percentage.round(2)}"
      if percentage > max_percentage
        max_percentage = percentage
        max_index = index
      end
    end
    "Max percentage: #{max_percentage} of #{title[max_index]}"
  end

  private

  def pick_one_randomly(population, score_sum)
    index = -1
    random_number = rand(0...score_sum)
    while random_number >= 0
      index += 1
      random_number -= population[index].score
    end
    population[index]
  end

  def save_schedule_to_database(registered_classes:, rooms_arr:)
    registered_classes.each do |class_id, position|
      day = position[0]
      start_section = position[1]
      room = position[2]
      registered_class = RegisteredClass.find(class_id)
      registered_class.update(start_section: start_section + 1, class_room_id: rooms_arr[room], day: convert_num_to_day(day))
    end
    # implement this to save best fit schedule to database according to its class_positions
  end

  def convert_num_to_day(num)
    dates = [RegisteredClass::MON, RegisteredClass::TUES, RegisteredClass::WEDS, RegisteredClass::THURS, RegisteredClass::FRI, RegisteredClass::SATUR]
    dates[num]
  end
end
