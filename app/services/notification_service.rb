require 'fcm'

class NotificationService
  def initialize
    @fcm = FCM.new(ENV['FCM_SERVER_KEY'])
  end

  def push_notification(registration_ids, options)
    @fcm.send(registration_ids, options)
  end
end
