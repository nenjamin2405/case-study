module TimePeriod
  class << self
    def weeks_for_timetable(classes)
      return nil if classes.blank?

      class_has_longest_duration = classes.first
      end_time = class_has_longest_duration.groupable.end_time
      start_time = class_has_longest_duration.groupable.start_time
      classes.where.not(id: class_has_longest_duration.id).map do |registered_class|
        groupable = registered_class.groupable
        end_time_compare = groupable.end_time
        start_time_compare = groupable.start_time
        end_time = end_time_compare if end_time_compare > end_time
        start_time = start_time_compare if start_time > start_time_compare
      end

      weeks_in_duration(start_time, end_time)
    end

    def timetable_for(week, classes)
      end_date = week.split('-')[1]
      end_date = Date.parse(end_date)
      registered_classes = classes
      timetable = []
      RegisteredClass::DAY_SECTIONS.times do |section|
        object = { section: section + 1 }
        RegisteredClass.days.map do |_key, value|
          registered_class_found = registered_classes.find_by(day: value, start_section: section + 1)
          if registered_class_found
            date_of_this_day = end_date.beginning_of_week(value.to_sym)
            groupable = registered_class_found.groupable
            object[value] = if groupable.start_time <= date_of_this_day && groupable.end_time >= date_of_this_day
                              registered_class_found
                            end
          else
            object[value] = nil
          end
        end
        timetable << object
      end
      timetable
    end

    private

    def weeks_in_duration(start_date, end_date)
      weeks = []
      week_covers_current_time = nil
      week_start_date = nil
      week_end_date = nil
      loop do
        week_start_date = week_end_date.nil? ? start_date.beginning_of_week : week_end_date + 1.day
        week_end_date = week_start_date.end_of_week
        week_duration = "#{week_start_date.strftime(RegisteredClass::DATE_FORMAT)} - #{week_end_date.strftime(RegisteredClass::DATE_FORMAT)}"
        weeks << week_duration
        current_time = Time.now.utc
        week_covers_current_time = week_duration if current_time >= week_start_date && current_time <= week_end_date
        break if week_end_date >= end_date
      end
      if week_covers_current_time.nil?
        week_covers_current_time = Time.now.utc > end_date ? weeks.last : weeks.first
      end
      [weeks, week_covers_current_time]
    end
  end
end
