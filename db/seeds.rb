admin = AdminUser.create!(email: 'quangnhatit14070@gmail.com', password: 'quangnhat', password_confirmation: 'quangnhat')
puts "#Admin #{admin.email} created!"

university = University.create!(code: 'IU', name: 'International University', description: Faker::Lorem.paragraph)
puts "#University #{university.name} created!"

departments = [
  {
    code: 'BA',
    name: 'Business',
    majors: [
      {
        code: 'TWWE',
        name: 'West of England'
      },
      {
        code: 'BA',
        name: 'Business Adminstration'
      },
      {
        code: 'FN',
        name: 'Financial & Banking'
      }
    ]
  },
  {
    code: 'BD',
    name: 'Board of Director'
  },
  {
    code: 'BM',
    name: 'Biomedical Engineering'
  },
  {
    code: 'BT',
    name: 'Bio-Technology'
  },
  {
    code: 'CE',
    name: 'Civil Engineering'
  },
  {
    code: 'EE',
    name: 'Electrical Engineering'
  },
  {
    code: 'EN',
    name: 'English'
  },
  {
    code: 'EVE',
    name: 'Environment Engineering'
  },
  {
    code: 'ISE',
    name: 'Industrial & Systems Engineering'
  },
  {
    code: 'IT',
    name: 'Computer Science & Engineering',
    majors: [
      {
        code: 'CS',
        name: 'Computer Science'
      },
      {
        code: 'NE',
        name: 'Network'
      },
      {
        code: 'CE',
        name: 'Computer Engineering'
      }
    ]
  },
  {
    code: 'MA',
    name: 'Mathematics'
  },
  {
    code: 'PH',
    name: 'Physics'
  }
]

departments.each do |department|
  created_department = Department.create(code: department[:code], name: department[:name], description: Faker::Lorem.paragraph)
  puts "#Department #{created_department.name} created!"

  next if department[:majors].blank?

  department[:majors].each do |major|
    created_major = created_department.majors.create(fee_per_credit: (20.0..70.0).step.to_a.sample, code: major[:code], name: major[:name], description: Faker::Lorem.paragraph)
    puts "#Major #{created_major.name} created!"
  end
end

# Lecturers
fake_lecturer = Lecturer.create!(
  first_name: 'Nhat',
  last_name: 'Truong',
  email: 'nhat-lecturer@iu.io',
  gender: 1,
  address: Faker::Address.full_address,
  salary: Faker::Number.decimal(4, 3),
  date_of_birth: Faker::Date.birthday(18, 20),
  phone_number: Faker::PhoneNumber.cell_phone,
  password: 'secret',
  password_confirmation: 'secret',
  confirmed_at: Time.now.utc
)
fake_lecturer.departments << Department.all.sample(rand(1..2))
fake_lecturer.save!

150.times do |n|
  lecturer = Lecturer.new(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    email: "professor-#{n}" + Faker::Internet.email,
    gender: [0, 1].sample,
    address: Faker::Address.full_address,
    date_of_birth: Faker::Date.birthday(23, 60),
    phone_number: Faker::PhoneNumber.cell_phone,
    salary: Faker::Number.decimal(4, 3),
    password: 'quangnhat',
    password_confirmation: 'quangnhat',
    confirmed_at: Time.now.utc
  )
  lecturer.departments << Department.all.sample(rand(1..2))
  lecturer.save!
  puts "#Lecturer #{lecturer.email} created!"
end

# Students
Student.create!(
  first_name: 'Nhat',
  last_name: 'Truong',
  email: 'nhat-student@iu.io',
  gender: 1,
  address: Faker::Address.full_address,
  date_of_birth: Faker::Date.birthday(18, 20),
  phone_number: Faker::PhoneNumber.cell_phone,
  major: Major.find_by(code: 'CS'),
  supervisor: Lecturer.all.sample,
  entering_year: 2018,
  password: 'secret',
  password_confirmation: 'secret',
  confirmed_at: Time.now.utc
)
5.times do |n|
  student = Student.new(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    email: "student-#{n}" + Faker::Internet.email,
    gender: [0, 1].sample,
    address: Faker::Address.full_address,
    date_of_birth: Faker::Date.birthday(18, 20),
    phone_number: Faker::PhoneNumber.cell_phone,
    major: Major.all.sample,
    supervisor: Lecturer.all.sample,
    entering_year: 2018,
    password: 'quangnhat',
    password_confirmation: 'quangnhat',
    confirmed_at: Time.now.utc
  )
  student.save!
  puts "#Student #{student.email} created!"
end

# Rooms

['S', 'M', 'L'].each do |size|
  seats = 0
  seats = if size.eql? 'S'
            100
          elsif size.eql? 'M'
            150
          else
            200
          end
  100.times.each do |n|
    campus = ['A', 'P', 'L'].sample
    class_room_created = ClassRoom.new(
      code: "#{size}#{n}-#{campus} Campus",
      seats: seats,
      building: ['A1', 'A2'].sample,
      campus: campus,
      category: [ClassRoom::CATEGORY_LAB, ClassRoom::CATEGORY_NORMAL, ClassRoom::CATEGORY_NORMAL, ClassRoom::CATEGORY_EXCLUSIVE].sample
    )

    class_room_created.save!
    puts "#Class room #{class_room_created.code} created!"
  end
end

# Courses

courses = []
190.times do |_n|
  courses.push(Faker::Educator.course_name)
end

courses.uniq!
courses.each do |course|
  credits = [2, 3, 4].sample
  course_created = Course.new(
    gpa_calculated: [true, true, true, true, false].sample,
    name: course,
    code: 'IU2018-' + course.split(' ').map { |word| word[0] }.join('').upcase,
    description: Faker::Lorem.paragraph,
    credits: credits,
    class_sections: credits,
    can_be_enroll_from_year: [1, 2, 3, 4].sample,
    category: [Course::CATEGORY_LAB_ISOLATED, Course::CATEGORY_THEORY, Course::CATEGORY_THEORY, Course::CATEGORY_THEORY, Course::CATEGORY_THEORY, Course::CATEGORY_THEORY].sample
  )
  course_created.majors << Major.all.sample(rand(1..2))
  course_created.save!
  puts "#Course #{course} created!"
  # @TODO add prerequisites
  if course_created.lab_isolated?
    CourseClassRoom.create!(coursable: course_created, class_room: ClassRoom.exclusive.sample)
  end
  # Create Laboratory
  next if course_created.credits != 3 || course_created.lab_isolated?

  lab_created = course_created.create_laboratory(
    code: course_created.code + '-LAB',
    name: course_created.name + ' Lab',
    description: Faker::Lorem.paragraph,
    class_sections: 4,
    credits: 1
  )
  puts "#Lab #{lab_created.name} created!"

  if [1, 2].sample == 1
    CourseClassRoom.create!(coursable: lab_created, class_room: ClassRoom.exclusive.sample)
  end
end

# Semesters

[2017, 2018, 2019].each do |start_year|
  [Semester::CATEGORY_SUMMER, Semester::CATEGORY_SPRING, Semester::CATEGORY_FALL].each do |semester_type|
    semester_created = Semester.new(
      category: semester_type,
      usd_to_vnd_rate: 23_760.0,
      start_year: start_year,
      end_year: start_year + 1
    )
    semester_created.save!
    puts "#Semester #{semester_created.category} #{semester_created.start_year}-#{semester_created.end_year} created!"
  end
end

# Theory Group

semester = Semester.last

Course.all.sample(23).each do |course|
  rand_slot = course.class_rooms.empty? ? [50, 60, 20, 80, 120, 100, 90, 35, 140].sample : course.class_rooms.pluck(:seats).min
  start_time = DateTime.current + 2.months
  end_time = start_time + 4.months
  theory_group_created = TheoryGroup.new(
    leader: Lecturer.all.sample,
    course: course,
    semester: semester,
    start_time: start_time,
    end_time: end_time,
    slot: rand_slot,
    remaining_slot: rand_slot
  )
  theory_group_created.save!
  puts "#Theory Group for course #{course.name} created!"

  # Laboratory Group
  next unless course.contains_lab?

  amount_of_lab_groups = [1, 2].sample
  puts "This theory group has #{amount_of_lab_groups} lab groups"
  slot_for_lab = rand_slot / amount_of_lab_groups
  amount_of_lab_groups.times do |n|
    lab_group_created = theory_group_created.lab_groups.new(
      laboratory: course.laboratory,
      lecturer: Lecturer.all.sample,
      start_time: start_time,
      end_time: end_time,
      slot: slot_for_lab,
      remaining_slot: slot_for_lab
    )
    lab_group_created.save!
    puts "Lab group time #{n + 1} created"
  end
end

# Generate Schedule
@schedule = ScheduleGenerator.new(20, 0.01).generate_schedule_for_current_semester
