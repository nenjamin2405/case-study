# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_15_140331) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "class_rooms", force: :cascade do |t|
    t.string "code"
    t.integer "seats"
    t.string "building"
    t.string "campus"
    t.string "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade do |t|
    t.string "author_type"
    t.bigint "author_id"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "discussion_id"
    t.index ["author_type", "author_id"], name: "index_comments_on_author_type_and_author_id"
    t.index ["discussion_id"], name: "index_comments_on_discussion_id"
  end

  create_table "course_class_rooms", force: :cascade do |t|
    t.string "coursable_type"
    t.bigint "coursable_id"
    t.bigint "class_room_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["class_room_id"], name: "index_course_class_rooms_on_class_room_id"
    t.index ["coursable_type", "coursable_id"], name: "index_course_class_rooms_on_coursable_type_and_coursable_id"
  end

  create_table "courses", force: :cascade do |t|
    t.boolean "gpa_calculated", default: true
    t.string "code"
    t.string "name"
    t.text "description"
    t.integer "credits"
    t.integer "class_sections"
    t.string "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "can_be_enroll_from_year", default: 1
  end

  create_table "courses_majors", id: false, force: :cascade do |t|
    t.bigint "major_id"
    t.bigint "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_courses_majors_on_course_id"
    t.index ["major_id"], name: "index_courses_majors_on_major_id"
  end

  create_table "departments", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "departments_lecturers", id: false, force: :cascade do |t|
    t.bigint "lecturer_id"
    t.bigint "department_id"
    t.index ["department_id"], name: "index_departments_lecturers_on_department_id"
    t.index ["lecturer_id"], name: "index_departments_lecturers_on_lecturer_id"
  end

  create_table "discussions", force: :cascade do |t|
    t.string "author_type"
    t.bigint "author_id"
    t.text "content"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "registered_class_id"
    t.string "slug"
    t.string "category", default: "discussion"
    t.boolean "is_comments_enabled", default: true
    t.index ["author_type", "author_id"], name: "index_discussions_on_author_type_and_author_id"
    t.index ["registered_class_id"], name: "index_discussions_on_registered_class_id"
    t.index ["slug"], name: "index_discussions_on_slug", unique: true
  end

  create_table "enrolments", force: :cascade do |t|
    t.bigint "student_id"
    t.string "status"
    t.bigint "semester_id"
    t.bigint "registered_class_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "fee"
    t.integer "credits"
    t.index ["registered_class_id"], name: "index_enrolments_on_registered_class_id"
    t.index ["semester_id"], name: "index_enrolments_on_semester_id"
    t.index ["student_id"], name: "index_enrolments_on_student_id"
  end

  create_table "friendly_id_slugs", id: :serial, force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "grade_columns", force: :cascade do |t|
    t.bigint "registered_class_id"
    t.string "name"
    t.float "percentage"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_published", default: false
    t.index ["registered_class_id"], name: "index_grade_columns_on_registered_class_id"
  end

  create_table "grades", force: :cascade do |t|
    t.bigint "grade_column_id"
    t.bigint "student_id"
    t.float "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["grade_column_id"], name: "index_grades_on_grade_column_id"
    t.index ["student_id"], name: "index_grades_on_student_id"
  end

  create_table "lab_groups", force: :cascade do |t|
    t.bigint "laboratory_id"
    t.bigint "theory_group_id"
    t.integer "slot"
    t.integer "remaining_slot"
    t.datetime "start_time"
    t.datetime "end_time"
    t.bigint "lecturer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["laboratory_id"], name: "index_lab_groups_on_laboratory_id"
    t.index ["lecturer_id"], name: "index_lab_groups_on_lecturer_id"
    t.index ["theory_group_id"], name: "index_lab_groups_on_theory_group_id"
  end

  create_table "laboratories", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.text "description"
    t.integer "credits"
    t.integer "class_sections"
    t.bigint "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_laboratories_on_course_id"
  end

  create_table "lecturers", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean "allow_password_change", default: false
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "nickname"
    t.string "image"
    t.string "email"
    t.integer "gender", default: 0
    t.string "first_name"
    t.string "last_name"
    t.string "address"
    t.datetime "date_of_birth"
    t.float "salary", default: 0.0
    t.string "phone_number"
    t.json "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "avatar", default: "default-avatar_bytklu.png"
    t.string "noti_keys", default: [], array: true
    t.index ["confirmation_token"], name: "index_lecturers_on_confirmation_token", unique: true
    t.index ["email"], name: "index_lecturers_on_email", unique: true
    t.index ["reset_password_token"], name: "index_lecturers_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_lecturers_on_uid_and_provider", unique: true
  end

  create_table "majors", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.text "description"
    t.bigint "department_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "fee_per_credit", default: 0.0
    t.index ["department_id"], name: "index_majors_on_department_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.string "actor_type"
    t.bigint "actor_id"
    t.string "receiver_type"
    t.bigint "receiver_id"
    t.datetime "read_at"
    t.datetime "seen_at"
    t.string "action"
    t.string "notifiable_type"
    t.bigint "notifiable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["actor_type", "actor_id"], name: "index_notifications_on_actor_type_and_actor_id"
    t.index ["notifiable_type", "notifiable_id"], name: "index_notifications_on_notifiable_type_and_notifiable_id"
    t.index ["receiver_type", "receiver_id"], name: "index_notifications_on_receiver_type_and_receiver_id"
  end

  create_table "prerequisite_courses", force: :cascade do |t|
    t.integer "prereq_id"
    t.integer "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_prerequisite_courses_on_course_id"
    t.index ["prereq_id", "course_id"], name: "index_prerequisite_courses_on_prereq_id_and_course_id", unique: true
    t.index ["prereq_id"], name: "index_prerequisite_courses_on_prereq_id"
  end

  create_table "registered_classes", force: :cascade do |t|
    t.string "day"
    t.integer "start_section"
    t.bigint "class_room_id"
    t.integer "groupable_id"
    t.string "groupable_type"
    t.integer "class_sections"
    t.integer "slot"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.boolean "gpa_calculated", default: true
    t.index ["class_room_id"], name: "index_registered_classes_on_class_room_id"
    t.index ["groupable_id", "groupable_type"], name: "index_registered_classes_on_groupable_id_and_groupable_type"
    t.index ["slug"], name: "index_registered_classes_on_slug", unique: true
  end

  create_table "registration_times", force: :cascade do |t|
    t.datetime "date"
    t.time "start_time"
    t.time "end_time"
    t.bigint "semester_id"
    t.integer "from_year"
    t.integer "to_year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["semester_id"], name: "index_registration_times_on_semester_id"
  end

  create_table "semesters", force: :cascade do |t|
    t.string "category"
    t.float "usd_to_vnd_rate"
    t.integer "start_year"
    t.integer "end_year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "student_school_fees", force: :cascade do |t|
    t.bigint "semester_id"
    t.bigint "student_id"
    t.float "fee", default: 0.0
    t.float "paid", default: 0.0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "remit", default: 0.0
    t.index ["semester_id"], name: "index_student_school_fees_on_semester_id"
    t.index ["student_id"], name: "index_student_school_fees_on_student_id"
  end

  create_table "students", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean "allow_password_change", default: false
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "nickname"
    t.string "image"
    t.string "email"
    t.bigint "major_id"
    t.bigint "supervisor_id"
    t.float "gpa", default: 0.0
    t.integer "semester_count", default: 0
    t.integer "entering_year"
    t.string "first_name"
    t.string "last_name"
    t.datetime "date_of_birth"
    t.string "phone_number"
    t.json "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "address"
    t.integer "gender", default: 0
    t.string "avatar", default: "default-avatar_bytklu.png"
    t.string "noti_keys", default: [], array: true
    t.index ["confirmation_token"], name: "index_students_on_confirmation_token", unique: true
    t.index ["email"], name: "index_students_on_email", unique: true
    t.index ["major_id"], name: "index_students_on_major_id"
    t.index ["reset_password_token"], name: "index_students_on_reset_password_token", unique: true
    t.index ["supervisor_id"], name: "index_students_on_supervisor_id"
    t.index ["uid", "provider"], name: "index_students_on_uid_and_provider", unique: true
  end

  create_table "theory_groups", force: :cascade do |t|
    t.bigint "course_id"
    t.bigint "semester_id"
    t.bigint "lecturer_id"
    t.integer "slot"
    t.integer "remaining_slot"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_theory_groups_on_course_id"
    t.index ["lecturer_id"], name: "index_theory_groups_on_lecturer_id"
    t.index ["semester_id"], name: "index_theory_groups_on_semester_id"
  end

  create_table "universities", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean "allow_password_change", default: false
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "nickname"
    t.string "image"
    t.string "email"
    t.json "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  add_foreign_key "comments", "discussions"
  add_foreign_key "discussions", "registered_classes"
  add_foreign_key "enrolments", "registered_classes"
  add_foreign_key "enrolments", "semesters"
  add_foreign_key "enrolments", "students"
  add_foreign_key "grade_columns", "registered_classes"
  add_foreign_key "grades", "grade_columns"
  add_foreign_key "grades", "students"
  add_foreign_key "lab_groups", "laboratories"
  add_foreign_key "lab_groups", "lecturers"
  add_foreign_key "lab_groups", "theory_groups"
  add_foreign_key "laboratories", "courses"
  add_foreign_key "majors", "departments"
  add_foreign_key "registration_times", "semesters"
  add_foreign_key "student_school_fees", "semesters"
  add_foreign_key "student_school_fees", "students"
  add_foreign_key "students", "lecturers", column: "supervisor_id"
  add_foreign_key "students", "majors"
  add_foreign_key "theory_groups", "courses"
  add_foreign_key "theory_groups", "lecturers"
  add_foreign_key "theory_groups", "semesters"
end
