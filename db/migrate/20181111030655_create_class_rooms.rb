class CreateClassRooms < ActiveRecord::Migration[5.2]
  def change
    create_table :class_rooms do |t|
      t.string :code
      t.integer :seats
      t.string :building
      t.string :campus
      t.string :category

      t.timestamps
    end
  end
end
