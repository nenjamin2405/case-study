class AddRelationshipForLecturerAndDepartment < ActiveRecord::Migration[5.2]
  def change
    create_table :departments_lecturers, id: false do |t|
      t.belongs_to :lecturer, index: true
      t.belongs_to :department, index: true
    end
  end
end
