class AddMajorCourseRelationship < ActiveRecord::Migration[5.2]
  def change
    create_table :courses_majors, id: false do |t|
      t.belongs_to :major, index: true
      t.belongs_to :course, index: true
      t.timestamps
    end
  end
end
