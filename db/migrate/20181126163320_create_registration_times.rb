class CreateRegistrationTimes < ActiveRecord::Migration[5.2]
  def change
    create_table :registration_times do |t|
      t.datetime :date
      t.time :start_time
      t.time :end_time
      t.references :semester, foreign_key: true
      t.integer :from_year
      t.integer :to_year

      t.timestamps
    end
  end
end
