class CreateLaboratories < ActiveRecord::Migration[5.2]
  def change
    create_table :laboratories do |t|
      t.string :code
      t.string :name
      t.text :description
      t.integer :credits
      t.integer :class_sections
      t.references :course, foreign_key: true

      t.timestamps
    end
  end
end
