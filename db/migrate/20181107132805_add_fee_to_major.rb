class AddFeeToMajor < ActiveRecord::Migration[5.2]
  def change
    add_column :majors, :fee_per_credit, :float, default: 0
  end
end
