class AddFeeToEnrolment < ActiveRecord::Migration[5.2]
  def change
    add_column :enrolments, :fee, :float
  end
end
