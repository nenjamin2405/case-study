class CreateGrades < ActiveRecord::Migration[5.2]
  def change
    create_table :grades do |t|
      t.references :grade_column, foreign_key: true
      t.references :student, foreign_key: true
      t.float :value

      t.timestamps
    end
  end
end
