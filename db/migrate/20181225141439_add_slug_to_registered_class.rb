class AddSlugToRegisteredClass < ActiveRecord::Migration[5.2]
  def change
    add_column :registered_classes, :slug, :string
    add_index :registered_classes, :slug, unique: true
  end
end
