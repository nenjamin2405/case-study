class CreateLabGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :lab_groups do |t|
      t.references :laboratory, foreign_key: true
      t.references :theory_group, foreign_key: true
      t.integer :slot
      t.integer :remaining_slot
      t.datetime :start_time
      t.datetime :end_time
      t.references :lecturer, foreign_key: true

      t.timestamps
    end
  end
end
