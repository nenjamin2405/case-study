class CreateEnrolments < ActiveRecord::Migration[5.2]
  def change
    create_table :enrolments do |t|
      t.references :student, foreign_key: true
      t.string :status
      t.references :semester, foreign_key: true
      t.references :registered_class, foreign_key: true

      t.timestamps
    end
  end
end
