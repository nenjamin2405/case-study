class AddCreditsToEnrolment < ActiveRecord::Migration[5.2]
  def change
    add_column :enrolments, :credits, :integer
  end
end
