class CreateStudentSchoolFees < ActiveRecord::Migration[5.2]
  def change
    create_table :student_school_fees do |t|
      t.references :semester, foreign_key: true
      t.references :student, foreign_key: true
      t.float :fee
      t.float :paid

      t.timestamps
    end
  end
end
