class CreateSemesters < ActiveRecord::Migration[5.2]
  def change
    create_table :semesters do |t|
      t.string :category
      t.float :usd_to_vnd_rate
      t.integer :start_year
      t.integer :end_year

      t.timestamps
    end
  end
end
