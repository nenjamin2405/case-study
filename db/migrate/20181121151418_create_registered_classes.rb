class CreateRegisteredClasses < ActiveRecord::Migration[5.2]
  def change
    create_table :registered_classes do |t|
      t.string :day
      t.integer :start_section
      t.belongs_to :class_room, index: true
      t.integer :groupable_id
      t.string  :groupable_type
      t.integer :class_sections
      t.integer :slot
      t.timestamps
    end

    add_index :registered_classes, [:groupable_id, :groupable_type]
  end
end
