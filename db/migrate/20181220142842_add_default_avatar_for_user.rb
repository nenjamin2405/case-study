class AddDefaultAvatarForUser < ActiveRecord::Migration[5.2]
  def change
    change_column :students, :avatar, :string, default: 'default-avatar_bytklu.png'
    change_column :lecturers, :avatar, :string, default: 'default-avatar_bytklu.png'
  end
end
