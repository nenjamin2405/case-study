class CreatePrerequisiteCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :prerequisite_courses do |t|
      t.integer :prereq_id
      t.integer :course_id

      t.timestamps
    end
    add_index :prerequisite_courses, :prereq_id
    add_index :prerequisite_courses, :course_id
    add_index :prerequisite_courses, [:prereq_id, :course_id], unique: true
  end
end
