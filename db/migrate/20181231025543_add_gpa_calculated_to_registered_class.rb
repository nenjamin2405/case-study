class AddGpaCalculatedToRegisteredClass < ActiveRecord::Migration[5.2]
  def change
    add_column :registered_classes, :gpa_calculated, :boolean, default: true
  end
end
