class AddRemitToSchoolFee < ActiveRecord::Migration[5.2]
  def change
    add_column :student_school_fees, :remit, :float
  end
end
