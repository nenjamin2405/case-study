class CreateTheoryGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :theory_groups do |t|
      t.references :course, foreign_key: true
      t.references :semester, foreign_key: true
      t.references :lecturer, foreign_key: true
      t.integer :slot
      t.integer :remaining_slot
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps
    end
  end
end
