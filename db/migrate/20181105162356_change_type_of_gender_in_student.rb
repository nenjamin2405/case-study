class ChangeTypeOfGenderInStudent < ActiveRecord::Migration[5.2]
  def change
    remove_column :students, :gender
    add_column :students, :gender, :integer, default: 0
  end
end
