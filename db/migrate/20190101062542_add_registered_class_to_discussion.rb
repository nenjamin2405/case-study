class AddRegisteredClassToDiscussion < ActiveRecord::Migration[5.2]
  def change
    add_reference :discussions, :registered_class, foreign_key: true
  end
end
