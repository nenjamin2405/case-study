class CreateDicussions < ActiveRecord::Migration[5.2]
  def change
    create_table :discussions do |t|
      t.references :author, polymorphic: true
      t.text :content
      t.string :title

      t.timestamps
    end
  end
end
