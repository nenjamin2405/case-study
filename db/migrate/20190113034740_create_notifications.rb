class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.references :actor, polymorphic: true
      t.references :receiver, polymorphic: true
      t.datetime :read_at
      t.datetime :seen_at
      t.string :action
      t.references :notifiable, polymorphic: true

      t.timestamps
    end
  end
end
