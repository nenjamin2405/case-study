class CreateGradeColumns < ActiveRecord::Migration[5.2]
  def change
    create_table :grade_columns do |t|
      t.references :registered_class, foreign_key: true
      t.string :name
      t.float :percentage

      t.timestamps
    end
  end
end
