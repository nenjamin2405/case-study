class AddNotiKeysToStudentAndLecturer < ActiveRecord::Migration[5.2]
  def change
    add_column :students, :noti_keys, :string, array: true, default: []
    add_column :lecturers, :noti_keys, :string, array: true, default: []
  end
end
