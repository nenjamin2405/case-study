class ChangeDefaultValueOfFee < ActiveRecord::Migration[5.2]
  def change
    change_column :student_school_fees, :fee, :float, default: 0
    change_column :student_school_fees, :paid, :float, default: 0
    change_column :student_school_fees, :remit, :float, default: 0
  end
end
