class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.boolean :gpa_calculated, default: true
      t.string :code
      t.string :name
      t.text :description
      t.integer :credits
      t.integer :class_sections
      t.string :category
      t.timestamps
    end
  end
end
