class AddIsPublishedToGradeColumn < ActiveRecord::Migration[5.2]
  def change
    add_column :grade_columns, :is_published, :boolean, default: false
  end
end
