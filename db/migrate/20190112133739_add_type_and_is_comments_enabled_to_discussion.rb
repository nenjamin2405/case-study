class AddTypeAndIsCommentsEnabledToDiscussion < ActiveRecord::Migration[5.2]
  def change
    add_column :discussions, :category, :string, default: Discussion::CATEGORY_DISCUSSION
    add_column :discussions, :is_comments_enabled, :boolean, default: true
  end
end
