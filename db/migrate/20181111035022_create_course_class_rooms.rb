class CreateCourseClassRooms < ActiveRecord::Migration[5.2]
  def change
    create_table :course_class_rooms do |t|
      t.references :coursable, polymorphic: true, index: true
      t.references :class_room, index: true
      t.timestamps
    end
  end
end
