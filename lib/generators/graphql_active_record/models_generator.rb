module GraphqlActiveRecord
  module Generators
    class ModelsGenerator < Rails::Generators::Base
      def generate_types_bases_on_models
        ApplicationRecord.descendants.collect do |type|
          generate "graphql_active_record:object #{type.name}"
        end
      end
    end
  end
end
