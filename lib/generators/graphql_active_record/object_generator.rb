module GraphqlActiveRecord
  module Generators
    class ObjectGenerator < Rails::Generators::NamedBase
      source_root File.expand_path('templates', __dir__)

      def copy_object_type_file
        template 'object_type.erb', "app/graphql/types/#{file_name}.rb", '-'
      end

      private

      def convert_type(field_name, field_type)
        if field_name == 'id'
          'ID'
        else
          case field_type
          when :datetime
            'GraphQL::Types::ISO8601DateTime'
          when :text
            'String'
          else
            field_type.capitalize
          end
        end
      end
    end
  end
end
