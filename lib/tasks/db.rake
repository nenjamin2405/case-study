namespace :db do
  desc 'Drop, create, migrate then seed the development database'
  task :remove_schema do
    exec 'rm db/schema.rb -f'
  end
  task reseed: ['db:drop', 'db:create', 'db:migrate', 'db:seed'] do
    puts 'Reseeding completed.'
  end
end
