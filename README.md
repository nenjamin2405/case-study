# INTERNATIONAL UNIVERSITY - THESIS PROJECT - EDUCATION SUPPORT SYSTEM FOR UNIVERSITY DOCUMENTATION - CASE STUDY SYSTEM 

> Case Study system is a combination of existing Edusoft and Blackboard system with improvements, particularly in the enhancement of the user interface and user experience.

## LIVE APP INFORMATION

1.	Demo Clip Url: https://youtu.be/-yaoeasEQYI
2.	Domain: https://iu-case-study.herokuapp.com
3.	Admin Site: https://iu-case-study.herokuapp.com/admin
5.	Credentials for testing:

| Title        | Email/Password  
| ------------- |:-------------:
| Admin      | nhat-admin@iu.io / secret
| Lecturer      | nhat-lecturer@iu.io / secret      
| Student | nhat-student@iu.io / secret      

