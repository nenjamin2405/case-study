importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js')

const config = {
  apiKey: 'AIzaSyDXvy-Z-DspHepoKrcYDK2F_uqJ1Tfrbnc',
  authDomain: 'iu-case-study.firebaseapp.com',
  databaseURL: 'https://iu-case-study.firebaseio.com',
  projectId: 'iu-case-study',
  storageBucket: 'iu-case-study.appspot.com',
  messagingSenderId: '585607279575',
}

firebase.initializeApp(config)

const messaging = firebase.messaging()
