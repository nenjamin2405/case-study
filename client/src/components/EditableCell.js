import React from 'react'
import { Input, Form, InputNumber } from 'antd'
import { omit } from 'lodash'
import { EditableContext } from './EditableRow'

const FormItem = Form.Item

export default class EditableCell extends React.Component {
  state = {
    editing: false,
  }

  componentDidMount() {
    if (this.props.editable) {
      document.addEventListener('click', this.handleClickOutside, true)
    }
  }

  componentWillUnmount() {
    if (this.props.editable) {
      document.removeEventListener('click', this.handleClickOutside, true)
    }
  }

  getInput = props => {
    if (this.props.inputType === 'number') {
      return <InputNumber {...props} min={0} max={100} />
    }
    return <Input {...props} />
  }

  toggleEdit = () => {
    this.setState(
      prev => ({ editing: !prev.editing }),
      () => {
        if (this.state.editing) {
          this.input.focus()
        }
      }
    )
  }

  handleClickOutside = e => {
    const { editing } = this.state
    if (editing && this.cell !== e.target && !this.cell.contains(e.target)) {
      this.save()
    }
  }

  save = () => {
    const { record, handleSave } = this.props
    this.form.validateFields((error, values) => {
      if (error) {
        return
      }
      this.toggleEdit()
      handleSave({ ...record, ...values })
    })
  }

  render() {
    const { editing } = this.state
    const { editable, dataIndex, title, record, ...restProps } = this.props
    return (
      <td
        ref={node => (this.cell = node)}
        {...omit(restProps, ['handleSave', 'inputType'])}
      >
        {editable ? (
          <EditableContext.Consumer>
            {form => {
              this.form = form
              return editing ? (
                <FormItem style={{ margin: 0 }}>
                  {form.getFieldDecorator(dataIndex, {
                    rules: [
                      {
                        required: true,
                        message: `${title} is required.`,
                      },
                    ],
                    initialValue: record[dataIndex],
                  })(
                    this.getInput({
                      ref: node => (this.input = node),
                      onPressEnter: this.save,
                    })
                  )}
                </FormItem>
              ) : (
                <div
                  className="editable-cell-value-wrap"
                  style={{ paddingRight: 24 }}
                  onClick={this.toggleEdit}
                >
                  {restProps.children}
                </div>
              )
            }}
          </EditableContext.Consumer>
        ) : (
          restProps.children
        )}
      </td>
    )
  }
}
