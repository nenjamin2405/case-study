import React from 'react'
import { Popover } from 'antd'

export class CustomPopover extends React.Component {
  constructor() {
    super()
    this.state = {
      show: false,
    }
  }

  convertContent = content => <span onClick={this.mouseOut}>{content}</span>

  handleVisibleChange = show => {
    this.setState({ show })
  }

  mouseOver = () => {
    this.setState({
      show: true,
    })
  }

  mouseOut = () => {
    this.setState({
      show: false,
    })
  }

  render() {
    const { children, content, ...restProps } = this.props
    return (
      <Popover
        visible={this.state.show}
        content={this.convertContent(content)}
        trigger="hover"
        onVisibleChange={this.handleVisibleChange}
        {...restProps}
      >
        {children}
      </Popover>
    )
  }
}
