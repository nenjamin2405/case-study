import { Query } from 'react-apollo'
import React from 'react'
import { showErrorModal } from '../lib/showErrorModal'

const CustomQuery = ({ children, ...props }) => (
  <Query {...props}>
    {({ loading, error, data, client }) => {
      if (error) return showErrorModal(error)
      return children({ loading, data, client })
    }}
  </Query>
)

export default CustomQuery
