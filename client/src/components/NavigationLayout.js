import React from 'react'
import { Layout } from 'antd'

import { Query } from 'react-apollo'
import TopHeader from './navigationLayout/TopHeader'
import StudentMenu from './navigationLayout/StudentMenu'
import { ROLES } from '../constants/roles'
import LecturerMenu from './navigationLayout/LecturerMenu'
import { GET_CURRENT_USER } from '../graphql/user'
import { APP_LOGO } from '../constants/app'

const { Content, Footer, Sider } = Layout

const NavigationLayout = ({ hidden, children }) => {
  if (hidden) return <div>{children}</div>
  return (
    <React.Fragment>
      <Layout>
        <Sider
          style={{
            overflow: 'auto',
            height: '100vh',
            position: 'fixed',
            left: 0,
            backgroundColor: 'white',
          }}
        >
          <img
            src={APP_LOGO}
            style={{
              height: 100,
              width: 'auto',
              display: 'flex',
              margin: '20px auto',
            }}
            className="logo"
            alt="logo"
          />
          <Query query={GET_CURRENT_USER}>
            {({ data: { currentUser } }) =>
              currentUser.__typename === ROLES.STUDENT ? (
                <StudentMenu />
              ) : (
                <LecturerMenu />
              )
            }
          </Query>
        </Sider>
        <Layout style={{ marginLeft: 200 }}>
          <TopHeader />
          <Content
            style={{
              margin: '24px 16px 0',
              overflow: 'initial',
              paddingTop: '4%',
            }}
          >
            <div
              style={{
                padding: 24,
                background: '#fff',
                minHeight: '100vh',
              }}
            >
              {children}
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            IU Case Study ©2018 Created by Nhat Truong
          </Footer>
        </Layout>
      </Layout>
    </React.Fragment>
  )
}
export default NavigationLayout
