import React from 'react'
import { Layout, Menu, Dropdown, Icon, Badge } from 'antd'
import { Image } from 'cloudinary-react'
import $ from 'jquery'
import { Query, withApollo } from 'react-apollo'
import { withRouter } from 'react-router'
import './TopHeader.scss'
import moment from 'moment'
import {
  GET_CURRENT_USER,
  GET_CURRENT_USER_NOTIFICATIONS,
  MARK_ALL_SEEN_FOR_NOTIFICATIONS,
} from '../../graphql/user'
import { CLOUD_NAME } from '../../constants/app'
import ChangeAvatarModal from './topHeader/ChangeAvatarModal'
import CustomQuery from '../CustomQuery'
import { generateNotificationItemContent } from '../../utils/notification'
import NotificationService from '../../services/NotificationService'

const { Header } = Layout

class TopHeader extends React.Component {
  logOut = () => {
    const notiService = new NotificationService(
      this.props.client,
      this.props.history
    )
    notiService.unregisterToken()
    $.ajax({
      type: 'DELETE',
      url: '/student_auth/sign_out',
      data: JSON.parse(localStorage.user),
    })
      .done(() => {
        localStorage.removeItem('user')
        this.props.client.resetStore()
        this.props.history.push('/login')
      })
      .fail(() => {
        $.ajax({
          type: 'DELETE',
          url: '/lecturer_auth/sign_out',
          data: JSON.parse(localStorage.user),
        }).done(() => {
          localStorage.removeItem('user')
          this.props.client.resetStore()
          this.props.history.push('/login')
        })
      })
  }

  render() {
    const { history, client } = this.props
    return (
      <Query query={GET_CURRENT_USER}>
        {({ data }) => {
          const { currentUser } = data
          return (
            <Header className="top-header">
              <span
                className="notification-icon"
                onClick={() => {
                  const notisData = client.readQuery({
                    query: GET_CURRENT_USER_NOTIFICATIONS,
                  })
                  client.writeQuery({
                    query: GET_CURRENT_USER_NOTIFICATIONS,
                    data: {
                      notifications: notisData.notifications.map(
                        notification => ({
                          ...notification,
                          seenAt: moment(),
                        })
                      ),
                    },
                  })
                  client.mutate({
                    mutation: MARK_ALL_SEEN_FOR_NOTIFICATIONS,
                    variables: {
                      input: {},
                    },
                  })
                }}
              >
                <CustomQuery query={GET_CURRENT_USER_NOTIFICATIONS}>
                  {({ data: dataNotification, loading }) => {
                    if (loading) return 'Loading...'
                    const { notifications } = dataNotification
                    return (
                      <Dropdown
                        overlay={
                          <Menu>
                            {notifications.map(notification =>
                              generateNotificationItemContent(
                                notification,
                                history,
                                client
                              )
                            )}
                          </Menu>
                        }
                        placement="bottomRight"
                        trigger={['click']}
                      >
                        <Badge
                          count={
                            notifications.filter(
                              notification => !notification.seenAt
                            ).length
                          }
                        >
                          <Icon type="bell" />
                        </Badge>
                      </Dropdown>
                    )
                  }}
                </CustomQuery>
              </span>

              <Dropdown
                overlay={
                  <Menu className="menu-dropdown" onClick={() => {}}>
                    <Menu.Item key="changeAvatar">
                      <div>
                        <ChangeAvatarModal />
                      </div>
                    </Menu.Item>
                    <Menu.Divider />
                    <Menu.Item key="logout" onClick={this.logOut}>
                      <Icon type="logout" />
                      Logout
                    </Menu.Item>
                  </Menu>
                }
                trigger={['click']}
              >
                <div className="ant-dropdown-link user-dropdown" href="#">
                  <Image
                    cloudName={CLOUD_NAME}
                    publicId={currentUser.avatar}
                    width="32"
                    height="32"
                    crop="scale"
                    style={{
                      borderRadius: '50%',

                      marginRight: 10,
                    }}
                  />
                  <b style={{ color: 'gray' }}>{currentUser.fullName}</b>
                </div>
              </Dropdown>
            </Header>
          )
        }}
      </Query>
    )
  }
}

export default withApollo(withRouter(TopHeader))
