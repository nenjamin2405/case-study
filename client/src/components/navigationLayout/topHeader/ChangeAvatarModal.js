import { Modal, Upload, Icon, message, Spin, Button, Slider } from 'antd'
import { withApollo, ApolloConsumer } from 'react-apollo'
import React from 'react'
import AvatarEditor from 'react-avatar-editor'
import { Image } from 'cloudinary-react'
import './ChangeAvatar.scss'
import { CLOUD_NAME } from '../../../constants/app'
import { GET_CURRENT_USER, UPDATE_AVATAR } from '../../../graphql/user'

class ChangeAvatarModal extends React.Component {
  state = {
    visible: false,
    loading: false,
    file: null,
    scale: 1,
  }

  onClickSave = () => {
    if (this.editor) {
      const canvas = this.editor.getImage()
      canvas.toBlob(blob => this.startUpload(blob))
    }
  }

  onScale = scale => {
    this.setState({ scale })
  }

  setEditorRef = editor => (this.editor = editor)

  beforeUpload = file => {
    const isJPG = file.type === 'image/jpeg' || 'image/png'
    if (!isJPG) {
      message.error('You can only upload JPG or PNG file!')
      return false
    }
    const isLt2M = file.size / 1024 / 1024 < 2
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!')
      return false
    }
    this.setState({ file })

    return false
  }

  startUpload = async file => {
    this.setState({ loading: true })

    this.props.client
      .mutate({
        mutation: UPDATE_AVATAR,
        variables: {
          input: {
            avatar: file,
          },
        },
      })
      .then(data => {
        const { avatarUrl } = data.data.updateAvatar

        const { currentUser } = this.props.client.readQuery({
          query: GET_CURRENT_USER,
        })
        currentUser.avatar = avatarUrl
        this.props.client.writeQuery({
          query: GET_CURRENT_USER,
          data: { currentUser },
        })

        this.setState({
          loading: false,
          file: null,
          scale: 0,
        })
      })
  }

  showModal = () => {
    this.setState({
      visible: true,
    })
  }

  handleOk = () => {
    this.setState({
      visible: false,
    })
  }

  handleCancel = () => {
    this.cancelUpdateAvatar()
    this.setState({
      visible: false,
    })
  }

  cancelUpdateAvatar = () => {
    this.setState({ file: null, scale: 1 })
  }

  render() {
    const { file, scale } = this.state
    return (
      <React.Fragment>
        <a onClick={this.showModal}>Change Avatar</a>
        <Modal
          maskClosable={false}
          title="Change Avatar"
          visible={this.state.visible}
          footer={null}
          onCancel={this.handleCancel}
        >
          <ApolloConsumer>
            {client => {
              const data = client.readQuery({
                query: GET_CURRENT_USER,
              })
              const { currentUser } = data
              return (
                <div className="upload-avatar-layout">
                  {!file && (
                    <Upload
                      name="avatar"
                      className="avatar-uploader"
                      showUploadList={false}
                      beforeUpload={this.beforeUpload}
                    >
                      <Button>
                        <Icon type="upload" /> Click to Upload
                      </Button>
                    </Upload>
                  )}
                  <br />
                  <br />

                  <Spin
                    tip="Uploading..."
                    spinning={this.state.loading}
                    style={{ right: '0%' }}
                    indicator={
                      <Icon type="loading" style={{ fontSize: 24 }} spin />
                    }
                  >
                    {file ? (
                      <React.Fragment>
                        <AvatarEditor
                          ref={this.setEditorRef}
                          image={file}
                          width={300}
                          height={300}
                          border={50}
                          scale={scale}
                        />
                        <Slider
                          value={scale}
                          onChange={this.onScale}
                          min={1}
                          max={5}
                          step={0.1}
                        />
                      </React.Fragment>
                    ) : (
                      <Image
                        cloudName={CLOUD_NAME}
                        publicId={currentUser.avatar}
                        width="300"
                        height="300"
                        crop="scale"
                        style={{
                          borderRadius: '50%',
                          margin: 'auto',
                        }}
                      />
                    )}
                  </Spin>

                  {file && (
                    <React.Fragment>
                      <Button type="primary" onClick={this.onClickSave}>
                        Save
                      </Button>
                      {'  '}
                      <Button onClick={this.cancelUpdateAvatar}>Cancel</Button>
                    </React.Fragment>
                  )}
                </div>
              )
            }}
          </ApolloConsumer>
        </Modal>
      </React.Fragment>
    )
  }
}

export default withApollo(ChangeAvatarModal)
