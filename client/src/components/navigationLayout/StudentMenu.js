import React from 'react'
import { withRouter, Link } from 'react-router-dom'
import { Menu, Icon } from 'antd'
import { ROUTES } from '../../constants/routes'

const Student = ({ location: { pathname } }) => {
  if (pathname.includes('class')) {
    pathname = ROUTES.STUDENT.registeredClasses
  }
  return (
    <Menu mode="inline" selectedKeys={[pathname]}>
      <Menu.Item key={ROUTES.HOME}>
        <Link to={ROUTES.HOME}>
          <Icon type="home" />
          <span className="nav-text">Home</span>
        </Link>
      </Menu.Item>
      <Menu.Item key={ROUTES.STUDENT.registeredClasses}>
        <Link to={ROUTES.STUDENT.registeredClasses}>
          <Icon type="profile" />
          <span className="nav-text">Registered Classes</span>
        </Link>
      </Menu.Item>
      <Menu.Item key={ROUTES.STUDENT.courseRegistration}>
        <Link to={ROUTES.STUDENT.courseRegistration}>
          <Icon type="schedule" />
          <span className="nav-text">Course Regisration</span>
        </Link>
      </Menu.Item>
      <Menu.Item key={ROUTES.STUDENT.timetable}>
        <Link to={ROUTES.STUDENT.timetable}>
          <Icon type="table" />
          <span className="nav-text">Timetable</span>
        </Link>
      </Menu.Item>
      <Menu.Item key={ROUTES.STUDENT.schoolfee}>
        <Link to={ROUTES.STUDENT.schoolfee}>
          <Icon type="dollar" />
          <span className="nav-text">School Fee</span>
        </Link>
      </Menu.Item>
      <Menu.Item key={ROUTES.STUDENT.courseProgram}>
        <Link to={ROUTES.STUDENT.courseProgram}>
          <Icon type="read" />
          <span className="nav-text">Course Program</span>
        </Link>
      </Menu.Item>
    </Menu>
  )
}
export default withRouter(Student)
