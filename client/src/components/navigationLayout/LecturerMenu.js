import React from 'react'
import { withRouter, Link } from 'react-router-dom'
import { Menu, Icon } from 'antd'
import { ROUTES } from '../../constants/routes'

const LecturerMenu = ({ location: { pathname } }) => {
  if (pathname.includes('class')) {
    pathname = ROUTES.LECTURER.teachingClasses
  }
  return (
    <Menu mode="inline" selectedKeys={[pathname]}>
      <Menu.Item key={ROUTES.HOME}>
        <Link to={ROUTES.HOME}>
          <Icon type="home" />
          <span className="nav-text">Home</span>
        </Link>
      </Menu.Item>
      <Menu.Item key={ROUTES.LECTURER.teachingClasses}>
        <Link to={ROUTES.LECTURER.teachingClasses}>
          <Icon type="profile" />
          <span className="nav-text">Teaching Classes</span>
        </Link>
      </Menu.Item>
      <Menu.Item key={ROUTES.LECTURER.lecturerTimetable}>
        <Link to={ROUTES.LECTURER.lecturerTimetable}>
          <Icon type="table" />
          <span className="nav-text">Timtable</span>
        </Link>
      </Menu.Item>
    </Menu>
  )
}

export default withRouter(LecturerMenu)
