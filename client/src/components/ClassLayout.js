import React from 'react'
import ClassInfo from './classLayout/ClassInfo'
import ClassTabs from './classLayout/ClassTabs'

const ClassLayout = ({ activeKey }) => (
  <React.Fragment>
    <ClassInfo />
    <br />
    <ClassTabs activeKey={activeKey} />
  </React.Fragment>
)

export default ClassLayout
