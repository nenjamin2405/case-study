import { Mutation } from 'react-apollo'
import React from 'react'
import { showErrorModal } from '../lib/showErrorModal'

const CustomMutation = ({ children, ...props }) => (
  <Mutation {...props} onError={error => showErrorModal(error)}>
    {(mutate, { loading, data }) => children(mutate, { loading, data })}
  </Mutation>
)

export default CustomMutation
