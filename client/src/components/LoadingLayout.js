import React from 'react'
import { Spin, Icon } from 'antd'

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />

const LoadingLayout = ({ loading, children }) => (
  <Spin tip="Loading..." spinning={loading} indicator={antIcon}>
    {children}
  </Spin>
)

export default LoadingLayout
