import React from 'react'
import { withRouter } from 'react-router-dom'
import { capitalize } from 'lodash'
import { Tabs, Icon, Divider } from 'antd'
import { Query } from 'react-apollo'
import { TABS } from '../../constants/registeredClass'
import StudentsTab from './classTabs/StudentsTab'
import { GET_CURRENT_USER } from '../../graphql/user'
import IndividualStudentTab from './classTabs/IndividualStudentTab'
import { ROLES } from '../../constants/roles'
import StudentsList from './classTabs/StudentsList'
import DiscussionsTab from './classTabs/DiscussionsTab'
import Discussion from './classTabs/Discussion'
import AnnouncementsTab from './classTabs/AnnouncementsTab'

const { TabPane } = Tabs
const ClassTabs = ({ activeKey, match, history }) => {
  const handleChangeTab = key => {
    const { slug } = match.params
    if (key === TABS.HOME) {
      history.push(`/class/${slug}`)
    } else {
      history.push(`/class/${slug}/${key}`)
    }
  }

  return (
    <Tabs defaultActiveKey={activeKey} onChange={handleChangeTab}>
      <TabPane
        tab={
          <span>
            <Icon type="home" />
            {capitalize(TABS.HOME)}
          </span>
        }
        key={TABS.HOME}
      >
        Home
      </TabPane>
      <TabPane
        tab={
          <span>
            <Icon type="message" />
            {capitalize(TABS.DISCUSSIONS)}
          </span>
        }
        key={TABS.DISCUSSIONS}
      >
        {match.params.discussionSlug ? (
          <Discussion discussionSlug={match.params.discussionSlug} />
        ) : (
          <DiscussionsTab slug={match.params.slug} />
        )}
      </TabPane>
      <TabPane
        tab={
          <span>
            <Icon type="team" />
            {capitalize(TABS.STUDENTS)}
          </span>
        }
        key={TABS.STUDENTS}
      >
        <Query query={GET_CURRENT_USER}>
          {({ data: { currentUser } }) =>
            currentUser.__typename === ROLES.STUDENT ? (
              <React.Fragment>
                <IndividualStudentTab slug={match.params.slug} />
                <Divider orientation="left">Students List</Divider>
                <StudentsList slug={match.params.slug} />
              </React.Fragment>
            ) : (
              <StudentsTab slug={match.params.slug} />
            )
          }
        </Query>
      </TabPane>

      <TabPane
        tab={
          <span>
            <Icon type="notification" />
            {capitalize(TABS.ANNOUNCEMENTS)}
          </span>
        }
        key={TABS.ANNOUNCEMENTS}
      >
        {match.params.discussionSlug ? (
          <Discussion discussionSlug={match.params.discussionSlug} />
        ) : (
          <AnnouncementsTab slug={match.params.slug} />
        )}
      </TabPane>
    </Tabs>
  )
}

export default withRouter(ClassTabs)
