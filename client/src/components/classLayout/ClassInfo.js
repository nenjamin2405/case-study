import React from 'react'
import { Skeleton } from 'antd'
import { withRouter } from 'react-router-dom'
import { GET_REGISTERED_CLASS } from '../../graphql/registeredClass'
import CustomQuery from '../CustomQuery'

const ClassInfo = ({ match }) => (
  <CustomQuery
    query={GET_REGISTERED_CLASS}
    variables={{ slug: match.params.slug }}
  >
    {({ loading, data }) => {
      if (loading) return <Skeleton active />
      return (
        <React.Fragment>
          <h2>{data.registeredClass.groupable.name}</h2>
          <p>{data.registeredClass.groupable.code}</p>
        </React.Fragment>
      )
    }}
  </CustomQuery>
)

export default withRouter(ClassInfo)
