import React, { useState } from 'react'
import { Drawer, Button, Icon } from 'antd'
import CreateGradeColumnForm from './createGradeColumnDrawer/CreateGradeColumnForm'

const CreateGradeColumnDrawer = () => {
  const [visible, setVisible] = useState(false)

  const showDrawer = () => {
    setVisible(true)
  }

  const onClose = () => {
    setVisible(false)
  }
  return (
    <React.Fragment>
      <Button type="primary" onClick={showDrawer}>
        <Icon type="plus" />
        Add Grade Column
      </Button>
      <Drawer
        title="Create Grade Column"
        placement="right"
        closable={false}
        onClose={onClose}
        visible={visible}
      >
        <CreateGradeColumnForm />
      </Drawer>
    </React.Fragment>
  )
}

export default CreateGradeColumnDrawer
