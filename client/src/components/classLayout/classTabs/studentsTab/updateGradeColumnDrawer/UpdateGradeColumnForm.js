import React from 'react'
import { Form, Input, Button, InputNumber } from 'antd'
import { withApollo } from 'react-apollo'
import LoadingLayout from '../../../../LoadingLayout'
import { UPDATE_RECORD } from '../../../../../graphql/common'
import { RECORD_TYPE } from '../../../../../constants/app'
import { showErrorModal } from '../../../../../lib/showErrorModal'

const FormItem = Form.Item

class UpdateGradeColumn extends React.Component {
  state = {
    loading: false,
  }

  handleUpdateGradeColumn = async e => {
    e.preventDefault()
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        this.setState({ loading: true })
        const { client } = this.props
        try {
          await client.mutate({
            mutation: UPDATE_RECORD,
            variables: {
              input: {
                type: RECORD_TYPE.GRADE_COLUMN,
                attributes: JSON.stringify({
                  ...values,
                  id: this.props.gradeColumn.id,
                }),
              },
            },
          })
          this.setState({ loading: false })
        } catch (error) {
          showErrorModal(error)
          this.setState({ loading: false })
        }
      }
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const { name, percentage } = this.props.gradeColumn
    return (
      <LoadingLayout loading={this.state.loading}>
        <Form onSubmit={this.handleUpdateGradeColumn} className="login-form">
          <FormItem>
            {getFieldDecorator('name', {
              rules: [
                {
                  required: true,
                  message: 'Please input grade column name!',
                },
              ],
              initialValue: name,
            })(<Input placeholder="name" />)}
          </FormItem>
          <FormItem>
            {getFieldDecorator('percentage', {
              rules: [
                {
                  required: true,
                  message: 'Please input grade column percentage!',
                },
              ],
              initialValue: percentage,
            })(
              <InputNumber
                placeholder="percentage"
                min={0}
                max={100}
                style={{ width: '100%' }}
              />
            )}
          </FormItem>
          <FormItem>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Update
            </Button>
          </FormItem>
        </Form>
      </LoadingLayout>
    )
  }
}

const UpdateGradeColumnForm = Form.create()(UpdateGradeColumn)

export default withApollo(UpdateGradeColumnForm)
