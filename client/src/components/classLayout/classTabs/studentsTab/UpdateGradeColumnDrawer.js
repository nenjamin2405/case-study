import React, { useState } from 'react'
import { Drawer, Button } from 'antd'
import UpdateGradeColumnForm from './updateGradeColumnDrawer/UpdateGradeColumnForm'

const UpdateGradeColumnDrawer = ({ gradeColumn }) => {
  const [visible, setVisible] = useState(false)

  const showDrawer = () => {
    setVisible(true)
  }

  const onClose = () => {
    setVisible(false)
  }
  return (
    <React.Fragment>
      <Button type="primary" icon="edit" onClick={showDrawer} block>
        Edit
      </Button>
      <Drawer
        title={`Update ${gradeColumn.name} Grade Column`}
        placement="right"
        closable={false}
        onClose={onClose}
        visible={visible}
      >
        <UpdateGradeColumnForm gradeColumn={gradeColumn} />
      </Drawer>
    </React.Fragment>
  )
}

export default UpdateGradeColumnDrawer
