import React from 'react'
import { Form, Input, Button, InputNumber } from 'antd'
import { withRouter } from 'react-router-dom'
import { withApollo } from 'react-apollo'
import LoadingLayout from '../../../../LoadingLayout'
import { CREATE_RECORD } from '../../../../../graphql/common'
import { RECORD_TYPE } from '../../../../../constants/app'
import { GET_CLASS_STUDENTS } from '../../../../../graphql/registeredClass'
import { showErrorModal } from '../../../../../lib/showErrorModal'

const FormItem = Form.Item

class CreateGradeColumn extends React.Component {
  state = {
    loading: false,
  }

  handleCreateGradeColumn = async e => {
    e.preventDefault()
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        this.setState({ loading: true })
        const { client, match } = this.props
        try {
          await client.mutate({
            mutation: CREATE_RECORD,
            variables: {
              input: {
                type: RECORD_TYPE.GRADE_COLUMN,
                attributes: JSON.stringify({
                  ...values,
                  registered_class_id: match.params.slug,
                }),
              },
            },
            refetchQueries: [
              {
                query: GET_CLASS_STUDENTS,
                variables: {
                  slug: match.params.slug,
                },
              },
            ],
          })
          this.props.form.resetFields()
          this.setState({ loading: false })
        } catch (error) {
          showErrorModal(error)
          this.setState({ loading: false })
        }
      }
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    return (
      <LoadingLayout loading={this.state.loading}>
        <Form onSubmit={this.handleCreateGradeColumn} className="login-form">
          <FormItem>
            {getFieldDecorator('name', {
              rules: [
                {
                  required: true,
                  message: 'Please input grade column name!',
                },
              ],
            })(<Input placeholder="name" />)}
          </FormItem>
          <FormItem>
            {getFieldDecorator('percentage', {
              rules: [
                {
                  required: true,
                  message: 'Please input grade column percentage!',
                },
              ],
            })(
              <InputNumber
                placeholder="percentage"
                min={0}
                max={100}
                style={{ width: '100%' }}
              />
            )}
          </FormItem>
          <FormItem>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Create
            </Button>
          </FormItem>
        </Form>
      </LoadingLayout>
    )
  }
}

const CreateGradeColumnForm = Form.create()(CreateGradeColumn)

export default withApollo(withRouter(CreateGradeColumnForm))
