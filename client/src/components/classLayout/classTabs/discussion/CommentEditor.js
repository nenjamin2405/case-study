import React, { useState } from 'react'
import { Comment, Form, Button } from 'antd'
import { isEmpty } from 'lodash'
import { animateScroll as scroll } from 'react-scroll'
import TextArea from 'antd/lib/input/TextArea'
import { Query } from 'react-apollo'
import { GET_CURRENT_USER } from '../../../../graphql/user'
import CustomMutation from '../../../CustomMutation'
import {
  CREATE_COMMENT,
  GET_COMMENTS,
} from '../../../../graphql/registeredClass'
import CloudImage from '../../../CloudImage'

const Editor = ({ onChange, onSubmit, submitting, value }) => (
  <div>
    <Form.Item>
      <TextArea rows={4} onChange={onChange} value={value} />
    </Form.Item>
    <Form.Item>
      <Button
        htmlType="submit"
        loading={submitting}
        onClick={onSubmit}
        type="primary"
      >
        Add Comment
      </Button>
    </Form.Item>
  </div>
)

const CommentEDitor = ({ discussionSlug, amountOfComments }) => {
  const [commentValue, setCommentValue] = useState('')

  return (
    <Comment
      avatar={
        <Query query={GET_CURRENT_USER}>
          {({ data }) => {
            const { avatar } = data.currentUser
            return <CloudImage publicId={avatar} />
          }}
        </Query>
      }
      content={
        <CustomMutation
          mutation={CREATE_COMMENT}
          variables={{
            input: {
              attributes: JSON.stringify({
                content: commentValue,
                discussion_id: discussionSlug,
              }),
            },
          }}
          refetchQueries={[
            {
              query: GET_COMMENTS,
              variables: {
                discussionSlug,
              },
            },
          ]}
          onCompleted={() => {
            setCommentValue('')
            if (amountOfComments > 1) {
              scroll.scrollToBottom({
                containerId: 'commentsContainer',
              })
            }
          }}
        >
          {(createComment, { loading: commentSubmitting }) => (
            <Editor
              onChange={e => setCommentValue(e.target.value)}
              onSubmit={isEmpty(commentValue) ? null : createComment}
              submitting={commentSubmitting}
              value={commentValue}
            />
          )}
        </CustomMutation>
      }
    />
  )
}

export default CommentEDitor
