import React from 'react'
import { Table, Button, Icon, Divider, Modal } from 'antd'
import { isEmpty, omit, cloneDeep } from 'lodash'
import { withApollo } from 'react-apollo'
import CustomQuery from '../../CustomQuery'
import CustomMutation from '../../CustomMutation'
import {
  GET_CLASS_STUDENTS,
  SAVE_STUDENT_GRADES,
  DELETE_GRADE_COLUMN,
  PUBLISH_CLASS_GRADES,
  TOGGLE_PUBLISH_GRADE_COLUMN,
} from '../../../graphql/registeredClass'
import CloudImage from '../../CloudImage'
import CreateGradeColumnDrawer from './studentsTab/CreateGradeColumnDrawer'
import EditableFormRow from '../../EditableRow'
import EditableCell from '../../EditableCell'
import UpdateGradeColumnDrawer from './studentsTab/UpdateGradeColumnDrawer'
import { CustomPopover } from '../../CustomPopover'
import { letterGradeFor } from '../../../utils/grade'
import { COLOR } from '../../../constants/color'

const { Column } = Table
const { confirm } = Modal
class StudentsTab extends React.Component {
  state = {
    isSavingGrades: false,
    isDeletingGradeColumn: false,
    isPublishingClassGrades: false,
  }

  handleSave = row => {
    const data = this.props.client.readQuery({
      query: GET_CLASS_STUDENTS,
      variables: {
        slug: this.props.slug,
      },
    })
    const newData = data.classStudents
      ? JSON.parse(data.classStudents.studentGrades)
      : []
    const index = newData.findIndex(itemData => row.key === itemData.key)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })
    this.props.client.writeQuery({
      query: GET_CLASS_STUDENTS,
      variables: {
        slug: this.props.slug,
      },
      data: {
        classStudents: {
          ...data.classStudents,
          studentGrades: JSON.stringify(newData),
        },
      },
    })
  }

  saveGrades = async () => {
    const data = this.props.client.readQuery({
      query: GET_CLASS_STUDENTS,
      variables: {
        slug: this.props.slug,
      },
    })
    const studentGrades = data.classStudents
      ? data.classStudents.studentGrades
      : JSON.stringify([])
    this.setState({ isSavingGrades: true })
    await this.props.client.mutate({
      mutation: SAVE_STUDENT_GRADES,
      variables: {
        input: {
          studentGrades,
        },
      },
      refetchQueries: [
        {
          query: GET_CLASS_STUDENTS,
          variables: {
            slug: this.props.slug,
          },
        },
      ],
    })
    this.setState({ isSavingGrades: false })
  }

  publishClassGrades = async () => {
    this.setState({ isPublishingClassGrades: true })
    await this.props.client.mutate({
      mutation: PUBLISH_CLASS_GRADES,
      variables: {
        input: {
          slug: this.props.slug,
        },
      },
      refetchQueries: [
        {
          query: GET_CLASS_STUDENTS,
          variables: {
            slug: this.props.slug,
          },
        },
      ],
    })
    this.setState({ isPublishingClassGrades: false })
  }

  deleteGradeColumn = async id => {
    this.setState({ isDeletingGradeColumn: true })
    await this.props.client.mutate({
      mutation: DELETE_GRADE_COLUMN,
      variables: {
        input: {
          id,
        },
      },
      refetchQueries: [
        {
          query: GET_CLASS_STUDENTS,
          variables: {
            slug: this.props.slug,
          },
        },
      ],
    })
    this.setState({ isDeletingGradeColumn: false })
  }

  render() {
    const {
      isSavingGrades,
      isDeletingGradeColumn,
      isPublishingClassGrades,
    } = this.state
    const showDeleteGradeColumnConfirm = (id, name, deleteGradeColumn) => {
      confirm({
        title: `Are you sure you want to delete ${name} grade column?`,
        content:
          'This action will remove all student grades related to this column',
        okText: 'Yes',
        okType: 'danger',
        cancelText: 'No',
        onOk() {
          deleteGradeColumn(id)
        },
      })
    }

    const gradeColumnSettings = gradeColumn => (
      <div>
        <UpdateGradeColumnDrawer gradeColumn={gradeColumn} />
        <Divider />
        <Button
          type="danger"
          icon="delete"
          block
          loading={isDeletingGradeColumn}
          onClick={() =>
            showDeleteGradeColumnConfirm(
              gradeColumn.id,
              gradeColumn.name,
              this.deleteGradeColumn
            )
          }
        >
          Delete
        </Button>
        <Divider />

        <CustomMutation
          mutation={TOGGLE_PUBLISH_GRADE_COLUMN}
          variables={{ input: { id: gradeColumn.id } }}
        >
          {(togglePublishGradeColumn, { loading }) => (
            <Button
              loading={loading}
              onClick={togglePublishGradeColumn}
              style={{
                backgroundColor: gradeColumn.isPublished
                  ? COLOR.MAIN_PURPLE
                  : COLOR.MAIN_GREEN,
                color: 'white',
              }}
              block
            >
              {gradeColumn.isPublished
                ? ' Un-publish Grades'
                : 'Publish Grades'}
            </Button>
          )}
        </CustomMutation>
      </div>
    )

    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    }

    return (
      <CustomQuery
        query={GET_CLASS_STUDENTS}
        variables={{ slug: this.props.slug }}
      >
        {({ loading, data }) => {
          const studentGrades = data.classStudents
            ? JSON.parse(data.classStudents.studentGrades)
            : []
          const gradeColumns = data.classStudents
            ? data.classStudents.gradeColumns
            : []
          return (
            <React.Fragment>
              <div>
                <CreateGradeColumnDrawer />

                <Button
                  loading={isSavingGrades}
                  type="primary"
                  onClick={this.saveGrades}
                  style={{ float: 'right' }}
                  disabled={isEmpty(gradeColumns)}
                >
                  <Icon type="save" />
                  Save Grades
                </Button>
                <Button
                  loading={isPublishingClassGrades}
                  onClick={this.publishClassGrades}
                  style={{
                    float: 'right',
                    backgroundColor: COLOR.MAIN_GREEN,
                    color: 'white',
                    marginRight: 10,
                  }}
                  disabled={isEmpty(gradeColumns)}
                >
                  <Icon type="cloud-upload" />
                  Publish All Grades
                </Button>
              </div>

              <br />

              <Table
                components={components}
                bordered
                dataSource={studentGrades}
                rowClassName="editable-row"
                loading={
                  loading ||
                  isSavingGrades ||
                  isDeletingGradeColumn ||
                  isPublishingClassGrades
                }
                rowKey={record => record.student.id}
              >
                <Column
                  title="Student"
                  key="student"
                  editable={false}
                  render={record => {
                    const {
                      first_name: firstName,
                      last_name: lastName,
                      avatar,
                    } = record.student
                    return (
                      <CloudImage publicId={avatar}>
                        {firstName} {lastName}
                      </CloudImage>
                    )
                  }}
                />
                {gradeColumns.map(gradeColumn => (
                  <Column
                    align="center"
                    editable
                    title={
                      <CustomPopover
                        content={gradeColumnSettings(gradeColumn)}
                        title={` ${gradeColumn.name} - ${
                          gradeColumn.percentage
                        }%`}
                      >
                        <a>
                          {gradeColumn.name} - {gradeColumn.percentage}%
                        </a>
                      </CustomPopover>
                    }
                    dataIndex={`grade-column-${gradeColumn.id}`}
                    key={gradeColumn.id}
                    onCell={record => ({
                      record,
                      editable: !isSavingGrades,
                      dataIndex: `grade-column-${gradeColumn.id}`,
                      title: `${gradeColumn.name} Score`,
                      handleSave: this.handleSave,
                      inputType: 'number',
                    })}
                  />
                ))}
                <Column
                  title="Average Grade"
                  key="averageGrade"
                  editable={false}
                  align="center"
                  render={record => {
                    const grades = omit(cloneDeep(record), ['student'])
                    let averageScore = 0
                    Object.keys(grades).forEach(key => {
                      const grade = grades[key]
                      const { percentage } = gradeColumns.find(
                        gradeColumn => gradeColumn.id === key.split('-').pop()
                      )
                      averageScore += (grade * percentage) / 100
                    })
                    return <b>{Math.round(averageScore)}</b>
                  }}
                />
                <Column
                  title="Letter Grade"
                  key="letterGrade"
                  editable={false}
                  align="center"
                  render={record => {
                    const grades = omit(cloneDeep(record), ['student'])
                    let averageScore = 0
                    Object.keys(grades).forEach(key => {
                      const grade = grades[key]
                      const { percentage } = gradeColumns.find(
                        gradeColumn => gradeColumn.id === key.split('-').pop()
                      )
                      averageScore += (grade * percentage) / 100
                    })
                    return <b>{letterGradeFor(Math.round(averageScore))}</b>
                  }}
                />
              </Table>
            </React.Fragment>
          )
        }}
      </CustomQuery>
    )
  }
}

export default withApollo(StudentsTab)
