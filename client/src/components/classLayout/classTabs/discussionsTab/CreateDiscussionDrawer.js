import React, { useState } from 'react'
import { Drawer, Button, Icon } from 'antd'
import CreateDiscussionForm from './createDiscussionDrawer/CreateDiscussionForm'

const CreateDiscussionDrawer = ({ isAnnouncement }) => {
  const [visible, setVisible] = useState(false)

  const showDrawer = () => {
    setVisible(true)
  }

  const onClose = () => {
    setVisible(false)
  }

  return (
    <React.Fragment>
      <Button type="primary" onClick={showDrawer}>
        <Icon type="plus" />
        {isAnnouncement ? 'Create Announcement' : ' Create Discussion'}
      </Button>
      <Drawer
        width="80%"
        title={isAnnouncement ? 'Announcement' : 'Discussion'}
        placement="right"
        closable
        onClose={onClose}
        visible={visible}
      >
        <CreateDiscussionForm
          isAnnouncement={isAnnouncement}
          closeDrawer={onClose}
        />
      </Drawer>
    </React.Fragment>
  )
}

export default CreateDiscussionDrawer
