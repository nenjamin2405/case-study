import React, { useState } from 'react'
import { withApollo } from 'react-apollo'
import { withRouter } from 'react-router-dom'
import { isEmpty } from 'lodash'
import { Form, Input, Button, Checkbox } from 'antd'
import { EditorState, convertToRaw } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import './CreateDiscussionForm.scss'
import LoadingLayout from '../../../../LoadingLayout'
import {
  CREATE_DISCUSSION,
  GET_CLASS_DISCUSSIONS,
  GET_CLASS_ANNOUNCEMENTS,
} from '../../../../../graphql/registeredClass'
import { showErrorModal } from '../../../../../lib/showErrorModal'
import { DISCUSSION_TYPE } from '../../../../../constants/registeredClass'

const FormItem = Form.Item

const CreateDiscussionForm = ({
  form,
  client,
  match,
  isAnnouncement,
  closeDrawer,
}) => {
  const { getFieldDecorator } = form
  const [editorState, setEditorState] = useState(EditorState.createEmpty())
  const [loading, setLoading] = useState(false)

  const onEditorStateChange = editState => {
    setEditorState(editState)
  }

  const handleCreateDiscussion = async e => {
    e.preventDefault()
    form.validateFields(async (err, values) => {
      if (!err && !isEmpty(editorState)) {
        setLoading(true)
        try {
          await client.mutate({
            mutation: CREATE_DISCUSSION,
            variables: {
              input: {
                attributes: JSON.stringify({
                  ...values,
                  content: draftToHtml(
                    convertToRaw(editorState.getCurrentContent())
                  ),
                  registered_class_id: match.params.slug,
                  category: isAnnouncement
                    ? DISCUSSION_TYPE.ANNOUNCEMENT
                    : DISCUSSION_TYPE.DISCUSSION,
                }),
              },
            },
            refetchQueries: [
              {
                query: isAnnouncement
                  ? GET_CLASS_ANNOUNCEMENTS
                  : GET_CLASS_DISCUSSIONS,
                variables: {
                  slug: match.params.slug,
                },
              },
            ],
          })
          setLoading(false)
          closeDrawer()
          form.resetFields()
          onEditorStateChange(EditorState.createEmpty())
        } catch (error) {
          showErrorModal(error)
          setLoading(false)
        }
      }
    })
  }

  const category = isAnnouncement
    ? DISCUSSION_TYPE.ANNOUNCEMENT
    : DISCUSSION_TYPE.DISCUSSION
  return (
    <LoadingLayout loading={loading}>
      <Form onSubmit={handleCreateDiscussion}>
        <FormItem>
          {getFieldDecorator('title', {
            rules: [
              {
                required: true,
                message: `Please input ${category} title!`,
              },
            ],
          })(<Input placeholder="Title" />)}
        </FormItem>
        <Editor
          editorState={editorState}
          wrapperClassName="demo-wrapper"
          editorClassName="case-study-wrapper"
          onEditorStateChange={onEditorStateChange}
        />
        <br />
        <FormItem>
          {getFieldDecorator('is_comments_enabled', {
            valuePropName: 'checked',
            initialValue: !isAnnouncement,
          })(<Checkbox>Enable Comments</Checkbox>)}
        </FormItem>
        <FormItem>
          <Button type="primary" htmlType="submit" style={{ height: 40 }} block>
            Post
          </Button>
        </FormItem>
      </Form>
    </LoadingLayout>
  )
}

export default withApollo(withRouter(Form.create()(CreateDiscussionForm)))
