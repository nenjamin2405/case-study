import React from 'react'
import { List } from 'antd'
import { Link } from 'react-router-dom'

import moment from 'moment'
import CustomQuery from '../../../CustomQuery'
import {
  GET_CLASS_DISCUSSIONS,
  GET_CLASS_ANNOUNCEMENTS,
} from '../../../../graphql/registeredClass'
import CloudImage from '../../../CloudImage'
import { COLOR } from '../../../../constants/color'

const DiscussionsList = ({ slug, isAnnouncement }) => (
  <CustomQuery
    query={isAnnouncement ? GET_CLASS_ANNOUNCEMENTS : GET_CLASS_DISCUSSIONS}
    variables={{ slug }}
  >
    {({ loading, data }) => (
      <List
        loading={loading}
        itemLayout="horizontal"
        dataSource={
          isAnnouncement ? data.classAnnouncements : data.classDiscussions
        }
        renderItem={discussion => (
          <List.Item>
            <List.Item.Meta
              avatar={<CloudImage publicId={discussion.author.avatar} />}
              title={
                <Link
                  to={`/class/${slug}/${
                    isAnnouncement ? 'announcements' : 'discussions'
                  }/${discussion.slug}`}
                >
                  <span style={{ color: COLOR.MAIN_BLUE }}>
                    {' '}
                    {discussion.title}
                  </span>
                </Link>
              }
              description={moment(discussion.createdAt).fromNow()}
            />
          </List.Item>
        )}
      />
    )}
  </CustomQuery>
)

export default DiscussionsList
