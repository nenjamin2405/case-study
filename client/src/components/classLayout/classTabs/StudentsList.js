import React from 'react'
import { Table } from 'antd'
import CustomQuery from '../../CustomQuery'
import { GET_STUDENTS_LIST } from '../../../graphql/registeredClass'
import CloudImage from '../../CloudImage'

const { Column } = Table

const StudentsList = ({ slug }) => (
  <React.Fragment>
    <CustomQuery query={GET_STUDENTS_LIST} variables={{ slug }}>
      {({ loading, data }) => (
        <Table
          dataSource={data.studentsList ? data.studentsList : []}
          loading={loading}
        >
          <Column
            title="Student"
            key="student"
            render={record => {
              const { fullName, avatar } = record
              return <CloudImage publicId={avatar}>{fullName}</CloudImage>
            }}
          />
        </Table>
      )}
    </CustomQuery>
  </React.Fragment>
)

export default StudentsList
