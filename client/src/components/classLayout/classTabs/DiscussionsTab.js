import React from 'react'
import { Query } from 'react-apollo'
import CreateDiscussionDrawer from './discussionsTab/CreateDiscussionDrawer'
import DiscussionsList from './discussionsTab/DiscussionsList'
import { GET_CURRENT_USER } from '../../../graphql/user'
import { ROLES } from '../../../constants/roles'

const DiscussionsTab = ({ slug }) => (
  <React.Fragment>
    <Query query={GET_CURRENT_USER}>
      {({ data: { currentUser } }) =>
        currentUser.__typename === ROLES.LECTURER && (
          <React.Fragment>
            <CreateDiscussionDrawer />
            <br />
            <br />
          </React.Fragment>
        )
      }
    </Query>

    <DiscussionsList slug={slug} />
  </React.Fragment>
)

export default DiscussionsTab
