import React from 'react'
import { Comment, List, Divider, Icon } from 'antd'
import moment from 'moment'
import { Element } from 'react-scroll'
import Pluralize from 'react-pluralize'
import parse from 'html-react-parser'
import CustomQuery from '../../CustomQuery'
import { GET_DISCUSSION, GET_COMMENTS } from '../../../graphql/registeredClass'
import CloudImage from '../../CloudImage'
import CommentEDitor from './discussion/CommentEditor'
import './Discussion.scss'

const Discussion = ({ discussionSlug }) => (
  <React.Fragment>
    <CustomQuery query={GET_DISCUSSION} variables={{ discussionSlug }}>
      {({ loading, data }) => (
        <React.Fragment>
          <List
            loading={loading}
            itemLayout="vertical"
            dataSource={data.discussion ? [data.discussion] : []}
            renderItem={discussion => (
              <List.Item>
                <List.Item.Meta
                  avatar={<CloudImage publicId={discussion.author.avatar} />}
                  title={<span>{discussion.title}</span>}
                  description={moment(discussion.createdAt).fromNow()}
                />
                {parse(data.discussion.content)}
              </List.Item>
            )}
          />
          {!loading &&
            data.discussion.isCommentsEnabled && (
              <CustomQuery query={GET_COMMENTS} variables={{ discussionSlug }}>
                {({ data: commentData, loading: loadingComment }) => (
                  <React.Fragment>
                    <Divider orientation="left">
                      <Icon type="message" style={{ marginRight: 10 }} />
                      <Pluralize
                        singular="comment"
                        count={
                          commentData.comments ? commentData.comments.length : 0
                        }
                      />
                    </Divider>
                    {commentData.comments &&
                      commentData.comments.length > 0 && (
                        <Element
                          name="commentsContainer"
                          className="test-scroll"
                          id="commentsContainer"
                        >
                          <List
                            className="comment-list"
                            loading={loadingComment}
                            itemLayout="horizontal"
                            dataSource={commentData.comments}
                            renderItem={comment => (
                              <Comment
                                author={comment.author.fullName}
                                avatar={
                                  <CloudImage
                                    publicId={comment.author.avatar}
                                  />
                                }
                                content={comment.content}
                                datetime={moment(comment.createdAt).fromNow()}
                              />
                            )}
                          />
                        </Element>
                      )}

                    <CommentEDitor
                      discussionSlug={discussionSlug}
                      amountOfComments={
                        commentData.comments ? commentData.comments.length : 0
                      }
                    />
                  </React.Fragment>
                )}
              </CustomQuery>
            )}
        </React.Fragment>
      )}
    </CustomQuery>
  </React.Fragment>
)

export default Discussion
