import React from 'react'
import { Table } from 'antd'
import { omit, cloneDeep, isEmpty } from 'lodash'
import CustomQuery from '../../CustomQuery'
import { GET_GRADES_OF_STUDENT } from '../../../graphql/registeredClass'
import CloudImage from '../../CloudImage'
import { letterGradeFor } from '../../../utils/grade'
import { GET_CURRENT_USER } from '../../../graphql/user'

const { Column } = Table
const IndividualStudentTab = ({ slug }) => (
  <CustomQuery query={GET_GRADES_OF_STUDENT} variables={{ slug }}>
    {({ loading, data, client }) => {
      const { currentUser } = client.readQuery({ query: GET_CURRENT_USER })
      const studentGrades = data.gradesOfStudent
        ? [
            {
              student: currentUser,
              ...JSON.parse(data.gradesOfStudent.studentGrades),
            },
          ]
        : []
      const gradeColumns = data.gradesOfStudent
        ? data.gradesOfStudent.gradeColumns
        : []
      return (
        <Table
          bordered
          dataSource={studentGrades}
          loading={loading}
          rowKey={record => record.student.id}
          pagination={false}
        >
          <Column
            title="You"
            key="student"
            editable={false}
            render={record => {
              const { fullName, avatar } = record.student
              return <CloudImage publicId={avatar}>{fullName}</CloudImage>
            }}
          />
          {gradeColumns.map(gradeColumn => (
            <Column
              align="center"
              editable
              title={
                <span>
                  {gradeColumn.name} - {gradeColumn.percentage}%
                </span>
              }
              dataIndex={`grade-column-${gradeColumn.id}`}
              key={gradeColumn.id}
            />
          ))}
          {!isEmpty(gradeColumns) && (
            <Column
              title="Average Grade"
              key="averageGrade"
              editable={false}
              align="center"
              render={record => {
                const grades = omit(cloneDeep(record), ['student'])
                let averageScore = 0
                Object.keys(grades).forEach(key => {
                  const grade = grades[key]
                  const { percentage } = gradeColumns.find(
                    gradeColumn => gradeColumn.id === key.split('-').pop()
                  )
                  averageScore += (grade * percentage) / 100
                })
                return <b>{Math.round(averageScore)}</b>
              }}
            />
          )}
          {!isEmpty(gradeColumns) && (
            <Column
              title="Letter Grade"
              key="letterGrade"
              editable={false}
              align="center"
              render={record => {
                const grades = omit(cloneDeep(record), ['student'])
                let averageScore = 0
                Object.keys(grades).forEach(key => {
                  const grade = grades[key]
                  const { percentage } = gradeColumns.find(
                    gradeColumn => gradeColumn.id === key.split('-').pop()
                  )
                  averageScore += (grade * percentage) / 100
                })
                return <b>{letterGradeFor(Math.round(averageScore))}</b>
              }}
            />
          )}
        </Table>
      )
    }}
  </CustomQuery>
)

export default IndividualStudentTab
