import { Route, Redirect } from 'react-router-dom'
import React from 'react'
import isAuthenticated from '../lib/isAuthenticated'
import { ROUTES } from '../constants/routes'

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated() ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: ROUTES.LOGIN,
            state: { from: props.location },
          }}
        />
      )
    }
  />
)

export default PrivateRoute
