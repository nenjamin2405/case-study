import { Image } from 'cloudinary-react'
import React from 'react'
import { CLOUD_NAME } from '../constants/app'
import { COLOR } from '../constants/color'

const CloudImage = ({ publicId, width, height, children, style }) => (
  <React.Fragment>
    <Image
      cloudName={CLOUD_NAME}
      publicId={publicId}
      width={width || 35}
      height={height || 35}
      crop="scale"
      style={{
        borderRadius: '50%',
        border: `1px solid ${COLOR.MAIN_BLUE}`,
        ...style,
      }}
    />{' '}
    <span style={{ marginLeft: 10 }}>{children}</span>
  </React.Fragment>
)

export default CloudImage
