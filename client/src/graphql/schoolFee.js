import gql from 'graphql-tag'

export const GET_STUDENT_SCHOOL_FEES = gql`
  query getStudenSchoolFees {
    studentSchoolFees {
      id
      semester {
        id
        startYear
        endYear
        category
      }
      fee
      remit
      paid
      enrolments {
        registeredClass {
          groupable {
            ... on TheoryGroup {
              id
              name
              code
            }
            ... on LabGroup {
              id
              name
              code
            }
          }
        }
        id
        fee
        credits
      }
    }
  }
`
