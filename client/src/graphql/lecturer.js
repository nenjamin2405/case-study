import gql from 'graphql-tag'

export const GET_LECTURER_TEACHING_CLASSES = gql`
  query getLecturerTeachingClasses {
    lecturerTeachingClasses {
      semester {
        startYear
        endYear
        id
        category
      }
      labGroups {
        id
        registeredClass {
          id
          day
          amountOfStudents
          day
          slug
        }
        laboratory {
          id
          code
          name
        }
      }
      theoryGroups {
        id
        registeredClass {
          id
          day
          amountOfStudents
          day
          slug
        }
        course {
          id
          code
          name
        }
      }
    }
  }
`
