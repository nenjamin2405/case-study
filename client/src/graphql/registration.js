import gql from 'graphql-tag'

export const GET_CLASSES_FOR_CURRENT_STUDENT = gql`
  query getClassesForCurrentStudent {
    classesForCurrentStudentToRegister {
      itIsTimeForRegistration
      endTime
      theoryGroups {
        id
        slot
        remainingSlot
        startTime
        endTime
        course {
          id
          code
          name
          credits
          gpaCalculated
        }
        lecturer {
          id
          fullName
        }
        registeredClass {
          day
          id
          registeredByCurrentStudent
          startSection
          classSections
          classRoom {
            id
            code
          }
        }
        labGroups {
          startTime
          endTime
          id
          slot
          remainingSlot
          course: laboratory {
            id
            code
            name
            credits
          }
          lecturer {
            id
            fullName
          }
          registeredClass {
            registeredByCurrentStudent
            id
            day
            startSection
            classSections
            classRoom {
              id
              code
            }
          }
        }
      }
      registeredClassIds
    }
  }
`
export const ENROLL_COURSES = gql`
  mutation enrollCourses($input: EnrollCoursesInput!) {
    enrollCourses(input: $input) {
      enrolled
    }
  }
`
