import gql from 'graphql-tag'

export const GET_REGISTERED_CLASS = gql`
  query getRegisteredClass($slug: String!) {
    registeredClass(slug: $slug) {
      id
      groupable {
        ... on TheoryGroup {
          id
          code
          name
        }
        ... on LabGroup {
          id
          code
          name
        }
      }
    }
  }
`

export const GET_CLASS_STUDENTS = gql`
  query getClassStudents($slug: String!) {
    classStudents(registeredClassId: $slug) {
      studentGrades
      gradeColumns {
        id
        name
        percentage
        isPublished
      }
    }
  }
`

export const GET_GRADES_OF_STUDENT = gql`
  query getGradesOfStudent($slug: String!) {
    gradesOfStudent(registeredClassId: $slug) {
      studentGrades
      gradeColumns {
        id
        name
        percentage
        isPublished
      }
    }
  }
`

export const SAVE_STUDENT_GRADES = gql`
  mutation saveStudentGrades($input: SaveStudentGradesInput!) {
    saveStudentGrades(input: $input) {
      success
    }
  }
`

export const DELETE_GRADE_COLUMN = gql`
  mutation deleteGradeColumn($input: DeleteGradeColumnInput!) {
    deleteGradeColumn(input: $input) {
      success
    }
  }
`

export const PUBLISH_CLASS_GRADES = gql`
  mutation publishClassGrades($input: PublishClassGradesInput!) {
    publishClassGrades(input: $input) {
      success
    }
  }
`

export const TOGGLE_PUBLISH_GRADE_COLUMN = gql`
  mutation togglePublishGradeColumn($input: TogglePublishGradeColumnInput!) {
    togglePublishGradeColumn(input: $input) {
      gradeColumn {
        id
        isPublished
      }
    }
  }
`

export const GET_STUDENTS_LIST = gql`
  query getStudentsList($slug: String!) {
    studentsList(slug: $slug) {
      id
      fullName
      avatar
    }
  }
`

export const CREATE_DISCUSSION = gql`
  mutation createDiscussion($input: CreateDiscussionInput!) {
    createDiscussion(input: $input) {
      discussion {
        id
        content
        title
      }
    }
  }
`

export const GET_CLASS_DISCUSSIONS = gql`
  query getClassDiscussions($slug: String!) {
    classDiscussions(slug: $slug) {
      id
      slug
      title
      createdAt
      author {
        ... on Lecturer {
          fullName
          avatar
          id
        }
      }
    }
  }
`

export const GET_CLASS_ANNOUNCEMENTS = gql`
  query getClassAnnouncements($slug: String!) {
    classAnnouncements(slug: $slug) {
      id
      slug
      title
      createdAt
      author {
        ... on Lecturer {
          fullName
          avatar
          id
        }
      }
    }
  }
`

export const GET_DISCUSSION = gql`
  query getDiscussion($discussionSlug: String!) {
    discussion(discussionSlug: $discussionSlug) {
      id
      title
      createdAt
      author {
        ... on Lecturer {
          fullName
          avatar
          id
        }
      }
      slug
      content
      isCommentsEnabled
    }
  }
`

export const GET_COMMENTS = gql`
  query getComments($discussionSlug: String!) {
    comments(discussionSlug: $discussionSlug) {
      id
      createdAt
      author {
        ... on Lecturer {
          fullName
          avatar
          id
        }
        ... on Student {
          fullName
          avatar
          id
        }
      }
      content
    }
  }
`
export const CREATE_COMMENT = gql`
  mutation createComment($input: CreateCommentInput!) {
    createComment(input: $input) {
      comment {
        id
        content
      }
    }
  }
`
