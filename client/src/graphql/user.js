import gql from 'graphql-tag'

export const UPDATE_AVATAR = gql`
  mutation updateAvatar($input: UpdateAvatarInput!) {
    updateAvatar(input: $input) {
      avatarUrl
    }
  }
`

export const GET_CURRENT_USER = gql`
  query getCurrentUser {
    currentUser {
      ... on Student {
        id
        email
        fullName
        avatar
      }
      ... on Lecturer {
        id
        email
        fullName
        avatar
      }
    }
  }
`

export const GET_CURRENT_USER_NOTIFICATIONS = gql`
  query getCurrentUserNotifications {
    notifications {
      id
      readAt
      seenAt
      content
      actor {
        ... on Student {
          id
          email
          fullName
          avatar
        }
        ... on Lecturer {
          id
          email
          fullName
          avatar
        }
      }
      notifiable {
        ... on Discussion {
          id
          slug
        }
        ... on Comment {
          id
          slug: discussionSlug
        }
        ... on GradeColumn {
          registeredClass {
            id
            slug
          }
        }
      }
      navigateLink
    }
  }
`

export const MARK_ALL_SEEN_FOR_NOTIFICATIONS = gql`
  mutation markAllSeenForNotifications($input: MarkAllSeenInput!) {
    markAllSeen(input: $input) {
      marked
    }
  }
`

export const MARK_AS_READ = gql`
  mutation markAsRead($input: MarkAsReadInput!) {
    markAsRead(input: $input) {
      marked
    }
  }
`
