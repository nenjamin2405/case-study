import gql from 'graphql-tag'

const section = gql`
  fragment Section on RegisteredClass {
    slug
    id
    classRoom {
      id
      code
    }
    classSections
    groupable {
      ... on TheoryGroup {
        id
        code
        name
        lecturer {
          id
          fullName
        }
      }
      ... on LabGroup {
        id
        code
        name
        lecturer {
          id
          fullName
        }
      }
    }
  }
`
export const GET_TIMETABLE_OF_CURRENT_STUDENT = gql`
  query getTimetableForCurrentStudent($week: String!) {
    timetableForCurrentStudent(week: $week) {
      section
      monday {
        ...Section
      }
      tuesday {
        ...Section
      }
      wednesday {
        ...Section
      }
      thursday {
        ...Section
      }
      friday {
        ...Section
      }
      saturday {
        ...Section
      }
    }
  }
  ${section}
`

export const GET_TIMETABLE_WEEKS = gql`
  query getTimetableWeeks {
    timetableWeeks {
      weeks
      weekCoversCurrentTime
    }
  }
`

export const GET_TIMETABLE_OF_CURRENT_LECTURER = gql`
  query getTimetableForCurrentLecturer($week: String!) {
    timetableForCurrentLecturer(week: $week) {
      section
      monday {
        ...Section
      }
      tuesday {
        ...Section
      }
      wednesday {
        ...Section
      }
      thursday {
        ...Section
      }
      friday {
        ...Section
      }
      saturday {
        ...Section
      }
    }
  }
  ${section}
`

export const GET_LECTURER_TIMETABLE_WEEKS = gql`
  query getLecturerTimetableWeeks {
    lecturerTimetableWeeks {
      weeks
      weekCoversCurrentTime
    }
  }
`
