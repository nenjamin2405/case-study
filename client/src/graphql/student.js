import gql from 'graphql-tag'

export const GET_STUDENT_REGISTERED_CLASSES = gql`
  query getLecturerRegisteredClasses {
    studentRegisteredClasses {
      semester {
        classesAnalysisOfCurrentStudent {
          systemFour
          classification
          creditsAchieved
          averageGrade
        }
        startYear
        endYear
        id
        category
      }
      labGroups {
        id
        registeredClass {
          id
          day
          amountOfStudents
          averageGradeOfCurrentStudent
          day
          slug
          credits
        }
        laboratory {
          id
          code
          name
        }
      }
      theoryGroups {
        id
        registeredClass {
          id
          day
          amountOfStudents
          averageGradeOfCurrentStudent
          day
          slug
          credits
        }
        course {
          id
          code
          name
        }
      }
    }
  }
`
export const GET_ALL_SEMESTERS_GRADES_ANALYSIS = gql`
  query getAllSemestersGradesAnalysis {
    allSemestersGradesAnalysis {
      systemFour
      averageGrade
      creditsAchieved
      classification
    }
  }
`
