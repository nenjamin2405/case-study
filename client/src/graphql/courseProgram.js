import gql from 'graphql-tag'

export const GET_STUDENT_COURSE_PROGRAM = gql`
  query getStudentCourseProgram {
    studentCourseProgram {
      category
      classSections
      code
      credits
      description
      gpaCalculated
      name
      id
      canBeEnrollFromYear
      statusWithCurrentStudent
      laboratory {
        id
        name
        code
        credits
        description
        classSections
        statusWithCurrentStudent
      }
    }
  }
`
