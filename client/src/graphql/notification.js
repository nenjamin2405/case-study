import gql from 'graphql-tag'

export const REGISTER_NOTI_KEY = gql`
  mutation registerNotiKey($input: RegisterNotiKeyInput!) {
    registerNotiKey(input: $input) {
      registered
    }
  }
`

export const UNREGISTER_NOTI_KEY = gql`
  mutation unregisterNotiKey($input: UnregisterNotiKeyInput!) {
    unregisterNotiKey(input: $input) {
      unRegistered
    }
  }
`
