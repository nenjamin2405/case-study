import gql from 'graphql-tag'

export const CREATE_RECORD = gql`
  mutation createRecord($input: CreateRecordInput!) {
    createRecord(input: $input) {
      record {
        ... on GradeColumn {
          id
          percentage
          name
        }
      }
    }
  }
`

export const UPDATE_RECORD = gql`
  mutation updateRecord($input: UpdateRecordInput!) {
    updateRecord(input: $input) {
      record {
        ... on GradeColumn {
          id
          percentage
          name
        }
      }
    }
  }
`
