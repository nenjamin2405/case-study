import logo from '../images/logo.png'

export const CLOUD_NAME = 'ezgamio'
export const CLOUD_URL =
  'http://res.cloudinary.com/ezgamio/image/upload/c_scale,h_40,w_40/'

export const RECORD_TYPE = {
  GRADE_COLUMN: 'GradeColumn',
}
export const NOTIFICATION_TYPE = {
  COMMENT: 'Comment',
  DISCUSSION: 'Discussion',
  GRADE_COLUMN_PUBLISHMENT: 'Grade Column Publishment',
}

export const APP_LOGO = logo
