const dev = process.env.NODE_ENV === 'development'

export const API_URL = dev ? 'http://localhost:3001' : process.env.API_URL
export const CLIENT_URL = dev
  ? 'http://localhost:3000'
  : 'http://iu-case-study.herokuapp.com'

export const LOGO_URL =
  'https://cdn.dribbble.com/users/28449/screenshots/1040285/cap-logo-ideas3.png'
