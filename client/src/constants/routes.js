export const ROUTES = {
  RESET_PASSWORD: '/password-reset',
  SIGNUP_SUCCESS: '/signup-success',
  HOME: '/',
  LOGIN: '/login',
  STUDENT: {
    courseRegistration: '/course-registration',
    timetable: '/timetable',
    schoolfee: '/school-fee',
    courseProgram: '/course-program',
    registeredClasses: '/registered-classes',
  },
  LECTURER: {
    teachingClasses: '/teaching-classes',
    lecturerTimetable: '/lecturer-timetable',
  },
  CLASS: {
    home: '/class/:slug',
    students: '/class/:slug/students',
    discussions: '/class/:slug/discussions',
    discussion: '/class/:slug/discussions/:discussionSlug',
    announcements: '/class/:slug/announcements',
    announcement: '/class/:slug/announcements/:discussionSlug',
  },
}
