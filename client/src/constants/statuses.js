export const ENROLMENT_STATUS = {
  STATUS_INCOMPLETED: { value: 'Incompleted', color: 'magenta' },
  STATUS_IN_PROGRESS: { value: 'In Progress', color: 'cyan' },
  STATUS_PASSED: { value: 'Passed', color: 'green' },
  STATUS_FAILED: { value: 'Failed', color: 'red' },
  STATUS_NOT_LEARNED: { value: 'Not Learned', color: 'purple' },
}
