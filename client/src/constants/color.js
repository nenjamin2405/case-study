export const COLOR = {
  SECONDARY_COLOR: '#0747A6',
  MAIN_GREEN: '#52c41a',
  MAIN_BLUE: '#1890ff',
  MAIN_MAGENTA: '#eb2f96',
  MAIN_PURPLE: '#faad14',
}
