export const NOTIFICATION_ACTION = {
  posted: 'posted',
  commented: 'commented',
}

export const NOTIFICATION = {
  notiKey: 'notiKey',
}
