export const TABS = {
  STUDENTS: 'students',
  DISCUSSIONS: 'discussions',
  ANNOUNCEMENTS: 'announcements',
  HOME: 'home',
}

export const DISCUSSION_TYPE = {
  DISCUSSION: 'discussion',
  ANNOUNCEMENT: 'announcement',
}
