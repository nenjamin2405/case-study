export const WEEKDAYS = {
  MON: 'monday',
  TUES: 'tuesday',
  WEDS: 'wednesday',
  THURS: 'thursday',
  FRI: 'friday',
  SATUR: 'saturday',
}

export const SECTIONS = [
  {
    index: 1,
    startTime: '7:10',
    endTime: '7:55',
  },
  {
    index: 2,
    startTime: '8:00',
    endTime: '8:45',
  },
  {
    index: 3,
    startTime: '8:50',
    endTime: '9:35',
  },
  {
    index: 4,
    startTime: '9:45',
    endTime: '10:30',
  },
  {
    index: 5,
    startTime: '10:50',
    endTime: '11:35',
  },
  {
    index: 6,
    startTime: '11:45',
    endTime: '12:30',
  },
  {
    index: 7,
    startTime: '12:50',
    endTime: '13:35',
  },
  {
    index: 8,
    startTime: '13:45',
    endTime: '14:30',
  },
  {
    index: 9,
    startTime: '14:35',
    endTime: '15:20',
  },
  {
    index: 10,
    startTime: '15:25',
    endTime: '16:10',
  },
]
