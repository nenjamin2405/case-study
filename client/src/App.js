import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { Query } from 'react-apollo'
import { withRouter } from 'react-router'
import PrivateRoute from './components/PrivateRoute'
import UnauthRoute from './components/UnauthRoute'
import Login from './pages/Login'
import Home from './pages/Home'
import NavigationLayout from './components/NavigationLayout'
import { ROUTES } from './constants/routes'
import ResetPassword from './pages/ResetPassword'
import SignupSuccess from './pages/SignupSuccess'
import CourseRegistration from './pages/CourseRegistration'
import Timetable from './pages/Timetable'
import SchoolFee from './pages/SchoolFee'
import CourseProgram from './pages/CourseProgram'
import { GET_CURRENT_USER } from './graphql/user'
import TeachingClasses from './pages/TeachingClasses'
import LecturerTimetable from './pages/LecturerTimetable'
import ClassStudent from './pages/ClassStudents'
import ClassHome from './pages/ClassHome'
import RegisteredClasses from './pages/RegisteredClasses'
import ClassDiscussions from './pages/ClassDiscussions'
import ClassAnnouncements from './pages/ClassAnnouncements'
import NotificationService from './services/NotificationService'

class App extends React.Component {
  async componentDidMount() {
    const notiService = new NotificationService(
      this.props.client,
      this.props.history
    )
    notiService.checkPermissions()
  }

  render() {
    return (
      <Query query={GET_CURRENT_USER}>
        {({ data, loading, error }) => {
          if (loading) return <div>Loading...</div>
          if (error) return `Error! ${error.message}`
          return (
            <React.Fragment>
              <NavigationLayout hidden={!data.currentUser}>
                <Switch>
                  <PrivateRoute exact path={ROUTES.HOME} component={Home} />
                  <PrivateRoute
                    exact
                    path={ROUTES.STUDENT.courseRegistration}
                    component={CourseRegistration}
                  />
                  <PrivateRoute
                    exact
                    path={ROUTES.STUDENT.timetable}
                    component={Timetable}
                  />
                  <PrivateRoute
                    exact
                    path={ROUTES.STUDENT.schoolfee}
                    component={SchoolFee}
                  />
                  <PrivateRoute
                    exact
                    path={ROUTES.STUDENT.courseProgram}
                    component={CourseProgram}
                  />
                  <PrivateRoute
                    exact
                    path={ROUTES.STUDENT.registeredClasses}
                    component={RegisteredClasses}
                  />
                  <PrivateRoute
                    exact
                    path={ROUTES.LECTURER.teachingClasses}
                    component={TeachingClasses}
                  />
                  <PrivateRoute
                    exact
                    path={ROUTES.LECTURER.lecturerTimetable}
                    component={LecturerTimetable}
                  />
                  <PrivateRoute
                    exact
                    path={ROUTES.CLASS.students}
                    component={ClassStudent}
                  />
                  <PrivateRoute
                    exact
                    path={ROUTES.CLASS.home}
                    component={ClassHome}
                  />
                  <PrivateRoute
                    exact
                    path={ROUTES.CLASS.discussions}
                    component={ClassDiscussions}
                  />
                  <PrivateRoute
                    exact
                    path={ROUTES.CLASS.discussion}
                    component={ClassDiscussions}
                  />
                  <PrivateRoute
                    exact
                    path={ROUTES.CLASS.announcements}
                    component={ClassAnnouncements}
                  />
                  <PrivateRoute
                    exact
                    path={ROUTES.CLASS.announcement}
                    component={ClassAnnouncements}
                  />
                  <UnauthRoute
                    exact
                    path={ROUTES.SIGNUP_SUCCESS}
                    component={SignupSuccess}
                  />
                  <UnauthRoute exact path={ROUTES.LOGIN} component={Login} />
                  <UnauthRoute
                    exact
                    path={ROUTES.RESET_PASSWORD}
                    component={ResetPassword}
                  />
                  <Route path="empty" component={null} key="empty" />
                </Switch>
              </NavigationLayout>
            </React.Fragment>
          )
        }}
      </Query>
    )
  }
}

export default withRouter(App)
