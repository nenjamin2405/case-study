import { notification as NotiModal } from 'antd'
import { messagingClient } from '../firebase'

import { NOTIFICATION } from '../constants/notification'
import { REGISTER_NOTI_KEY, UNREGISTER_NOTI_KEY } from '../graphql/notification'
import {
  navigateAndScrollToComment,
  markRead,
  fetchGradeColumnsAndNavigateToGradeTable,
} from '../utils/notification'
import { GET_CURRENT_USER_NOTIFICATIONS } from '../graphql/user'
import { NOTIFICATION_TYPE } from '../constants/app'

const popupkey = 'notificationPopup'
export default class NotificationService {
  constructor(apolloClient, history) {
    this.apolloClient = apolloClient
    this.history = history
  }

  checkPermissions = async () => {
    try {
      await messagingClient.requestPermission()
      await this.getToken()
      this.notificationListener()
    } catch (error) {
      alert('Please allow notification for receiving the best user experience')
    }
  }

  getToken = async () => {
    let fcmToken = await localStorage.getItem(NOTIFICATION.notiKey)
    if (!fcmToken) {
      fcmToken = await messagingClient.getToken()
      this.registerToken(fcmToken)
    }
  }

  registerToken = async fcmToken => {
    await this.apolloClient.mutate({
      mutation: REGISTER_NOTI_KEY,
      variables: {
        input: {
          notiKey: fcmToken,
        },
      },
    })
    await localStorage.setItem(NOTIFICATION.notiKey, fcmToken)
  }

  notificationListener = async () => {
    messagingClient.onMessage(payload => {
      this.apolloClient.query({
        query: GET_CURRENT_USER_NOTIFICATIONS,
        fetchPolicy: 'network-only',
      })
      const { notification, data } = payload
      const { title, body } = notification
      const navigateLink = data['gcm.notification.navigateLink']
      const notificationPayload = JSON.parse(
        data['gcm.notification.notification']
      )
      const { type } = notificationPayload
      NotiModal.open({
        message: title,
        description: body,
        placement: 'bottomRight',
        key: popupkey,
        style: {
          cursor: 'pointer',
        },
        onClick: async () => {
          markRead(notificationPayload, this.apolloClient)
          switch (type) {
            case NOTIFICATION_TYPE.GRADE_COLUMN_PUBLISHMENT:
              fetchGradeColumnsAndNavigateToGradeTable({
                client: this.apolloClient,
                history: this.history,
                navigateLink,
                notification: notificationPayload,
              })
              break

            default:
              navigateAndScrollToComment({
                client: this.apolloClient,
                history: this.history,
                navigateLink,
                notification: notificationPayload,
              })
              break
          }

          NotiModal.close(popupkey)
        },
      })
    })
  }

  unregisterToken = async () => {
    const fcmToken = await localStorage.getItem(NOTIFICATION.notiKey)
    await this.apolloClient.mutate({
      mutation: UNREGISTER_NOTI_KEY,
      variables: {
        input: {
          notiKey: fcmToken,
        },
      },
    })
    await localStorage.removeItem(NOTIFICATION.notiKey)
  }
}
