import React from 'react'
import ClassLayout from '../components/ClassLayout'
import { TABS } from '../constants/registeredClass'

const ClassAnnouncements = () => (
  <React.Fragment>
    <ClassLayout activeKey={TABS.ANNOUNCEMENTS} />
  </React.Fragment>
)

export default ClassAnnouncements
