import React from 'react'
import { Table, Tag } from 'antd'

const columns = [
  {
    title: 'Code',
    key: 'code',
    render: record => record.registeredClass.groupable.code,
  },
  {
    title: 'Name',
    key: 'name',
    render: record => (
      <Tag color="blue">{record.registeredClass.groupable.name}</Tag>
    ),
  },
  {
    title: 'Credits',
    dataIndex: 'credits',
    align: 'center',
  },
  {
    title: 'Fee',
    dataIndex: 'fee',
    render: fee => `$ ${fee}`,
  },
]

const RegisteredClassesTable = ({ enrolments }) => (
  <Table columns={columns} dataSource={enrolments} rowKey="id" />
)

export default RegisteredClassesTable
