import React from 'react'
import { List, Card } from 'antd'
import { COLOR } from '../../../constants/color'

const Description = ({ fee, remit, paid }) => {
  const data = [
    {
      title: 'Total Fee',
      value: `$ ${fee}`,
      color: COLOR.MAIN_BLUE,
    },
    {
      title: 'Remit',
      value: `$ ${remit}`,
      color: COLOR.MAIN_MAGENTA,
    },
    {
      title: 'Paid',
      value: `$ ${paid}`,
      color: COLOR.MAIN_GREEN,
    },
    {
      title: 'Total Payable Fee',
      value: `$ ${fee - remit - paid}`,
      color: COLOR.MAIN_PURPLE,
    },
  ]

  return (
    <React.Fragment>
      <List
        grid={{ gutter: 16, column: 4 }}
        dataSource={data}
        renderItem={item => (
          <List.Item>
            <Card title={item.title} style={{ textAlign: 'center' }}>
              <h1 style={{ color: item.color }}>
                <b>{item.value}</b>
              </h1>
            </Card>
          </List.Item>
        )}
      />
    </React.Fragment>
  )
}

export default Description
