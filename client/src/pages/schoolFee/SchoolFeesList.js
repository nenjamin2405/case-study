import React from 'react'
import { Collapse, Alert } from 'antd'
import { isEmpty } from 'lodash'
import CustomQuery from '../../components/CustomQuery'
import { GET_STUDENT_SCHOOL_FEES } from '../../graphql/schoolFee'
import Description from './schoolFeesList/Description'
import RegisteredClassesTable from './schoolFeesList/RegisteredClassesTable'

const { Panel } = Collapse

const SchoolFeesList = () => (
  <React.Fragment>
    <CustomQuery query={GET_STUDENT_SCHOOL_FEES}>
      {({ loading, data }) => {
        if (loading) return <div>Loading...</div>
        if (isEmpty(data.studentSchoolFees))
          return (
            <Alert
              description="You don't have any school fees!"
              type="warning"
              showIcon
              message="Warning"
            />
          )
        return (
          <Collapse
            bordered={false}
            defaultActiveKey={[data.studentSchoolFees[0].id]}
          >
            {data.studentSchoolFees.map(
              ({ id, semester, fee, remit, paid, enrolments }) => (
                <Panel
                  header={`${semester.category} ${semester.startYear} - ${
                    semester.endYear
                  }`}
                  key={id}
                >
                  <Description
                    semester={semester}
                    fee={fee}
                    remit={remit}
                    paid={paid}
                  />
                  <br />
                  <RegisteredClassesTable enrolments={enrolments} />
                </Panel>
              )
            )}
          </Collapse>
        )
      }}
    </CustomQuery>
  </React.Fragment>
)

export default SchoolFeesList
