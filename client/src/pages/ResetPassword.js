import React from 'react'
import ResetPasswordForm from './resetPassword/ResetPasswordForm'

const ResetPassword = () => <ResetPasswordForm />

export default ResetPassword
