import React from 'react'
import TeachingClassesList from './teachingClasses/TeachingClassesList'

const TeachingClasses = () => (
  <React.Fragment>
    <TeachingClassesList />
  </React.Fragment>
)

export default TeachingClasses
