import React from 'react'
import { Link } from 'react-router-dom'
import { ROUTES } from '../constants/routes'

const SignupSuccess = () => (
  <React.Fragment>
    <div>Your email has successfully verifed</div>
    <Link to={ROUTES.LOGIN}>Get Started</Link>
  </React.Fragment>
)

export default SignupSuccess
