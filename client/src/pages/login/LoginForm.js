import React from 'react'
import { Form, Icon, Input, Button, Checkbox, Modal } from 'antd'
import $ from 'jquery'
import { withApollo } from 'react-apollo'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import LoadingLayout from '../../components/LoadingLayout'
import ForgotPassword from './loginForm/ForgotPassword'
import { storeUserSession } from '../../lib/storeUserSession'
import { ROUTES } from '../../constants/routes'

import { GET_CURRENT_USER } from '../../graphql/user'
import { APP_LOGO } from '../../constants/app'
import NotificationService from '../../services/NotificationService'

const FormItem = Form.Item

function showError() {
  Modal.error({
    title: 'Login Failed',
    content: 'Invalid Email/Password',
  })
}
class LoginForm extends React.Component {
  state = {
    loading: false,
  }

  handleSubmit = e => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({ loading: true })
        $.ajax({
          type: 'POST',
          url: '/student_auth/sign_in',
          data: {
            ...values,
          },
        })
          .done((response, status, jqXHR) => {
            this.setState({ loading: false })

            storeUserSession({
              accessToken: jqXHR.getResponseHeader('access-token'),
              client: jqXHR.getResponseHeader('client'),
              uid: response.data.uid,
            })
            this.props.client.resetStore().then(() =>
              this.props.client.query({
                query: GET_CURRENT_USER,
              })
            )

            this.props.history.push(ROUTES.HOME)
          })
          .fail(() => {
            $.ajax({
              type: 'POST',
              url: '/lecturer_auth/sign_in',
              data: {
                ...values,
              },
            })
              .done((response, status, jqXHR) => {
                this.setState({ loading: false })

                storeUserSession({
                  accessToken: jqXHR.getResponseHeader('access-token'),
                  client: jqXHR.getResponseHeader('client'),
                  uid: response.data.uid,
                })
                this.props.client.resetStore().then(() =>
                  this.props.client.query({
                    query: GET_CURRENT_USER,
                  })
                )

                const notiService = new NotificationService(
                  this.props.client,
                  this.props.history
                )
                notiService.checkPermissions()

                this.props.history.push(ROUTES.HOME)
              })
              .fail(() => {
                this.setState({ loading: false })
                showError()
              })
          })
      }
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    return (
      <div className="form-layout">
        <div className="form-border">
          <LoadingLayout loading={this.state.loading}>
            <Link to="/">
              <img
                src={APP_LOGO}
                style={{ height: 175, width: 'auto' }}
                className="logo"
                alt="logo"
              />
            </Link>
            <Form onSubmit={this.handleSubmit} className="login-form">
              <FormItem>
                {getFieldDecorator('email', {
                  rules: [
                    { required: true, message: 'Please input your email!' },
                  ],
                })(
                  <Input
                    prefix={
                      <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />
                    }
                    placeholder="email"
                  />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('password', {
                  rules: [
                    { required: true, message: 'Please input your Password!' },
                  ],
                })(
                  <Input
                    prefix={
                      <Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />
                    }
                    type="password"
                    placeholder="Password"
                  />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('remember', {
                  valuePropName: 'checked',
                  initialValue: true,
                })(<Checkbox>Remember me</Checkbox>)}
                <ForgotPassword />
                <Button
                  type="primary"
                  htmlType="submit"
                  className="login-form-button"
                >
                  Log in
                </Button>
              </FormItem>
            </Form>
          </LoadingLayout>
        </div>
      </div>
    )
  }
}

const LoginBox = Form.create()(LoginForm)

export default withApollo(withRouter(LoginBox))
