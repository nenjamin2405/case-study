import { Modal, Form, Input } from 'antd'
import React from 'react'

const FormItem = Form.Item

const ForgotPasswordModal = Form.create()(
  class ForgotPasswordForm extends React.Component {
    render() {
      const { visible, onCancel, onCreate, form } = this.props
      const { getFieldDecorator } = form
      return (
        <Modal
          visible={visible}
          title="Reset Email"
          okText="Send"
          onCancel={onCancel}
          onOk={onCreate}
        >
          <Form layout="vertical">
            <FormItem label="Email">
              {getFieldDecorator('resetEmail', {
                rules: [
                  {
                    required: true,
                    message: 'Please input your email',
                  },
                ],
              })(<Input />)}
            </FormItem>
          </Form>
        </Modal>
      )
    }
  }
)

export default ForgotPasswordModal
