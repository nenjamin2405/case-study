import React from 'react'
import $ from 'jquery'
import { Modal } from 'antd'
import ForgotPasswordModal from './forgotPassword/ForgotPasswordModal'
import { ROUTES } from '../../../constants/routes'
import { CLIENT_URL } from '../../../constants/common'

export default class ForgotPassword extends React.Component {
  state = {
    visible: false,
    sendingEmail: false,
  }

  showModal = () => {
    this.setState({ visible: true })
  }

  handleCancel = () => {
    this.setState({ visible: false })
  }

  handleCreate = () => {
    const { form } = this.formRef.props
    form.validateFields((err, values) => {
      if (err) {
        return
      }
      this.setState({ sendingEmail: true })

      const email = values.resetEmail
      $.ajax({
        type: 'POST',
        url: '/student_auth/password',
        data: {
          email,
          redirect_url: CLIENT_URL + ROUTES.RESET_PASSWORD,
        },
      })
        .done(response => {
          Modal.success({
            title: 'Password Recovery Email Sent',
            content: `A message has been sent to ${email} with instructions to reset your password.`,
          })
          this.setState({ sendingEmail: false })
        })
        .fail(() => {
          $.ajax({
            type: 'POST',
            url: '/lecturer_auth/password',
            data: {
              email,
              redirect_url: CLIENT_URL + ROUTES.RESET_PASSWORD,
            },
          })
            .done(response => {
              Modal.success({
                title: 'Password Recovery Email Sent',
                content: `A message has been sent to ${email} with instructions to reset your password.`,
              })
              this.setState({ sendingEmail: false })
            })
            .fail(() => {
              this.setState({ sendingEmail: false })
              Modal.error({
                title: 'Password Reset Failed',
                content:
                  'Something went wrong in the password recovery email sending progress!',
              })
            })
        })
      form.resetFields()
      this.setState({ visible: false })
    })
  }

  saveFormRef = formRef => {
    this.formRef = formRef
  }

  render() {
    const { sendingEmail } = this.state
    return (
      <div>
        <a className="login-form-forgot" onClick={this.showModal}>
          Forgot password
        </a>
        <ForgotPasswordModal
          confirmLoading={sendingEmail}
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleCreate}
        />
      </div>
    )
  }
}
