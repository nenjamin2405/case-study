import React from 'react'
import ClassLayout from '../components/ClassLayout'
import { TABS } from '../constants/registeredClass'

const ClassStudent = () => (
  <React.Fragment>
    <ClassLayout activeKey={TABS.STUDENTS} />
  </React.Fragment>
)

export default ClassStudent
