import React from 'react'
import ClassLayout from '../components/ClassLayout'
import { TABS } from '../constants/registeredClass'

const ClassHome = () => (
  <React.Fragment>
    <ClassLayout activeKey={TABS.HOME} />
  </React.Fragment>
)

export default ClassHome
