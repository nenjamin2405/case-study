import { Form, Icon, Input, Button, Modal } from 'antd'
import React from 'react'
import $ from 'jquery'
import { withRouter } from 'react-router'
import queryString from 'query-string'
import { withApollo } from 'react-apollo'
import { storeUserSession } from '../../lib/storeUserSession'
import { ROUTES } from '../../constants/routes'
import { GET_CURRENT_USER } from '../../graphql/user'

const FormItem = Form.Item

class ResetPassword extends React.Component {
  state = {
    loading: false,
  }

  handleSubmit = e => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const params = queryString.parse(this.props.location.search)
        const { client, uid } = params
        $.ajax({
          type: 'PUT',
          url: '/student_auth/password',
          headers: {
            uid,
            client,
            'access-token': params['access-token'],
            Accept: 'application/json',
            'token-type': 'Bearer',
          },
          data: {
            password: values.password,
            password_confirmation: values.password_confirmation,
          },
        })
          .done(response => {
            storeUserSession({
              accessToken: params['access-token'],
              client,
              uid,
            })

            this.props.client.resetStore().then(() =>
              this.props.client.query({
                query: GET_CURRENT_USER,
              })
            )

            this.props.history.push(ROUTES.HOME)
          })
          .then(() => {
            this.setState({ loading: false })
          })
          .fail(error => {
            $.ajax({
              type: 'PUT',
              url: '/lecturer_auth/password',
              headers: {
                uid,
                client,
                'access-token': params['access-token'],
                Accept: 'application/json',
                'token-type': 'Bearer',
              },
              data: {
                password: values.password,
                password_confirmation: values.password_confirmation,
              },
            })
              .done(response => {
                storeUserSession({
                  accessToken: params['access-token'],
                  client,
                  uid,
                })

                this.props.client.resetStore().then(() =>
                  this.props.client.query({
                    query: GET_CURRENT_USER,
                  })
                )

                this.props.history.push(ROUTES.HOME)
              })
              .then(() => {
                this.setState({ loading: false })
              })
              .fail(error => {
                Modal.error({
                  title: 'Password Reset Failed',
                  content: JSON.parse(error.responseText).errors.full_messages
                    ? JSON.parse(error.responseText).errors.full_messages.join(
                        ', '
                      )
                    : JSON.parse(error.responseText).errors.join(', '),
                })
              })
          })
      }
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const { loading } = this.state
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <FormItem>
          {getFieldDecorator('password', {
            rules: [
              { required: true, message: 'Please input your new password!' },
            ],
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Password"
            />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password_confirmation', {
            rules: [
              { required: true, message: 'Please input password confirmation' },
            ],
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Password"
            />
          )}
        </FormItem>
        <Button
          type="primary"
          htmlType="submit"
          className="login-form-button"
          loading={loading}
        >
          Change password
        </Button>
      </Form>
    )
  }
}

const ResetPasswordForm = Form.create()(ResetPassword)

export default withApollo(withRouter(ResetPasswordForm))
