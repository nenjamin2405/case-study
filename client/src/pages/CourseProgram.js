import React from 'react'
import CourseProgramList from './courseProgram/CourseProgramList'

const CourseProgram = () => (
  <React.Fragment>
    <CourseProgramList />
  </React.Fragment>
)

export default CourseProgram
