import React from 'react'
import { Collapse, Alert, Divider, List, Icon, Avatar } from 'antd'
import { Link } from 'react-router-dom'
import { isEmpty } from 'lodash'
import CustomQuery from '../../components/CustomQuery'
import { GET_LECTURER_TEACHING_CLASSES } from '../../graphql/lecturer'
import { COLOR } from '../../constants/color'

const { Panel } = Collapse

const IconText = ({ type, text }) => (
  <span>
    <Icon type={type} style={{ marginRight: 8 }} />
    {text !== null ? text : 'No Data'}
  </span>
)

const TeachingClassesList = () => (
  <React.Fragment>
    <CustomQuery query={GET_LECTURER_TEACHING_CLASSES}>
      {({ loading, data }) => {
        if (loading) return <div>Loading...</div>
        if (isEmpty(data.lecturerTeachingClasses))
          return (
            <Alert
              description="You don't have any teaching classes!"
              type="warning"
              showIcon
              message="Warning"
            />
          )
        return (
          <Collapse
            bordered={false}
            defaultActiveKey={[data.lecturerTeachingClasses[0].semester.id]}
          >
            {data.lecturerTeachingClasses.map(
              ({
                semester: { id, category, startYear, endYear },
                theoryGroups,
                labGroups,
              }) => (
                <Panel
                  header={`${category} ${startYear} - ${endYear}`}
                  key={id}
                >
                  {!isEmpty(theoryGroups) && (
                    <React.Fragment>
                      <Divider orientation="left">Theory Groups</Divider>
                      <List
                        itemLayout="vertical"
                        dataSource={theoryGroups}
                        renderItem={theoryGroup => (
                          <List.Item
                            actions={[
                              <IconText
                                type="team"
                                text={
                                  theoryGroup.registeredClass.amountOfStudents
                                }
                              />,
                              <IconText
                                type="calendar"
                                text={theoryGroup.registeredClass.day}
                              />,
                            ]}
                          >
                            <List.Item.Meta
                              avatar={
                                <Avatar
                                  style={{ backgroundColor: COLOR.MAIN_BLUE }}
                                >
                                  <Icon type="read" />
                                </Avatar>
                              }
                              title={
                                <Link
                                  to={`/class/${
                                    theoryGroup.registeredClass.slug
                                  }`}
                                  style={{ color: COLOR.MAIN_BLUE }}
                                >
                                  {theoryGroup.course.name}
                                </Link>
                              }
                            />
                          </List.Item>
                        )}
                      />
                    </React.Fragment>
                  )}
                  {!isEmpty(labGroups) && (
                    <React.Fragment>
                      <Divider orientation="left">Lab Groups</Divider>
                      <List
                        itemLayout="vertical"
                        dataSource={labGroups}
                        renderItem={labGroup => (
                          <List.Item
                            actions={[
                              <IconText
                                type="team"
                                text={labGroup.registeredClass.amountOfStudents}
                              />,
                              <IconText
                                type="calendar"
                                text={labGroup.registeredClass.day}
                              />,
                            ]}
                          >
                            <List.Item.Meta
                              avatar={
                                <Avatar
                                  style={{ backgroundColor: COLOR.MAIN_GREEN }}
                                >
                                  <Icon type="bg-colors" />
                                </Avatar>
                              }
                              title={
                                <Link
                                  to={`/class/${labGroup.registeredClass.slug}`}
                                  style={{ color: COLOR.MAIN_GREEN }}
                                >
                                  {labGroup.laboratory.name}
                                </Link>
                              }
                            />
                          </List.Item>
                        )}
                      />
                    </React.Fragment>
                  )}
                </Panel>
              )
            )}
          </Collapse>
        )
      }}
    </CustomQuery>
  </React.Fragment>
)

export default TeachingClassesList
