import React from 'react'
import { Table, Tag, Icon, Card } from 'antd'
import CustomQuery from '../../components/CustomQuery'
import { GET_STUDENT_COURSE_PROGRAM } from '../../graphql/courseProgram'
import { ENROLMENT_STATUS } from '../../constants/statuses'

const renderStatusWithCurrentStuent = status => {
  const enrolStatus = Object.keys(ENROLMENT_STATUS).find(
    key => ENROLMENT_STATUS[key].value === status
  )

  if (enrolStatus) {
    return <Tag color={ENROLMENT_STATUS[enrolStatus].color}>{status}</Tag>
  }
  return <Tag color="geekblue">{status}</Tag>
}

const columns = [
  {
    title: 'Code',
    key: 'code',
    dataIndex: 'code',
  },
  {
    title: 'Name',
    key: 'name',
    dataIndex: 'name',
    render: name => <Tag color="blue">{name}</Tag>,
  },
  {
    title: 'Credits',
    dataIndex: 'credits',
    align: 'center',
  },
  {
    title: 'Category',
    dataIndex: 'category',
    render: category => <b>{category}</b>,
  },
  {
    title: 'Year Can Be Enrolled',
    dataIndex: 'canBeEnrollFromYear',
    align: 'center',
    render: year => <b>{year}</b>,
  },
  {
    title: 'Has Laboratory',
    align: 'center',
    render: record =>
      record.laboratory ? (
        <Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" />
      ) : (
        <Icon type="close-circle" theme="twoTone" twoToneColor="#eb2f96" />
      ),
  },
  {
    title: 'GPA Calculated',
    dataIndex: 'gpaCalculated',
    align: 'center',
    render: gpaCalculated =>
      gpaCalculated ? (
        <Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" />
      ) : (
        <Icon type="close-circle" theme="twoTone" twoToneColor="#eb2f96" />
      ),
  },
  {
    title: 'Status',
    dataIndex: 'statusWithCurrentStudent',
    align: 'center',
    render: status => renderStatusWithCurrentStuent(status),
  },
]

const CourseProgramList = () => (
  <CustomQuery query={GET_STUDENT_COURSE_PROGRAM}>
    {({ loading, data }) => (
      <Table
        columns={columns}
        dataSource={data.studentCourseProgram}
        loading={loading}
        rowKey="id"
        expandedRowRender={record => (
          <div style={{ margin: 0 }}>
            <p>{record.description}</p>
            {record.laboratory && (
              <React.Fragment>
                <Card title="Laboratory">
                  <p>
                    <b>Code:</b> {record.laboratory.code}
                  </p>
                  <p>
                    <b>Name:</b> {record.laboratory.name}
                  </p>
                  <p>
                    <b>Credits:</b> {record.laboratory.credits}
                  </p>
                </Card>
              </React.Fragment>
            )}
          </div>
        )}
      />
    )}
  </CustomQuery>
)

export default CourseProgramList
