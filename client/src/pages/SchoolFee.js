import React from 'react'
import SchoolFeesList from './schoolFee/SchoolFeesList'

const SchoolFee = () => (
  <React.Fragment>
    <SchoolFeesList />
  </React.Fragment>
)

export default SchoolFee
