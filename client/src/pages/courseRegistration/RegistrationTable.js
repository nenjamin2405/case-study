import React from 'react'
import { withApollo } from 'react-apollo'
import { Table, Alert, Divider, Button, Icon, Progress, Row } from 'antd'
import moment from 'moment'
import Countdown from 'react-countdown-now'
import './RegistrationTable.scss'
import {
  GET_CLASSES_FOR_CURRENT_STUDENT,
  ENROLL_COURSES,
} from '../../graphql/registration'
import CustomMutation from '../../components/CustomMutation'
import CustomQuery from '../../components/CustomQuery'

const PAGINATION_OPTIONS = {
  defaultPageSize: 5,
}

const CountdownRegistration = ({ endTime, client }) => {
  if (endTime) {
    const currentTimeMoment = moment()
    const endTimeMoment = moment(endTime, 'HH:mm')
    const diff = endTimeMoment.diff(currentTimeMoment, 'miliseconds')

    return (
      <Countdown
        date={Date.now() + diff}
        renderer={({ hours, minutes, seconds, completed }) => {
          if (completed) {
            const data = client.readQuery({
              query: GET_CLASSES_FOR_CURRENT_STUDENT,
            })
            client.writeQuery({
              query: GET_CLASSES_FOR_CURRENT_STUDENT,
              data: {
                classesForCurrentStudentToRegister: {
                  ...data.classesForCurrentStudentToRegister,
                  itIsTimeForRegistration: false,
                },
              },
            })
            return (
              <Progress
                type="circle"
                percent={100}
                strokeColor="#f5222d"
                format={() => (
                  <span style={{ color: '#f5222d' }}>Time Over</span>
                )}
              />
            )
          }
          return (
            <Progress
              type="circle"
              percent={100}
              strokeColor="#1890ff"
              format={() => (
                <span style={{ color: '#1890ff' }}>
                  {hours}:{minutes}:{seconds}
                </span>
              )}
            />
          )
        }}
      />
    )
  }
  return null
}

const COLUMNS = [
  {
    title: 'Code',
    dataIndex: 'code',
    render: (_, record) => record.course.code,
  },
  {
    title: 'Course Name',
    dataIndex: 'courseName',
    render: (_, record) => record.course.name,
  },
  {
    title: 'Credits',
    dataIndex: 'credits',
    render: (_, record) => record.course.credits,
  },
  {
    title: 'Lecturer',
    dataIndex: 'lecturer',
    render: (_, record) => record.lecturer.fullName,
  },
  {
    title: 'Slots',
    dataIndex: 'slot',
    render: (_, record) =>
      record.labGroups && record.labGroups.length > 0 ? null : record.slot,
  },
  {
    title: 'Remaining Slots',
    dataIndex: 'remainingSlot',
    render: (_, record) =>
      record.labGroups && record.labGroups.length > 0
        ? null
        : record.remainingSlot,
  },
  {
    title: 'Start From',
    dataIndex: 'startTime',
    render: startTime => moment(startTime).format('MMMM Do YYYY'),
  },
  {
    title: 'End At',
    dataIndex: 'endTime',
    render: endTime => moment(endTime).format('MMMM Do YYYY'),
  },
  {
    title: 'Class Room',
    dataIndex: 'classRoom',
    render: (_, record) => record.registeredClass.classRoom.code,
  },
  {
    title: 'Week Day',
    dataIndex: 'weekDay',
    render: (_, record) => record.registeredClass.day,
  },
]

const RegistrationTable = ({ client }) => {
  const rowSelectionOfAllCoursesTable = (
    selectedClassIds,
    itIsTimeForRegistration
  ) => ({
    onChange: selectedRowKeys => {
      const data = client.readQuery({
        query: GET_CLASSES_FOR_CURRENT_STUDENT,
      })
      client.writeQuery({
        query: GET_CLASSES_FOR_CURRENT_STUDENT,
        data: {
          classesForCurrentStudentToRegister: {
            ...data.classesForCurrentStudentToRegister,
            registeredClassIds: selectedRowKeys,
          },
        },
      })
    },
    selectedRowKeys: selectedClassIds,
    getCheckboxProps: record => ({
      disabled:
        !itIsTimeForRegistration ||
        (record.labGroups && record.labGroups.length > 0),
    }),
    hideDefaultSelections: true,
  })

  return (
    <CustomQuery query={GET_CLASSES_FOR_CURRENT_STUDENT}>
      {({ data, loading, error }) => {
        if (error) return `Error! ${error.message}`
        if (!loading && !data.classesForCurrentStudentToRegister)
          return (
            <Alert
              description="You have nothing to register now!"
              type="warning"
              showIcon
              message="Warning"
            />
          )
        return (
          <React.Fragment>
            {data.classesForCurrentStudentToRegister &&
            data.classesForCurrentStudentToRegister.endTime ? (
              <Row
                type="flex"
                justify="center"
                align="middle"
                style={{ paddingTop: 20, paddingBottom: 40 }}
              >
                <CountdownRegistration
                  endTime={data.classesForCurrentStudentToRegister.endTime}
                  client={client}
                />
              </Row>
            ) : null}

            <div>
              <Table
                rowKey={record => record.registeredClass.id}
                expandedRowKeys={
                  data.classesForCurrentStudentToRegister &&
                  data.classesForCurrentStudentToRegister.theoryGroups
                    ? data.classesForCurrentStudentToRegister.theoryGroups.map(
                        theoryGroup => theoryGroup.registeredClass.id
                      )
                    : []
                }
                columns={COLUMNS}
                rowSelection={rowSelectionOfAllCoursesTable(
                  data.classesForCurrentStudentToRegister
                    ? data.classesForCurrentStudentToRegister.registeredClassIds
                    : [],
                  data.classesForCurrentStudentToRegister &&
                  data.classesForCurrentStudentToRegister
                    .itIsTimeForRegistration
                    ? data.classesForCurrentStudentToRegister
                        .itIsTimeForRegistration
                    : false
                )}
                pagination={PAGINATION_OPTIONS}
                dataSource={
                  data.classesForCurrentStudentToRegister &&
                  data.classesForCurrentStudentToRegister.theoryGroups
                    ? data.classesForCurrentStudentToRegister.theoryGroups.map(
                        theoryGroup => {
                          const { labGroups } = theoryGroup
                          const returnData = {
                            ...theoryGroup,
                            children: labGroups.length > 0 ? labGroups : null,
                          }
                          return returnData
                        }
                      )
                    : []
                }
                loading={loading}
              />
            </div>
            <Divider orientation="left">Selected Courses</Divider>
            <CustomMutation
              mutation={ENROLL_COURSES}
              variables={{
                input: {
                  classIds: data.classesForCurrentStudentToRegister
                    ? data.classesForCurrentStudentToRegister.registeredClassIds
                    : null,
                },
              }}
              refetchQueries={[{ query: GET_CLASSES_FOR_CURRENT_STUDENT }]}
            >
              {(enrollCourses, { loading: loadingEnroll }) => (
                <React.Fragment>
                  <Button
                    disabled={
                      data.classesForCurrentStudentToRegister
                        ? !data.classesForCurrentStudentToRegister
                            .itIsTimeForRegistration
                        : false
                    }
                    onClick={() => enrollCourses()}
                    loading={loadingEnroll}
                    type="primary"
                    style={{ marginBottom: 16 }}
                  >
                    Save Registration
                  </Button>

                  <Table
                    rowKey={record => record.registeredClass.id}
                    columns={[
                      ...COLUMNS,
                      {
                        title: 'Stored to database',
                        align: 'center',
                        dataIndex: 'storedToDatabase',
                        render: (_, record) =>
                          record.registeredClass.registeredByCurrentStudent ? (
                            <Icon
                              type="check-circle"
                              theme="twoTone"
                              twoToneColor="#52c41a"
                            />
                          ) : (
                            <Icon
                              type="close-circle"
                              theme="twoTone"
                              twoToneColor="#eb2f96"
                            />
                          ),
                      },
                    ]}
                    dataSource={
                      data.classesForCurrentStudentToRegister
                        ? data.classesForCurrentStudentToRegister.theoryGroups
                            .filter(theoryGroup => {
                              const {
                                registeredClassIds,
                              } = data.classesForCurrentStudentToRegister
                              const classFound = registeredClassIds.find(
                                classId =>
                                  theoryGroup.registeredClass.id === classId ||
                                  theoryGroup.labGroups.find(
                                    labGroup =>
                                      labGroup.registeredClass.id === classId
                                  )
                              )
                              if (classFound) {
                                return true
                              }
                              return false
                            })
                            .map(theoryGroup => {
                              const { labGroups } = theoryGroup
                              const returnData = {
                                ...theoryGroup,
                                children:
                                  labGroups.length > 0
                                    ? labGroups.filter(labGroup =>
                                        data.classesForCurrentStudentToRegister.registeredClassIds.includes(
                                          labGroup.registeredClass.id
                                        )
                                      )
                                    : null,
                              }
                              return returnData
                            })
                        : []
                    }
                    pagination={false}
                    loading={loading || loadingEnroll}
                  />
                </React.Fragment>
              )}
            </CustomMutation>
          </React.Fragment>
        )
      }}
    </CustomQuery>
  )
}
export default withApollo(RegistrationTable)
