import React from 'react'
import ProfessorTimetable from './lecturerTimetable/ProfessorTimetable'

const LecturerTimetable = () => (
  <React.Fragment>
    <ProfessorTimetable />
  </React.Fragment>
)

export default LecturerTimetable
