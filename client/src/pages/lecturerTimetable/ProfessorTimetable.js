import React from 'react'
import { capitalize, cloneDeep } from 'lodash'
import { Link } from 'react-router-dom'

import { Table, Select, Row, Col, Button, Alert, Icon } from 'antd'
import { ApolloConsumer } from 'react-apollo'
import { WEEKDAYS } from '../../constants/time'
import CustomQuery from '../../components/CustomQuery'
import {
  GET_TIMETABLE_OF_CURRENT_LECTURER,
  GET_LECTURER_TIMETABLE_WEEKS,
} from '../../graphql/timetable'

const { Option } = Select

const renderContent = registeredClass => {
  const obj = {
    children: null,
    props: {},
  }
  if (registeredClass) {
    const { groupable, classRoom, classSections, slug } = registeredClass
    const { name: courseName } = groupable
    const { code: roomCode } = classRoom
    obj.children = (
      <div>
        <Link to={`/class/${slug}`}>{courseName}</Link>

        <p>
          <Icon
            type="shop"
            theme="twoTone"
            style={{ marginRight: 5 }}
            twoToneColor="#52c41a"
          />
          {roomCode}
        </p>
      </div>
    )
    obj.props.rowSpan = classSections
  }
  return obj
}

const columns = [
  {
    title: 'Section',
    key: 'section',
    dataIndex: 'section',
    align: 'center',
  },
  ...Object.keys(WEEKDAYS).map(key => ({
    title: capitalize(WEEKDAYS[key]),
    key: WEEKDAYS[key],
    dataIndex: WEEKDAYS[key],
    render: renderContent,
    align: 'center',
  })),
]

const isFirstWeek = client => {
  const dataLecturerTimetableWeeks = cloneDeep(
    client.readQuery({
      query: GET_LECTURER_TIMETABLE_WEEKS,
    })
  )
  const {
    weeks,
    weekCoversCurrentTime,
  } = dataLecturerTimetableWeeks.lecturerTimetableWeeks
  return weeks.shift() === weekCoversCurrentTime
}

const isLastWeek = client => {
  const dataLecturerTimetableWeeks = cloneDeep(
    client.readQuery({
      query: GET_LECTURER_TIMETABLE_WEEKS,
    })
  )
  const {
    weeks,
    weekCoversCurrentTime,
  } = dataLecturerTimetableWeeks.lecturerTimetableWeeks
  return weeks.pop() === weekCoversCurrentTime
}

const ProfessorTimetable = () => (
  <React.Fragment>
    <CustomQuery query={GET_LECTURER_TIMETABLE_WEEKS}>
      {({ loading: loadingTimetaleWeeks, data }) => {
        if (loadingTimetaleWeeks) return <div>Loading...</div>
        if (!data.lecturerTimetableWeeks)
          return (
            <Alert
              description="You don't have any timtables!"
              type="warning"
              showIcon
              message="Warning"
            />
          )
        return (
          <React.Fragment>
            <ApolloConsumer>
              {client => (
                <Row
                  type="flex"
                  justify="center"
                  align="middle"
                  style={{ paddingTop: 20, paddingBottom: 40 }}
                >
                  <Col span={2} style={{ textAlign: 'center' }}>
                    <Button
                      icon="left"
                      disabled={isFirstWeek(client)}
                      onClick={() => {
                        const dataLecturerTimetableWeeks = client.readQuery({
                          query: GET_LECTURER_TIMETABLE_WEEKS,
                        })
                        const {
                          weeks,
                          weekCoversCurrentTime,
                        } = dataLecturerTimetableWeeks.lecturerTimetableWeeks

                        const prevIndex =
                          weeks.indexOf(weekCoversCurrentTime) - 1
                        client.writeQuery({
                          query: GET_LECTURER_TIMETABLE_WEEKS,
                          data: {
                            lecturerTimetableWeeks: {
                              ...dataLecturerTimetableWeeks.lecturerTimetableWeeks,
                              weekCoversCurrentTime: weeks[prevIndex],
                            },
                          },
                        })
                      }}
                    />
                  </Col>
                  <Col span={4} style={{ textAlign: 'center' }}>
                    <Select
                      onSelect={value => {
                        const dataLecturerTimetableWeeks = client.readQuery({
                          query: GET_LECTURER_TIMETABLE_WEEKS,
                        })
                        client.writeQuery({
                          query: GET_LECTURER_TIMETABLE_WEEKS,
                          data: {
                            lecturerTimetableWeeks: {
                              ...dataLecturerTimetableWeeks.lecturerTimetableWeeks,
                              weekCoversCurrentTime: value,
                            },
                          },
                        })
                      }}
                      value={data.lecturerTimetableWeeks.weekCoversCurrentTime}
                    >
                      {data.lecturerTimetableWeeks.weeks.map(
                        (weekDuration, index) => (
                          <Option value={weekDuration} key={index}>
                            {weekDuration}
                          </Option>
                        )
                      )}
                    </Select>
                  </Col>
                  <Col span={2} style={{ textAlign: 'center' }}>
                    <Button
                      icon="right"
                      disabled={isLastWeek(client)}
                      onClick={() => {
                        const dataLecturerTimetableWeeks = client.readQuery({
                          query: GET_LECTURER_TIMETABLE_WEEKS,
                        })
                        const {
                          weeks,
                          weekCoversCurrentTime,
                        } = dataLecturerTimetableWeeks.lecturerTimetableWeeks
                        debugger
                        const nextIndex =
                          weeks.indexOf(weekCoversCurrentTime) + 1
                        client.writeQuery({
                          query: GET_LECTURER_TIMETABLE_WEEKS,
                          data: {
                            lecturerTimetableWeeks: {
                              ...dataLecturerTimetableWeeks.lecturerTimetableWeeks,
                              weekCoversCurrentTime: weeks[nextIndex],
                            },
                          },
                        })
                      }}
                    />
                  </Col>
                </Row>
              )}
            </ApolloConsumer>

            <CustomQuery
              query={GET_TIMETABLE_OF_CURRENT_LECTURER}
              variables={{
                week: data.lecturerTimetableWeeks.weekCoversCurrentTime,
              }}
            >
              {({ Loading, data: timetable }) => (
                <Table
                  pagination={false}
                  rowKey="section"
                  loading={Loading}
                  columns={columns}
                  dataSource={timetable.timetableForCurrentLecturer}
                  bordered
                />
              )}
            </CustomQuery>
          </React.Fragment>
        )
      }}
    </CustomQuery>
  </React.Fragment>
)

export default ProfessorTimetable
