import React from 'react'
import { Collapse, Alert, Divider, Icon, Avatar, Table } from 'antd'
import { Link } from 'react-router-dom'
import { isEmpty } from 'lodash'
import CustomQuery from '../../components/CustomQuery'
import { GET_STUDENT_REGISTERED_CLASSES } from '../../graphql/student'
import { COLOR } from '../../constants/color'
import { letterGradeFor } from '../../utils/grade'
import ClassesAnalysis from './registeredClassesList/ClassesAnalysis'

const { Panel } = Collapse

const { Column } = Table

const RegisteredClassesList = () => (
  <React.Fragment>
    <CustomQuery query={GET_STUDENT_REGISTERED_CLASSES}>
      {({ loading, data }) => {
        if (loading) return <div>Loading...</div>
        if (isEmpty(data.studentRegisteredClasses))
          return (
            <Alert
              description="You don't have any registered classes!"
              type="warning"
              showIcon
              message="Warning"
            />
          )
        return (
          <Collapse
            bordered={false}
            defaultActiveKey={[data.studentRegisteredClasses[0].semester.id]}
          >
            {data.studentRegisteredClasses.map(
              ({
                semester: {
                  id,
                  category,
                  startYear,
                  endYear,
                  classesAnalysisOfCurrentStudent,
                },
                theoryGroups,
                labGroups,
              }) => (
                <Panel
                  header={`${category} ${startYear} - ${endYear}`}
                  key={id}
                >
                  {!isEmpty(theoryGroups) && (
                    <React.Fragment>
                      <Divider orientation="left">Theory Groups</Divider>
                      <Table
                        bordered
                        dataSource={theoryGroups}
                        loading={loading}
                        rowKey={theoryGroup => theoryGroup.id}
                        pagination={false}
                      >
                        <Column
                          title="Name"
                          key="name"
                          render={theoryGroup => (
                            <Link
                              to={`/class/${theoryGroup.registeredClass.slug}`}
                              style={{ color: COLOR.MAIN_BLUE }}
                            >
                              <Avatar
                                style={{
                                  backgroundColor: COLOR.MAIN_BLUE,
                                  marginRight: 10,
                                }}
                              >
                                <Icon type="read" />
                              </Avatar>
                              {theoryGroup.course.name}
                            </Link>
                          )}
                        />
                        <Column
                          title="Day"
                          key="day"
                          render={theoryGroup =>
                            theoryGroup.registeredClass.day
                          }
                          align="center"
                        />
                        <Column
                          title="Credits"
                          key="credits"
                          render={theoryGroup =>
                            theoryGroup.registeredClass.credits
                          }
                          align="center"
                        />
                        <Column
                          title="Amount Of Students"
                          key="studentAmount"
                          render={theoryGroup =>
                            theoryGroup.registeredClass.amountOfStudents
                          }
                          align="center"
                        />
                        <Column
                          title="Average Grade"
                          key="averageGrade"
                          render={theoryGroup =>
                            theoryGroup.registeredClass
                              .averageGradeOfCurrentStudent
                          }
                          align="center"
                        />
                        <Column
                          title="Letter Grade"
                          key="letterGrade"
                          render={theoryGroup =>
                            letterGradeFor(
                              theoryGroup.registeredClass
                                .averageGradeOfCurrentStudent
                            )
                          }
                          align="center"
                        />
                      </Table>
                    </React.Fragment>
                  )}
                  {!isEmpty(labGroups) && (
                    <React.Fragment>
                      <Divider orientation="left">Lab Groups</Divider>
                      <Table
                        bordered
                        dataSource={labGroups}
                        loading={loading}
                        rowKey={labGroup => labGroup.id}
                        pagination={false}
                      >
                        <Column
                          title="Name"
                          key="name"
                          render={labGroup => (
                            <Link
                              to={`/class/${labGroup.registeredClass.slug}`}
                              style={{ color: COLOR.MAIN_GREEN }}
                            >
                              <Avatar
                                style={{
                                  backgroundColor: COLOR.MAIN_GREEN,
                                  marginRight: 10,
                                }}
                              >
                                <Icon type="bg-colors" />
                              </Avatar>
                              {labGroup.laboratory.name}
                            </Link>
                          )}
                        />
                        <Column
                          title="Day"
                          key="day"
                          render={labGroup => labGroup.registeredClass.day}
                          align="center"
                        />
                        <Column
                          title="Credits"
                          key="credits"
                          align="center"
                          render={labGroup => labGroup.registeredClass.credits}
                        />
                        <Column
                          title="Amount Of Students"
                          key="studentAmount"
                          align="center"
                          render={labGroup =>
                            labGroup.registeredClass.amountOfStudents
                          }
                        />
                        <Column
                          title="Average Grade"
                          key="averageGrade"
                          align="center"
                          render={labGroup =>
                            labGroup.registeredClass
                              .averageGradeOfCurrentStudent
                          }
                        />
                        <Column
                          title="Letter Grade"
                          key="letterGrade"
                          align="center"
                          render={labGroup =>
                            letterGradeFor(
                              labGroup.registeredClass
                                .averageGradeOfCurrentStudent
                            )
                          }
                        />
                      </Table>
                      <br />
                      <Divider orientation="left">Analysis</Divider>
                      <ClassesAnalysis
                        analysis={classesAnalysisOfCurrentStudent}
                      />
                    </React.Fragment>
                  )}
                </Panel>
              )
            )}
          </Collapse>
        )
      }}
    </CustomQuery>
  </React.Fragment>
)

export default RegisteredClassesList
