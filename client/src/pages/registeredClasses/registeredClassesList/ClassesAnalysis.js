import React from 'react'
import { List, Card, Skeleton } from 'antd'
import { COLOR } from '../../../constants/color'

const ClassesAnalysis = ({
  analysis: { systemFour, classification, creditsAchieved, averageGrade },
  loading,
  isAccumulated,
}) => {
  const data = [
    {
      title: isAccumulated ? (
        <b>Average Grade Accumulated (System 10/100)</b>
      ) : (
        'Semester Average Grade (System 10/100)'
      ),
      value: averageGrade,
      color: COLOR.MAIN_BLUE,
    },
    {
      title: isAccumulated ? (
        <b>Average Grade Accumulated (System 4)</b>
      ) : (
        'Semester Average Grade (System 4)'
      ),
      value: systemFour,
      color: COLOR.MAIN_MAGENTA,
    },
    {
      title: isAccumulated ? <b>Credits Accumulated</b> : 'Credits Achieved',
      value: creditsAchieved,
      color: COLOR.MAIN_GREEN,
    },
    {
      title: isAccumulated ? <b>Overall Classification</b> : 'Classification',
      value: classification,
      color: COLOR.MAIN_PURPLE,
    },
  ]

  return (
    <React.Fragment>
      <List
        grid={{ gutter: 16, column: 4 }}
        dataSource={data}
        renderItem={item => (
          <List.Item>
            <Card title={item.title} style={{ textAlign: 'center' }}>
              <Skeleton loading={loading} active>
                <h1 style={{ color: item.color }}>
                  <b>{item.value}</b>
                </h1>
              </Skeleton>
            </Card>
          </List.Item>
        )}
      />
    </React.Fragment>
  )
}

export default ClassesAnalysis
