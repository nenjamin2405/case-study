import React from 'react'
import RegisteredClassesList from './registeredClasses/RegisteredClassesList'
import CustomQuery from '../components/CustomQuery'
import { GET_ALL_SEMESTERS_GRADES_ANALYSIS } from '../graphql/student'
import ClassesAnalysis from './registeredClasses/registeredClassesList/ClassesAnalysis'

const RegisteredClasses = () => (
  <React.Fragment>
    <CustomQuery query={GET_ALL_SEMESTERS_GRADES_ANALYSIS}>
      {({ loading, data }) => (
        <ClassesAnalysis
          analysis={
            data.allSemestersGradesAnalysis
              ? data.allSemestersGradesAnalysis
              : {}
          }
          isAccumulated
          loading={loading}
        />
      )}
    </CustomQuery>
    <RegisteredClassesList />
  </React.Fragment>
)

export default RegisteredClasses
