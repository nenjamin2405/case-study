import React from 'react'
import StudentTimetable from './timetable/StudentTimetable'

const Timetable = () => (
  <React.Fragment>
    <StudentTimetable />
  </React.Fragment>
)

export default Timetable
