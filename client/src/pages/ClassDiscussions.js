import React from 'react'
import ClassLayout from '../components/ClassLayout'
import { TABS } from '../constants/registeredClass'

const ClassDiscussions = () => (
  <React.Fragment>
    <ClassLayout activeKey={TABS.DISCUSSIONS} />
  </React.Fragment>
)

export default ClassDiscussions
