import React from 'react'
import LoginBox from './login/LoginForm'
import './Login.scss'

const Login = () => (
  <div className="login-container">
    <LoginBox />
  </div>
)

export default Login
