import React from 'react'
import RegistrationTable from './courseRegistration/RegistrationTable'

const CourseRegistration = () => (
  <React.Fragment>
    <RegistrationTable />
  </React.Fragment>
)

export default CourseRegistration
