const isAuthenticated = () => {
  if (localStorage.user) return true
  return false
}

export default isAuthenticated
