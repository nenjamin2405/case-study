import { Modal } from 'antd'
import React from 'react'

export const showErrorModal = errors => {
  Modal.error({
    title: 'Error',
    content: errors.graphQLErrors.map((error, i) => (
      <div key={i}>{error.message}</div>
    )),
  })
}
