export const storeUserSession = ({ accessToken, client, uid }) => {
  localStorage.setItem(
    'user',
    JSON.stringify({
      'access-token': accessToken,
      client,
      uid,
    })
  )
}
