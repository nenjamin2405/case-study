import React from 'react'
import { Tag } from 'antd'

const inRange = (number, start, end) => {
  if (number >= start && number <= end) {
    return true
  }
  return false
}

const renderTag = (grade, color, letter, renderGrade) => (
  <Tag color={color}>
    {renderGrade && <b>{grade} - </b>}
    {letter}
  </Tag>
)

export const letterGradeFor = (grade, renderGrade) => {
  if (inRange(grade, 97, 100)) {
    return renderTag(grade, 'green', 'A+', renderGrade)
  }
  if (inRange(grade, 93, 96)) {
    return renderTag(grade, 'lime', 'A', renderGrade)
  }
  if (inRange(grade, 90, 92)) {
    return renderTag(grade, 'cyan', 'A-', renderGrade)
  }
  if (inRange(grade, 87, 89)) {
    return renderTag(grade, 'blue', 'B+', renderGrade)
  }
  if (inRange(grade, 83, 86)) {
    return renderTag(grade, 'geekBlue', 'B', renderGrade)
  }
  if (inRange(grade, 80, 82)) {
    return renderTag(grade, 'purple', 'B-', renderGrade)
  }
  if (inRange(grade, 77, 79)) {
    return renderTag(grade, 'gold', 'C+', renderGrade)
  }
  if (inRange(grade, 73, 76)) {
    return renderTag(grade, 'orange', 'C', renderGrade)
  }
  if (inRange(grade, 70, 72)) {
    return renderTag(grade, 'volcano', 'C-', renderGrade)
  }
  if (inRange(grade, 67, 69)) {
    return renderTag(grade, 'magenta', 'D+', renderGrade)
  }
  if (inRange(grade, 65, 66)) {
    return renderTag(grade, 'red', 'D', renderGrade)
  }
  if (grade < 65) {
    return renderTag(grade, '#f50', 'F', renderGrade)
  }
}
