import { Menu, List } from 'antd'
import moment from 'moment'
import React from 'react'
import { animateScroll as scroll } from 'react-scroll'
import gql from 'graphql-tag'
import CloudImage from '../components/CloudImage'
import {
  MARK_AS_READ,
  MARK_ALL_SEEN_FOR_NOTIFICATIONS,
  GET_CURRENT_USER_NOTIFICATIONS,
} from '../graphql/user'
import { GET_COMMENTS, GET_GRADES_OF_STUDENT } from '../graphql/registeredClass'

export const generateNotificationItemContent = (
  notification,
  history,
  client
) => {
  const {
    navigateLink,
    actor,
    createdAt,
    id,
    readAt,
    content,
    notifiable,
  } = notification

  return (
    <Menu.Item
      key={id}
      onClick={() => {
        markRead(notification, client)
        switch (notifiable.__typename) {
          case 'GradeColumn':
            fetchGradeColumnsAndNavigateToGradeTable({
              client,
              history,
              navigateLink,
              notification,
            })
            break

          default:
            navigateAndScrollToComment({
              client,
              history,
              navigateLink,
              notification,
            })
            break
        }
      }}
      style={!readAt && { backgroundColor: '#e6f7ff' }}
    >
      <List.Item>
        <List.Item.Meta
          avatar={<CloudImage publicId={actor.avatar} />}
          title={<span>{content}</span>}
          description={moment(createdAt).fromNow()}
        />
      </List.Item>
    </Menu.Item>
  )
}

export const navigateAndScrollToComment = async ({
  client,
  history,
  navigateLink,
  notification,
}) => {
  await client.query({
    query: GET_COMMENTS,
    variables: {
      discussionSlug: notification.notifiable.slug,
    },
    fetchPolicy: 'network-only',
  })
  history.push(navigateLink)
  scroll.scrollToBottom({
    containerId: 'commentsContainer',
  })
}

export const fetchGradeColumnsAndNavigateToGradeTable = async ({
  client,
  history,
  navigateLink,
  notification,
}) => {
  await client.query({
    query: GET_GRADES_OF_STUDENT,
    variables: {
      slug: notification.notifiable.registeredClass.slug,
    },
    fetchPolicy: 'network-only',
  })
  history.push(navigateLink)
}

export const markRead = (notification, client) => {
  const { readAt, id } = notification
  if (!readAt) {
    client.writeFragment({
      id: `Notification:${id}`,
      fragment: gql`
        fragment updatedNotification on Notification {
          readAt
        }
      `,
      data: {
        readAt: moment(),
        __typename: 'Notification',
      },
    })
    client.mutate({
      mutation: MARK_AS_READ,
      variables: {
        input: {
          notificationId: id.toString(),
        },
      },
    })
    const notisData = client.readQuery({
      query: GET_CURRENT_USER_NOTIFICATIONS,
    })
    client.writeQuery({
      query: GET_CURRENT_USER_NOTIFICATIONS,
      data: {
        notifications: notisData.notifications.map(noti => ({
          ...noti,
          seenAt: moment(),
        })),
      },
    })
    client.mutate({
      mutation: MARK_ALL_SEEN_FOR_NOTIFICATIONS,
      variables: {
        input: {},
      },
    })
  }
}
