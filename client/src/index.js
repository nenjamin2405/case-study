import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router } from 'react-router-dom'
import './index.css'
import { ApolloLink } from 'apollo-link'
import { createUploadLink } from 'apollo-upload-client'
import { withClientState } from 'apollo-link-state'
import { ApolloProvider } from 'react-apollo'
import { setContext } from 'apollo-link-context'
import { ApolloClient } from 'apollo-client'
import {
  InMemoryCache,
  IntrospectionFragmentMatcher,
} from 'apollo-cache-inmemory'
import introspectionQueryResultData from './fragmentTypes.json'
import { resolvers, defaults } from './resolvers'
import App from './App'

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData,
})

require('dotenv').config()

const uploadLink = createUploadLink({ uri: '/graphql' })

const cache = new InMemoryCache({ fragmentMatcher })

const authLink = setContext((_, { headers }) => {
  const { user } = localStorage
  return {
    headers: {
      ...headers,
      ...JSON.parse(user || null),
    },
  }
})

const stateLink = withClientState({
  cache,
  defaults,
  resolvers,
})

const client = new ApolloClient({
  link: ApolloLink.from([authLink, stateLink]).concat(uploadLink),
  cache,
})

ReactDOM.render(
  <Router>
    <ApolloProvider client={client}>
      <App client={client} />
    </ApolloProvider>
  </Router>,
  document.getElementById('root')
)
