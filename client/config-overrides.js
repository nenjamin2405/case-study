const { injectBabelPlugin } = require('react-app-rewired')
const rewireLess = require('react-app-rewire-less')
const fetch = require('node-fetch')
const fs = require('fs')

module.exports = function override(config, env) {
  let configOverride = rewireLess.withLoaderOptions({
    javascriptEnabled: true,
    modifyVars: { '@primary-color': '#1890ff' },
  })(config, env)
  configOverride = injectBabelPlugin(
    ['import', { libraryName: 'antd', style: true }],
    configOverride
  )

  return configOverride
}
